var graf5 = {
    canvas: false,
    stage:false,
    exportRoot: false,
    handleComplete: function() {
        graf5.canvas = document.getElementById("canvas_graf5");
        graf5.exportRoot = new lib_graf5.gripa_povijesni_1();
        graf5.stage = new createjs.Stage(graf5.canvas);
        graf5.stage.addChild(graf5.exportRoot);
        graf5.stage.enableMouseOver();
        //Registers the "tick" event listener.
        createjs.Ticker.setFPS(lib_graf5.properties.fps);
        createjs.Ticker.addEventListener("tick", graf5.stage);
        //Code to support hidpi screens and responsive scaling.
        (function(isResp, respDim, isScale, scaleType) {
        var lastW, lastH, lastS=1;
        window.addEventListener('resize', resizeCanvas);
        resizeCanvas();
        function resizeCanvas() {
            var w = lib_graf5.properties.width, h = lib_graf5.properties.height;
            var iw = window.innerWidth, ih=window.innerHeight;
            var pRatio = window.devicePixelRatio, xRatio=iw/w, yRatio=ih/h, sRatio=1;
            if(isResp) {
                if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                    sRatio = lastS;
                }
                else if(!isScale) {
                    if(iw<w || ih<h)
                        sRatio = Math.min(xRatio, yRatio);
                }
                else if(scaleType==1) {
                    sRatio = Math.min(xRatio, yRatio);
                }
                else if(scaleType==2) {
                    sRatio = Math.max(xRatio, yRatio);
                }
            }
            graf5.canvas.width = w*pRatio*sRatio;
            graf5.canvas.height = h*pRatio*sRatio;
            graf5.canvas.style.width = w*sRatio+'px';
            graf5.canvas.style.height = h*sRatio+'px';
            graf5.stage.scaleX = pRatio*sRatio;
            graf5.stage.scaleY = pRatio*sRatio;
            lastW = iw; lastH = ih; lastS = sRatio;
        }
    })(false,'both',false,1);
    }
};

var semafor = {
    canvas: false,
    stage: false,
    exportRoot:false,
    anim_container: false,
    dom_overlay_container: false,
    fnStartAnimation: false,
    init: function() {
        semafor.canvas = document.getElementById("canvas_semafor");
        semafor.anim_container = document.getElementById("animation_container");
        semafor.dom_overlay_container = document.getElementById("dom_overlay_container");
        images = images||{};
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", semafor.handleFileLoad);
        loader.addEventListener("complete", semafor.handleComplete);
        loader.loadManifest(lib_semafor.properties.manifest);
    },
    handleFileLoad: function(evt) {
        if (evt.item.type == "image") { images[evt.item.id] = evt.result; }
    },
    handleComplete: function(evt) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var queue = evt.target;
        var ssMetadata = lib_semafor.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        semafor.exportRoot = new lib_semafor.avatar_desktop();
        semafor.stage = new createjs.Stage(semafor.canvas);
        semafor.stage.addChild(semafor.exportRoot);
        semafor.stage.enableMouseOver();
        //Registers the "tick" event listener.
        semafor.fnStartAnimation = function() {
            createjs.Ticker.setFPS(lib_semafor.properties.fps);
            createjs.Ticker.addEventListener("tick", semafor.stage);
        };

        semafor.fnStartAnimation();

        semafor.exportRoot.kontent.x = 300;
        semafor.exportRoot.kontent.y = 310;
    }
};

var semafor_mobile = {
    canvas: false,
    stage: false,
    exportRoot:false,
    anim_container: false,
    dom_overlay_container: false,
    fnStartAnimation: false,
    init: function() {
        semafor_mobile.canvas = document.getElementById("canvas_semafor");
        semafor_mobile.anim_container = document.getElementById("animation_container");
        semafor_mobile.dom_overlay_container = document.getElementById("dom_overlay_container");
        images = images||{};
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", semafor_mobile.handleFileLoad);
        loader.addEventListener("complete", semafor_mobile.handleComplete);
        loader.loadManifest(lib_semaform.properties.manifest);
    },
    handleFileLoad: function(evt) {
        if (evt.item.type == "image") { images[evt.item.id] = evt.result; }
    },
    handleComplete: function(evt) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var queue = evt.target;
        var ssMetadata = lib_semaform.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        semafor_mobile.exportRoot = new lib_semaform.avatar_mobile();
        semafor_mobile.stage = new createjs.Stage(semafor_mobile.canvas);
        semafor_mobile.stage.addChild(semafor_mobile.exportRoot);
        semafor_mobile.stage.enableMouseOver();
        //Registers the "tick" event listener.
        semafor_mobile.fnStartAnimation = function() {
            createjs.Ticker.setFPS(lib_semaform.properties.fps);
            createjs.Ticker.addEventListener("tick", semafor_mobile.stage);
        }
        //Code to support hidpi screens and responsive scaling.

        semafor_mobile.fnStartAnimation();
        window.addEventListener("resize", semafor_mobile.resize);
        semafor_mobile.resize();
    },
    resize: function() {
        var innerdiv = document.getElementById('box'),
            avatar_width=innerdiv.offsetWidth,
            avatar_height=innerdiv.offsetHeight,
            omjer=1.55,
            orginal_sirina=400;

        avatar_height = avatar_width*omjer;

        semafor_mobile.canvas.width = avatar_width;
        semafor_mobile.canvas.height = avatar_width*omjer;

        semafor_mobile.exportRoot.kontent.x = avatar_width / 2;
        semafor_mobile.exportRoot.kontent.y = avatar_height / 2;

        var scale = avatar_width/orginal_sirina;
        semafor_mobile.exportRoot.kontent.scaleX = semafor_mobile.exportRoot.kontent.scaleY = scale;
        document.getElementById("box").style.height = avatar_height + "px";
    }
};

var karta = {
    canvas: false,
    stage: false,
    exportRoot:false,
    anim_container: false,
    dom_overlay_container: false,
    init: function() {
        karta.canvas = document.getElementById("canvas");
        karta.anim_container = document.getElementById("animation_container");
        karta.dom_overlay_container = document.getElementById("dom_overlay_container");
        karta.handleComplete();
    },
    handleComplete: function() {
        karta.exportRoot = new lib_karta.karta();
        karta.stage = new createjs.Stage(karta.canvas);
        karta.stage.addChild(karta.exportRoot);
        karta.stage.enableMouseOver();
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            createjs.Ticker.setFPS(lib_karta.properties.fps);
            createjs.Ticker.addEventListener("tick", karta.stage);
        }
        //Code to support hidpi screens and responsive scaling.

        fnStartAnimation();
    }
};