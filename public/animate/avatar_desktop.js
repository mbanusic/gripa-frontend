(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {};
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {
	for(var i = 0; i < cacheList.length; i++) {
		if(cacheList[i].cacheCanvas)
			cacheList[i].updateCache();
	}
};

lib.addElementsToCache = function (textInst, cacheList) {
	var cur = textInst;
	while(cur != semafor.exportRoot) {
		if(cacheList.indexOf(cur) != -1)
			break;
		cur = cur.parent;
	}
	if(cur != semafor.exportRoot) {
		var cur2 = textInst;
		var index = cacheList.indexOf(cur);
		while(cur2 != cur) {
			cacheList.splice(index, 0, cur2);
			cur2 = cur2.parent;
			index++;
		}
	}
	else {
		cur = textInst;
		while(cur != semafor.exportRoot) {
			cacheList.push(cur);
			cur = cur.parent;
		}
	}
};

lib.gfontAvailable = function(family, totalGoogleCount) {
	lib.properties.webfonts[family] = true;
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];
	for(var f = 0; f < txtInst.length; ++f)
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);

	loadedGoogleCount++;
	if(loadedGoogleCount == totalGoogleCount) {
		lib.updateListCache(gFontsUpdateCacheList);
	}
};

lib.tfontAvailable = function(family, totalTypekitCount) {
	lib.properties.webfonts[family] = true;
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];
	for(var f = 0; f < txtInst.length; ++f)
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);

	loadedTypekitCount++;
	if(loadedTypekitCount == totalTypekitCount) {
		lib.updateListCache(tFontsUpdateCacheList);
	}
};
// symbols:



(lib.avatar2 = function() {
	this.initialize(img.avatar2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,431,701);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.drhtavica = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_43 = function() {
		this.gotoAndPlay(1);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(43).call(this.frame_43).wait(1));

	// Layer 3 copy
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgJKAxVQgHgHgHgUQgbhOgHgjIgKg6IgWhZQgIghgHhBQgIhDgHggQgQg6gFgeIgLhOQgHgvgKgdQgGgSgCgIIgBAAQhWlIAGlVQAChLAGhoQAPkOAOh4QAHg7AHgrQAShTAFgqQAGgrgChLQgBhWACggQhfCXhXC3QhKCahPDEQggBRgNAsQgNAxgVBkQgVBYgfA0IgBAEQgBAJgGAKIgLASQgJASgIAaIgNAuQgPAzgZAVQgRAOgXAAQgXABgNgPQgJAGgKAMIgRAVQgWAZgWABQgNAAgLgKQgKgLAEgMQgmAbgaAFQgSAEgSgEQgTgFgLgNQgPgSgCgqQgUAIgSgCQgWgCgKgPQgIgNADgZQADgaANgfQAHgUARgiIBqjbQgUADgYATQgkAdgGADQgbAQgdgBQgYAAgKgOQgJgKADgSQACgOAPgbQAxhWAcgqQAthFAtgxQAMgOAkgkQAfgfAQgTQAPgRAIgFIAIgEIADgPQAJghAegzQAjg8AJgWQALgZAJgpIAPhDQAJgjAhhaQAxiIA1jNQAShGgBglIAAgYIABgGIgBgIQAAgMAMgZQAMgaAQguQASg2AJgUQAOggAohNQAZgvAbg5QARgmABgXIgBgVQgBgOABgIIABgBIgBgPQAAhdAGhbIAFgyQAIg5Ath2QArhwAGg/IACgMQgFgGgDgIQgCgIAAgLIAAgTQAAgVgIghIgNg3QgDgUABgcIABgwIgBgvQgBgbAEgTQAGghAcgzQAnhGAkgpQBYhhC5g7IAOgEIAGgDQA7geAhgLIAfgLQASgGALgGQAigQALgbQAEgJAEgZQAOhigBgyQgBgNABgJIgDgDQglgfgSgTQgcgfgNgeQgOghgEg5QgFhLgEgSQgDgPgIgdQgJgegDgPQgGggAAhCQABivAKilIABgLQgGgIgCgPQgHg2AXhHQAhhiBKhPQBIhNBggtQBdgrBqgJQBogJBnAYQA0AMAjASQAiARA2AtQBQBGAvA9QA8BPARBSIACAKIADAJQAYBZgTBzQgMBHgqCCQglBygYA3IgbA/QgQAkgIAbQgLAogDAzQgCAdAABAQABA/AGAjQAJA2AbAiQAbAkA1AWQAgAOBCAQQCqApBkAoQCQA7BcBWQA9A7ArBLQAqBLASBTQAEAPgCAMIgCAFQAHCdgQCbQgFAqAAATQgBAjAGAaQAJAiAfAvQAqA/AHAPQAPAeANAsIATBNQAVBOAtBvIBNC4QBKDCAfDSQADAXgGAMIABABQAGALgBAYQgBBOgOERQgLDaACCGIACBoQABA8gDArQgDAsgKBIQgNBXgDAcQgIBIgDCgIAAATIAAA4IACABQgBARACAYIAFAyIAFAuIAEAYIABAUQgBAOgFAdIAAAxIAAAMQgEAYgGAVIgPAzQgJAfgEAUQgDAWgBAeIAAA0QgBBogcCXQgNBMgTAnQgWAwgiARQgVAKgWgCQgYgDgPgPQgcAWgWAEQgPADgOgEQgPgEgHgMQgKgNACgcQADgigDgKQgHAdgfANQgfANgagPQgVgNgKgaQgJgXAAgdQAAgOAFgmQADghgBgTQgWAEgOgCQgUgCgKgMQgLgLgDgbQgEgoAJgzIAUhZQAch2APh5IADgPIgBAAIAAgBQAJhcACgvIAEi6QAChvAOhKQAEgRAIgLIABgBQgBgJABgNQAMjMgrjGIgVhcQgNg4gGglQgJg9gFhtIgdpdQAAgPACgLQgGgIgIgPIjXnPIgDANIgIApQgFAVgKAdIgSAyQgPAvgWBnIgqDPQgMBCgGAoQgIA7AAAxQAACKBHDSIA8CqQAhBkAQBKQATBcAJCTQAMDQgKC0IgCAbIgDAAIgFArIgFAtIgGArQgCAQAAAcIAAArIgFAvIgFAuIABBsQABBBgPAoQgGAPgIAGIgCAJIgSA7QgNAngFAVQgMAugKBVQgLBcgJAnIgUBQQgFAagLBLQgIBAgLAkQgFATgLAEQgIAEgJgFQgJgFgEgJQgEgMAEgaIBPnFQAMhFAJgiIAYhOIAMgwIAKAAIABgFQAMgngBg/QgChaABgOIAFgqIAEgpQABgNAAgYIAAgmQABgWAIg0QAHgwAAgbQAAgVAEgIIABgBIABghQAJi2gJiKQgKisgmiPQgQg9gfhbIg0iXQg8i2gCh/QgBgzAJhPQAUinBCkBQAdhuAShCIAZhaQAJghATgDQAMgBAJALQAGAHACAJQAHAHAHATIA8CPQAvBvAbA3IA0BiQAfA7ARAoQANAbgIAOQADAKAAAMIAeJrQAFBcAFAkQAFAiALAvIATBPQA0DpgPDwQgBAWgJAJIgCAOQgMBFgDBfIgEClQgCBdgHBNIgEAAQACAKgCARQgLBogmCdQgNAygDAVQgGApAFAgQANgEAHgQIAKgeQANgxAkgkQAOgNALABQAIAAAHAHQAGAGACAJQADANgHAWIgkCCQgTBLAEA4QABAMAGAJQAGALAKgDQAIgCAGgQIBHjRQAOgmAWAEQALADAFANQAEAMgBAPQgCAZgTBtQgOBSAHA0QAKACAJgIQAJgHADgLIADgVQABgPACgGQACgLAHgIQAIgJAKgBQAJAAAHAHQAHAGADAJQADAMgFAZQgEATAHAIQAHAIANgFQALgEAIgLQAPgUALgeQAHgVAIgiQAgiVABiYIAAgyQABgcAEgVQAEgRAJgcIAPgtQAHgaAFgeQAFgiABgoIAAgoIgKgiQgIglgDguQgCgdABg1IAAgCQAAiRAIhIIASh7QAPhtABidQgBixAChZQABhcAKi5IANjxIgBgBQgCgEAAgGQgBgLAGgGIAAgBIABgEIgFgGQgHgMgCgcQgNidhbjoIhMi9QgrhvgXhSIgShCQgMgmgNgaQgHgOgshFQgggygIgkQgGgaABgiQAAgQAFgrQAPibgEigQgBgQACgLIgDgLQgShHgkhAQglhAg0gzQhUhRiEg3QhegoiaglQhZgWgqgSQhGgegigxQgdgogLg+QgGgogBhIQAAhBACgiQADg3ALgrQAKgnAWg0IAnhZQAWg2AjhxQAehgAIg0QAOhTgPhCIgEgXIgBgNIgFgNIgGgUQgQhAgxg/QglgvhCg5QgqglgdgRQgqgZhAgQQhogZhtARQhsARhcA4QhZA2g4BUQg7BZgCBgQAAAVgDALIADAEQAGAMgCAZQgJClgCClQgBAvAFAaQADAOAIAYIALAlQAHAcADAoIADBFQAEAmAJAUQALAXAeAcQAoAkAIALQAJAMADAMQABAFgBAEQACAIAAAMQABAagCAgIgJBeQgDAcgEAOQgOArgwAiQgSANgQAGIgqANQgSAGgeAOIggAPQgRAIgHABQgLAGgSAGQizA5hOBqQgTAagXAsQgVAjgHAYQgHAVgBAcQgBAQAAAhQABA0ADAZQADASAKA0QAJArABAaQABAOgCAJIABACQAHALgBAVQgBAcgQA3QgbBZggBRQgOAlgDALQgFATgFAnQgMBnACBqQAAARgCAJIABAGQAGAogPAwQgKAhgbAzIgvBaQgbA1gQAoIgXBEQgOArgLAZIgDAGIAAABQAAA8gPBJQgKAtgYBUIgvCeQgQA1gKAaIgYBDQgPAogHAbIgKA1QgGAggHAVQgLAfgjA7QgiA6gLAhQgGARgEAHQgGAJgIACIAAABQgFAMgQARIhIBHQgpAqgaAhQgdAngrBKIgqBIQAkgYAegXQAVgRAOgGQAVgJARAGQANAFAJAPQAHANABAQQAAAVgQAlQgLAZg4B0QgrBVgTA6IBBg/QAYgWAOAFQAKADAEALQAEAKgCALQgCAIgGALIgLARQgYApABAuQAcgEAXgRQAXgRANgZIAOgdQALgRAQACQAKAAAHAKQAHAKAAALQABAOgOAbQASgCANghQAOghATABQAQAAAIARQAFALABAUQAGADAGgGQAFgFACgIQAXhKAihHQAFgJAFgHQABgHADgIIALgTQASgiAMg1IARhbQAMg1AghbQCbmrDxmDQAJgPAKgIQAOgKAMAGQAPAGABAiIABDOQAABVgGAqQgCARgMBBQgYCCgNCjQgIBlgIDBQgGCNABBHQAEDtBKE2QAEAHADAKQAJAaAHAjIALA9IAXCIIAKA8QAPBYAKAsQATBbADAXIAHAqQAEAOAIAVIAYBBQAGATgBAKQgCATgPAEIgHACQgLAAgJgLg");
	this.shape.setTransform(181,306.6);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(21).to({_off:false},0).to({_off:true},11).wait(12));

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EgJKAxVQgHgHgHgUQgbhOgHgjIgKg6IgWhZQgIghgHhBQgIhDgHggQgQg6gFgeIgLhOQgHgvgKgdQgGgSgCgIIgBAAQhWlIAGlVQAChLAGhoQAPkOAOh4QAHg7AHgrQAShTAFgqQAGgrgChLQgBhWACggQhfCXhXC3QhKCahPDEQggBRgNAsQgNAxgVBkQgVBYgfA0IgBAEQgBAJgGAKIgLASQgJASgIAaIgNAuQgPAzgZAVQgRAOgXAAQgXABgNgPQgJAGgKAMIgRAVQgWAZgWABQgNAAgLgKQgKgLAEgMQgmAbgaAFQgSAEgSgEQgTgFgLgNQgPgSgCgqQgUAIgSgCQgWgCgKgPQgIgNADgZQADgaANgfQAHgUARgiIBqjbQgUADgYATQgkAdgGADQgbAQgdgBQgYAAgKgOQgJgKADgSQACgOAPgbQAxhWAcgqQAthFAtgxQAMgOAkgkQAfgfAQgTQAPgRAIgFIAIgEIADgPQAJghAegzQAjg8AJgWQALgZAJgpIAPhDQAJgjAhhaQAxiIA1jNQAShGgBglIAAgYIABgGIgBgIQAAgMAMgZQAMgaAQguQASg2AJgUQAOggAohNQAZgvAbg5QARgmABgXIgBgVQgBgOABgIIABgBIgBgPQAAhdAGhbIAFgyQAIg5Ath2QArhwAGg/IACgMQgFgGgDgIQgCgIAAgLIAAgTQAAgVgIghIgNg3QgDgUABgcIABgwIgBgvQgBgbAEgTQAGghAcgzQAnhGAkgpQBYhhC5g7IAOgEIAGgDQA7geAhgLIAfgLQASgGALgGQAigQALgbQAEgJAEgZQAOhigBgyQgBgNABgJIgDgDQglgfgSgTQgcgfgNgeQgOghgEg5QgFhLgEgSQgDgPgIgdQgJgegDgPQgGggAAhCQABivAKilIABgLQgGgIgCgPQgHg2AXhHQAhhiBKhPQBIhNBggtQBdgrBqgJQBogJBnAYQA0AMAjASQAiARA2AtQBQBGAvA9QA8BPARBSIACAKIADAJQAYBZgTBzQgMBHgqCCQglBygYA3IgbA/QgQAkgIAbQgLAogDAzQgCAdAABAQABA/AGAjQAJA2AbAiQAbAkA1AWQAgAOBCAQQCqApBkAoQCQA7BcBWQA9A7ArBLQAqBLASBTQAEAPgCAMIgCAFQAHCdgQCbQgFAqAAATQgBAjAGAaQAJAiAfAvQAqA/AHAPQAPAeANAsIATBNQAVBOAtBvIBNC4QBKDCAfDSQADAXgGAMIABABQAGALgBAYQgBBOgOERQgLDaACCGIACBoQABA8gDArQgDAsgKBIQgNBXgDAcQgIBIgDCgIAAATIAAA4IACABQgBARACAYIAFAyIAFAuIAEAYIABAUQgBAOgFAdIAAAxIAAAMQgEAYgGAVIgPAzQgJAfgEAUQgDAWgBAeIAAA0QgBBogcCXQgNBMgTAnQgWAwgiARQgVAKgWgCQgYgDgPgPQgcAWgWAEQgPADgOgEQgPgEgHgMQgKgNACgcQADgigDgKQgHAdgfANQgfANgagPQgVgNgKgaQgJgXAAgdQAAgOAFgmQADghgBgTQgWAEgOgCQgUgCgKgMQgLgLgDgbQgEgoAJgzIAUhZQAch2APh5IADgPIgBAAIAAgBQAJhcACgvIAEi6QAChvAOhKQAEgRAIgLIABgBQgBgJABgNQAMjMgrjGIgVhcQgNg4gGglQgJg9gFhtIgdpdQAAgPACgLQgGgIgIgPIjXnPIgDANIgIApQgFAVgKAdIgSAyQgPAvgWBnIgqDPQgMBCgGAoQgIA7AAAxQAACKBHDSIA8CqQAhBkAQBKQATBcAJCTQAMDQgKC0IgCAbIgDAAIgFArIgFAtIgGArQgCAQAAAcIAAArIgFAvIgFAuIABBsQABBBgPAoQgGAPgIAGIgCAJIgSA7QgNAngFAVQgMAugKBVQgLBcgJAnIgUBQQgFAagLBLQgIBAgLAkQgFATgLAEQgIAEgJgFQgJgFgEgJQgEgMAEgaIBPnFQAMhFAJgiIAYhOIAMgwIAKAAIABgFQAMgngBg/QgChaABgOIAFgqIAEgpQABgNAAgYIAAgmQABgWAIg0QAHgwAAgbQAAgVAEgIIABgBIABghQAJi2gJiKQgKisgmiPQgQg9gfhbIg0iXQg8i2gCh/QgBgzAJhPQAUinBCkBQAdhuAShCIAZhaQAJghATgDQAMgBAJALQAGAHACAJQAHAHAHATIA8CPQAvBvAbA3IA0BiQAfA7ARAoQANAbgIAOQADAKAAAMIAeJrQAFBcAFAkQAFAiALAvIATBPQA0DpgPDwQgBAWgJAJIgCAOQgMBFgDBfIgEClQgCBdgHBNIgEAAQACAKgCARQgLBogmCdQgNAygDAVQgGApAFAgQANgEAHgQIAKgeQANgxAkgkQAOgNALABQAIAAAHAHQAGAGACAJQADANgHAWIgkCCQgTBLAEA4QABAMAGAJQAGALAKgDQAIgCAGgQIBHjRQAOgmAWAEQALADAFANQAEAMgBAPQgCAZgTBtQgOBSAHA0QAKACAJgIQAJgHADgLIADgVQABgPACgGQACgLAHgIQAIgJAKgBQAJAAAHAHQAHAGADAJQADAMgFAZQgEATAHAIQAHAIANgFQALgEAIgLQAPgUALgeQAHgVAIgiQAgiVABiYIAAgyQABgcAEgVQAEgRAJgcIAPgtQAHgaAFgeQAFgiABgoIAAgoIgKgiQgIglgDguQgCgdABg1IAAgCQAAiRAIhIIASh7QAPhtABidQgBixAChZQABhcAKi5IANjxIgBgBQgCgEAAgGQgBgLAGgGIAAgBIABgEIgFgGQgHgMgCgcQgNidhbjoIhMi9QgrhvgXhSIgShCQgMgmgNgaQgHgOgshFQgggygIgkQgGgaABgiQAAgQAFgrQAPibgEigQgBgQACgLIgDgLQgShHgkhAQglhAg0gzQhUhRiEg3QhegoiaglQhZgWgqgSQhGgegigxQgdgogLg+QgGgogBhIQAAhBACgiQADg3ALgrQAKgnAWg0IAnhZQAWg2AjhxQAehgAIg0QAOhTgPhCIgEgXIgBgNIgFgNIgGgUQgQhAgxg/QglgvhCg5QgqglgdgRQgqgZhAgQQhogZhtARQhsARhcA4QhZA2g4BUQg7BZgCBgQAAAVgDALIADAEQAGAMgCAZQgJClgCClQgBAvAFAaQADAOAIAYIALAlQAHAcADAoIADBFQAEAmAJAUQALAXAeAcQAoAkAIALQAJAMADAMQABAFgBAEQACAIAAAMQABAagCAgIgJBeQgDAcgEAOQgOArgwAiQgSANgQAGIgqANQgSAGgeAOIggAPQgRAIgHABQgLAGgSAGQizA5hOBqQgTAagXAsQgVAjgHAYQgHAVgBAcQgBAQAAAhQABA0ADAZQADASAKA0QAJArABAaQABAOgCAJIABACQAHALgBAVQgBAcgQA3QgbBZggBRQgOAlgDALQgFATgFAnQgMBnACBqQAAARgCAJIABAGQAGAogPAwQgKAhgbAzIgvBaQgbA1gQAoIgXBEQgOArgLAZIgDAGIAAABQAAA8gPBJQgKAtgYBUIgvCeQgQA1gKAaIgYBDQgPAogHAbIgKA1QgGAggHAVQgLAfgjA7QgiA6gLAhQgGARgEAHQgGAJgIACIAAABQgFAMgQARIhIBHQgpAqgaAhQgdAngrBKIgqBIQAkgYAegXQAVgRAOgGQAVgJARAGQANAFAJAPQAHANABAQQAAAVgQAlQgLAZg4B0QgrBVgTA6IBBg/QAYgWAOAFQAKADAEALQAEAKgCALQgCAIgGALIgLARQgYApABAuQAcgEAXgRQAXgRANgZIAOgdQALgRAQACQAKAAAHAKQAHAKAAALQABAOgOAbQASgCANghQAOghATABQAQAAAIARQAFALABAUQAGADAGgGQAFgFACgIQAXhKAihHQAFgJAFgHQABgHADgIIALgTQASgiAMg1IARhbQAMg1AghbQCbmrDxmDQAJgPAKgIQAOgKAMAGQAPAGABAiIABDOQAABVgGAqQgCARgMBBQgYCCgNCjQgIBlgIDBQgGCNABBHQAEDtBKE2QAEAHADAKQAJAaAHAjIALA9IAXCIIAKA8QAPBYAKAsQATBbADAXIAHAqQAEAOAIAVIAYBBQAGATgBAKQgCATgPAEIgHACQgLAAgJgLg");
	this.shape_1.setTransform(181,306.6);
	this.shape_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1).to({_off:false},0).to({_off:true},11).wait(32));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.bolumisicima = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_36 = function() {
		this.gotoAndPlay(1);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(36).call(this.frame_36).wait(1));

	// Layer 1 copy
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3D4E").s().p("EgJKAxVQgHgHgHgUQgbhOgHgjIgKg6IgWhZQgIghgHhBQgIhDgHggQgQg6gFgeIgLhOQgHgvgKgdQgGgSgCgIIgBAAQhWlIAGlVQAChLAGhoQAPkOAOh4QAHg7AHgrQAShTAFgqQAGgrgChLQgBhWACggQhfCXhXC3QhKCahPDEQggBRgNAsQgNAxgVBkQgVBYgfA0IgBAEQgBAJgGAKIgLASQgJASgIAaIgNAuQgPAzgZAVQgRAOgXAAQgXABgNgPQgJAGgKAMIgRAVQgWAZgWABQgNAAgLgKQgKgLAEgMQgmAbgaAFQgSAEgSgEQgTgFgLgNQgPgSgCgqQgUAIgSgCQgWgCgKgPQgIgNADgZQADgaANgfQAHgUARgiIBqjbQgUADgYATQgkAdgGADQgbAQgdgBQgYAAgKgOQgJgKADgSQACgOAPgbQAxhWAcgqQAthFAtgxQAMgOAkgkQAfgfAQgTQAPgRAIgFIAIgEIADgPQAJghAegzQAjg8AJgWQALgZAJgpIAPhDQAJgjAhhaQAxiIA1jNQAShGgBglIAAgYIABgGIgBgIQAAgMAMgZQAMgaAQguQASg2AJgUQAOggAohNQAZgvAbg5QARgmABgXIgBgVQgBgOABgIIABgBIgBgPQAAhdAGhbIAFgyQAIg5Ath2QArhwAGg/IACgMQgFgGgDgIQgCgIAAgLIAAgTQAAgVgIghIgNg3QgDgUABgcIABgwIgBgvQgBgbAEgTQAGghAcgzQAnhGAkgpQBYhhC5g7IAOgEIAGgDQA7geAhgLIAfgLQASgGALgGQAigQALgbQAEgJAEgZQAOhigBgyQgBgNABgJIgDgDQglgfgSgTQgcgfgNgeQgOghgEg5QgFhLgEgSQgDgPgIgdQgJgegDgPQgGggAAhCQABivAKilIABgLQgGgIgCgPQgHg2AXhHQAhhiBKhPQBIhNBggtQBdgrBqgJQBogJBnAYQA0AMAjASQAiARA2AtQBQBGAvA9QA8BPARBSIACAKIADAJQAYBZgTBzQgMBHgqCCQglBygYA3IgbA/QgQAkgIAbQgLAogDAzQgCAdAABAQABA/AGAjQAJA2AbAiQAbAkA1AWQAgAOBCAQQCqApBkAoQCQA7BcBWQA9A7ArBLQAqBLASBTQAEAPgCAMIgCAFQAHCdgQCbQgFAqAAATQgBAjAGAaQAJAiAfAvQAqA/AHAPQAPAeANAsIATBNQAVBOAtBvIBNC4QBKDCAfDSQADAXgGAMIABABQAGALgBAYQgBBOgOERQgLDaACCGIACBoQABA8gDArQgDAsgKBIQgNBXgDAcQgIBIgDCgIAAATIAAA4IACABQgBARACAYIAFAyIAFAuIAEAYIABAUQgBAOgFAdIAAAxIAAAMQgEAYgGAVIgPAzQgJAfgEAUQgDAWgBAeIAAA0QgBBogcCXQgNBMgTAnQgWAwgiARQgVAKgWgCQgYgDgPgPQgcAWgWAEQgPADgOgEQgPgEgHgMQgKgNACgcQADgigDgKQgHAdgfANQgfANgagPQgVgNgKgaQgJgXAAgdQAAgOAFgmQADghgBgTQgWAEgOgCQgUgCgKgMQgLgLgDgbQgEgoAJgzIAUhZQAch2APh5IADgPIgBAAIAAgBQAJhcACgvIAEi6QAChvAOhKQAEgRAIgLIABgBQgBgJABgNQAMjMgrjGIgVhcQgNg4gGglQgJg9gFhtIgdpdQAAgPACgLQgGgIgIgPIjXnPIgDANIgIApQgFAVgKAdIgSAyQgPAvgWBnIgqDPQgMBCgGAoQgIA7AAAxQAACKBHDSIA8CqQAhBkAQBKQATBcAJCTQAMDQgKC0IgCAbIgDAAIgFArIgFAtIgGArQgCAQAAAcIAAArIgFAvIgFAuIABBsQABBBgPAoQgGAPgIAGIgCAJIgSA7QgNAngFAVQgMAugKBVQgLBcgJAnIgUBQQgFAagLBLQgIBAgLAkQgFATgLAEQgIAEgJgFQgJgFgEgJQgEgMAEgaIBPnFQAMhFAJgiIAYhOIAMgwIAKAAIABgFQAMgngBg/QgChaABgOIAFgqIAEgpQABgNAAgYIAAgmQABgWAIg0QAHgwAAgbQAAgVAEgIIABgBIABghQAJi2gJiKQgKisgmiPQgQg9gfhbIg0iXQg8i2gCh/QgBgzAJhPQAUinBCkBQAdhuAShCIAZhaQAJghATgDQAMgBAJALQAGAHACAJQAHAHAHATIA8CPQAvBvAbA3IA0BiQAfA7ARAoQANAbgIAOQADAKAAAMIAeJrQAFBcAFAkQAFAiALAvIATBPQA0DpgPDwQgBAWgJAJIgCAOQgMBFgDBfIgEClQgCBdgHBNIgEAAQACAKgCARQgLBogmCdQgNAygDAVQgGApAFAgQANgEAHgQIAKgeQANgxAkgkQAOgNALABQAIAAAHAHQAGAGACAJQADANgHAWIgkCCQgTBLAEA4QABAMAGAJQAGALAKgDQAIgCAGgQIBHjRQAOgmAWAEQALADAFANQAEAMgBAPQgCAZgTBtQgOBSAHA0QAKACAJgIQAJgHADgLIADgVQABgPACgGQACgLAHgIQAIgJAKgBQAJAAAHAHQAHAGADAJQADAMgFAZQgEATAHAIQAHAIANgFQALgEAIgLQAPgUALgeQAHgVAIgiQAgiVABiYIAAgyQABgcAEgVQAEgRAJgcIAPgtQAHgaAFgeQAFgiABgoIAAgoIgKgiQgIglgDguQgCgdABg1IAAgCQAAiRAIhIIASh7QAPhtABidQgBixAChZQABhcAKi5IANjxIgBgBQgCgEAAgGQgBgLAGgGIAAgBIABgEIgFgGQgHgMgCgcQgNidhbjoIhMi9QgrhvgXhSIgShCQgMgmgNgaQgHgOgshFQgggygIgkQgGgaABgiQAAgQAFgrQAPibgEigQgBgQACgLIgDgLQgShHgkhAQglhAg0gzQhUhRiEg3QhegoiaglQhZgWgqgSQhGgegigxQgdgogLg+QgGgogBhIQAAhBACgiQADg3ALgrQAKgnAWg0IAnhZQAWg2AjhxQAehgAIg0QAOhTgPhCIgEgXIgBgNIgFgNIgGgUQgQhAgxg/QglgvhCg5QgqglgdgRQgqgZhAgQQhogZhtARQhsARhcA4QhZA2g4BUQg7BZgCBgQAAAVgDALIADAEQAGAMgCAZQgJClgCClQgBAvAFAaQADAOAIAYIALAlQAHAcADAoIADBFQAEAmAJAUQALAXAeAcQAoAkAIALQAJAMADAMQABAFgBAEQACAIAAAMQABAagCAgIgJBeQgDAcgEAOQgOArgwAiQgSANgQAGIgqANQgSAGgeAOIggAPQgRAIgHABQgLAGgSAGQizA5hOBqQgTAagXAsQgVAjgHAYQgHAVgBAcQgBAQAAAhQABA0ADAZQADASAKA0QAJArABAaQABAOgCAJIABACQAHALgBAVQgBAcgQA3QgbBZggBRQgOAlgDALQgFATgFAnQgMBnACBqQAAARgCAJIABAGQAGAogPAwQgKAhgbAzIgvBaQgbA1gQAoIgXBEQgOArgLAZIgDAGIAAABQAAA8gPBJQgKAtgYBUIgvCeQgQA1gKAaIgYBDQgPAogHAbIgKA1QgGAggHAVQgLAfgjA7QgiA6gLAhQgGARgEAHQgGAJgIACIAAABQgFAMgQARIhIBHQgpAqgaAhQgdAngrBKIgqBIQAkgYAegXQAVgRAOgGQAVgJARAGQANAFAJAPQAHANABAQQAAAVgQAlQgLAZg4B0QgrBVgTA6IBBg/QAYgWAOAFQAKADAEALQAEAKgCALQgCAIgGALIgLARQgYApABAuQAcgEAXgRQAXgRANgZIAOgdQALgRAQACQAKAAAHAKQAHAKAAALQABAOgOAbQASgCANghQAOghATABQAQAAAIARQAFALABAUQAGADAGgGQAFgFACgIQAXhKAihHQAFgJAFgHQABgHADgIIALgTQASgiAMg1IARhbQAMg1AghbQCbmrDxmDQAJgPAKgIQAOgKAMAGQAPAGABAiIABDOQAABVgGAqQgCARgMBBQgYCCgNCjQgIBlgIDBQgGCNABBHQAEDtBKE2QAEAHADAKQAJAaAHAjIALA9IAXCIIAKA8QAPBYAKAsQATBbADAXIAHAqQAEAOAIAVIAYBBQAGATgBAKQgCATgPAEIgHACQgLAAgJgLg");
	this.shape.setTransform(181,306.6);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(19).to({_off:false},0).to({_off:true},9).wait(9));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF3D4E").s().p("EgJKAxVQgHgHgHgUQgbhOgHgjIgKg6IgWhZQgIghgHhBQgIhDgHggQgQg6gFgeIgLhOQgHgvgKgdQgGgSgCgIIgBAAQhWlIAGlVQAChLAGhoQAPkOAOh4QAHg7AHgrQAShTAFgqQAGgrgChLQgBhWACggQhfCXhXC3QhKCahPDEQggBRgNAsQgNAxgVBkQgVBYgfA0IgBAEQgBAJgGAKIgLASQgJASgIAaIgNAuQgPAzgZAVQgRAOgXAAQgXABgNgPQgJAGgKAMIgRAVQgWAZgWABQgNAAgLgKQgKgLAEgMQgmAbgaAFQgSAEgSgEQgTgFgLgNQgPgSgCgqQgUAIgSgCQgWgCgKgPQgIgNADgZQADgaANgfQAHgUARgiIBqjbQgUADgYATQgkAdgGADQgbAQgdgBQgYAAgKgOQgJgKADgSQACgOAPgbQAxhWAcgqQAthFAtgxQAMgOAkgkQAfgfAQgTQAPgRAIgFIAIgEIADgPQAJghAegzQAjg8AJgWQALgZAJgpIAPhDQAJgjAhhaQAxiIA1jNQAShGgBglIAAgYIABgGIgBgIQAAgMAMgZQAMgaAQguQASg2AJgUQAOggAohNQAZgvAbg5QARgmABgXIgBgVQgBgOABgIIABgBIgBgPQAAhdAGhbIAFgyQAIg5Ath2QArhwAGg/IACgMQgFgGgDgIQgCgIAAgLIAAgTQAAgVgIghIgNg3QgDgUABgcIABgwIgBgvQgBgbAEgTQAGghAcgzQAnhGAkgpQBYhhC5g7IAOgEIAGgDQA7geAhgLIAfgLQASgGALgGQAigQALgbQAEgJAEgZQAOhigBgyQgBgNABgJIgDgDQglgfgSgTQgcgfgNgeQgOghgEg5QgFhLgEgSQgDgPgIgdQgJgegDgPQgGggAAhCQABivAKilIABgLQgGgIgCgPQgHg2AXhHQAhhiBKhPQBIhNBggtQBdgrBqgJQBogJBnAYQA0AMAjASQAiARA2AtQBQBGAvA9QA8BPARBSIACAKIADAJQAYBZgTBzQgMBHgqCCQglBygYA3IgbA/QgQAkgIAbQgLAogDAzQgCAdAABAQABA/AGAjQAJA2AbAiQAbAkA1AWQAgAOBCAQQCqApBkAoQCQA7BcBWQA9A7ArBLQAqBLASBTQAEAPgCAMIgCAFQAHCdgQCbQgFAqAAATQgBAjAGAaQAJAiAfAvQAqA/AHAPQAPAeANAsIATBNQAVBOAtBvIBNC4QBKDCAfDSQADAXgGAMIABABQAGALgBAYQgBBOgOERQgLDaACCGIACBoQABA8gDArQgDAsgKBIQgNBXgDAcQgIBIgDCgIAAATIAAA4IACABQgBARACAYIAFAyIAFAuIAEAYIABAUQgBAOgFAdIAAAxIAAAMQgEAYgGAVIgPAzQgJAfgEAUQgDAWgBAeIAAA0QgBBogcCXQgNBMgTAnQgWAwgiARQgVAKgWgCQgYgDgPgPQgcAWgWAEQgPADgOgEQgPgEgHgMQgKgNACgcQADgigDgKQgHAdgfANQgfANgagPQgVgNgKgaQgJgXAAgdQAAgOAFgmQADghgBgTQgWAEgOgCQgUgCgKgMQgLgLgDgbQgEgoAJgzIAUhZQAch2APh5IADgPIgBAAIAAgBQAJhcACgvIAEi6QAChvAOhKQAEgRAIgLIABgBQgBgJABgNQAMjMgrjGIgVhcQgNg4gGglQgJg9gFhtIgdpdQAAgPACgLQgGgIgIgPIjXnPIgDANIgIApQgFAVgKAdIgSAyQgPAvgWBnIgqDPQgMBCgGAoQgIA7AAAxQAACKBHDSIA8CqQAhBkAQBKQATBcAJCTQAMDQgKC0IgCAbIgDAAIgFArIgFAtIgGArQgCAQAAAcIAAArIgFAvIgFAuIABBsQABBBgPAoQgGAPgIAGIgCAJIgSA7QgNAngFAVQgMAugKBVQgLBcgJAnIgUBQQgFAagLBLQgIBAgLAkQgFATgLAEQgIAEgJgFQgJgFgEgJQgEgMAEgaIBPnFQAMhFAJgiIAYhOIAMgwIAKAAIABgFQAMgngBg/QgChaABgOIAFgqIAEgpQABgNAAgYIAAgmQABgWAIg0QAHgwAAgbQAAgVAEgIIABgBIABghQAJi2gJiKQgKisgmiPQgQg9gfhbIg0iXQg8i2gCh/QgBgzAJhPQAUinBCkBQAdhuAShCIAZhaQAJghATgDQAMgBAJALQAGAHACAJQAHAHAHATIA8CPQAvBvAbA3IA0BiQAfA7ARAoQANAbgIAOQADAKAAAMIAeJrQAFBcAFAkQAFAiALAvIATBPQA0DpgPDwQgBAWgJAJIgCAOQgMBFgDBfIgEClQgCBdgHBNIgEAAQACAKgCARQgLBogmCdQgNAygDAVQgGApAFAgQANgEAHgQIAKgeQANgxAkgkQAOgNALABQAIAAAHAHQAGAGACAJQADANgHAWIgkCCQgTBLAEA4QABAMAGAJQAGALAKgDQAIgCAGgQIBHjRQAOgmAWAEQALADAFANQAEAMgBAPQgCAZgTBtQgOBSAHA0QAKACAJgIQAJgHADgLIADgVQABgPACgGQACgLAHgIQAIgJAKgBQAJAAAHAHQAHAGADAJQADAMgFAZQgEATAHAIQAHAIANgFQALgEAIgLQAPgUALgeQAHgVAIgiQAgiVABiYIAAgyQABgcAEgVQAEgRAJgcIAPgtQAHgaAFgeQAFgiABgoIAAgoIgKgiQgIglgDguQgCgdABg1IAAgCQAAiRAIhIIASh7QAPhtABidQgBixAChZQABhcAKi5IANjxIgBgBQgCgEAAgGQgBgLAGgGIAAgBIABgEIgFgGQgHgMgCgcQgNidhbjoIhMi9QgrhvgXhSIgShCQgMgmgNgaQgHgOgshFQgggygIgkQgGgaABgiQAAgQAFgrQAPibgEigQgBgQACgLIgDgLQgShHgkhAQglhAg0gzQhUhRiEg3QhegoiaglQhZgWgqgSQhGgegigxQgdgogLg+QgGgogBhIQAAhBACgiQADg3ALgrQAKgnAWg0IAnhZQAWg2AjhxQAehgAIg0QAOhTgPhCIgEgXIgBgNIgFgNIgGgUQgQhAgxg/QglgvhCg5QgqglgdgRQgqgZhAgQQhogZhtARQhsARhcA4QhZA2g4BUQg7BZgCBgQAAAVgDALIADAEQAGAMgCAZQgJClgCClQgBAvAFAaQADAOAIAYIALAlQAHAcADAoIADBFQAEAmAJAUQALAXAeAcQAoAkAIALQAJAMADAMQABAFgBAEQACAIAAAMQABAagCAgIgJBeQgDAcgEAOQgOArgwAiQgSANgQAGIgqANQgSAGgeAOIggAPQgRAIgHABQgLAGgSAGQizA5hOBqQgTAagXAsQgVAjgHAYQgHAVgBAcQgBAQAAAhQABA0ADAZQADASAKA0QAJArABAaQABAOgCAJIABACQAHALgBAVQgBAcgQA3QgbBZggBRQgOAlgDALQgFATgFAnQgMBnACBqQAAARgCAJIABAGQAGAogPAwQgKAhgbAzIgvBaQgbA1gQAoIgXBEQgOArgLAZIgDAGIAAABQAAA8gPBJQgKAtgYBUIgvCeQgQA1gKAaIgYBDQgPAogHAbIgKA1QgGAggHAVQgLAfgjA7QgiA6gLAhQgGARgEAHQgGAJgIACIAAABQgFAMgQARIhIBHQgpAqgaAhQgdAngrBKIgqBIQAkgYAegXQAVgRAOgGQAVgJARAGQANAFAJAPQAHANABAQQAAAVgQAlQgLAZg4B0QgrBVgTA6IBBg/QAYgWAOAFQAKADAEALQAEAKgCALQgCAIgGALIgLARQgYApABAuQAcgEAXgRQAXgRANgZIAOgdQALgRAQACQAKAAAHAKQAHAKAAALQABAOgOAbQASgCANghQAOghATABQAQAAAIARQAFALABAUQAGADAGgGQAFgFACgIQAXhKAihHQAFgJAFgHQABgHADgIIALgTQASgiAMg1IARhbQAMg1AghbQCbmrDxmDQAJgPAKgIQAOgKAMAGQAPAGABAiIABDOQAABVgGAqQgCARgMBBQgYCCgNCjQgIBlgIDBQgGCNABBHQAEDtBKE2QAEAHADAKQAJAaAHAjIALA9IAXCIIAKA8QAPBYAKAsQATBbADAXIAHAqQAEAOAIAVIAYBBQAGATgBAKQgCATgPAEIgHACQgLAAgJgLg");
	this.shape_1.setTransform(181,306.6);
	this.shape_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1).to({_off:false},0).to({_off:true},10).wait(26));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.animacijagumbacopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1 copy
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(4,1,1).p("ACCAAQAAA2gmAmQgmAmg2AAQg1AAgmgmQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1g");
	this.shape.setTransform(13,13);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(14));

	// Layer 1 copy 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#09C0C2").ss(4,1,1).p("ACCAAQAAA2gmAmQgmAmg2AAQg1AAgmgmQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1g");
	this.shape_1.setTransform(13,13);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(9,192,194,0.992)").ss(4,1,1).p("AiCAAQAAg2AmgmQAmgmA2AAQA2AAAnAmQAmAmAAA2QAAA2gmAnQgnAmg2AAQg2AAgmgmQgmgnAAg2g");
	this.shape_2.setTransform(13,13);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(9,192,194,0.976)").ss(4,1,1).p("AiFAAQAAg3AngnQAngnA3AAQA4AAAnAnQAnAnAAA3QAAA4gnAnQgnAng4AAQg3AAgngnQgngnAAg4g");
	this.shape_3.setTransform(13,13);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("rgba(9,192,194,0.945)").ss(4,1,1).p("AiLAAQAAg5ApgpQApgpA5AAQA6AAApApQApApAAA5QAAA6gpApQgpApg6AAQg5AAgpgpQgpgpAAg6g");
	this.shape_4.setTransform(13,13);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("rgba(9,192,194,0.906)").ss(4,1,1).p("AiSAAQAAg8ArgrQArgrA8AAQA9AAArArQArArAAA8QAAA9grArQgrArg9AAQg8AAgrgrQgrgrAAg9g");
	this.shape_5.setTransform(13,13);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("rgba(9,192,194,0.851)").ss(4,1,1).p("AicAAQAAhAAuguQAuguBAAAQBBAAAuAuQAuAuAABAQAABBguAuQguAuhBAAQhAAAguguQguguAAhBg");
	this.shape_6.setTransform(13,13);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("rgba(9,192,194,0.788)").ss(4,1,1).p("AinAAQAAhFAxgxQAxgxBFAAQBGAAAxAxQAxAxAABFQAABGgxAxQgxAxhGAAQhFAAgxgxQgxgxAAhGg");
	this.shape_7.setTransform(13,13);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("rgba(9,192,194,0.71)").ss(4,1,1).p("Ai1AAQAAhLA1g1QA1g1BLAAQBMAAA1A1QA1A1AABLQAABMg1A1Qg1A1hMAAQhLAAg1g1Qg1g1AAhMg");
	this.shape_8.setTransform(13,13);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(9,192,194,0.62)").ss(4,1,1).p("AjEAAQAAhRA5g6QA6g5BRAAQBSAAA5A5QA6A6AABRQAABSg6A5Qg5A6hSAAQhRAAg6g6Qg5g5AAhSg");
	this.shape_9.setTransform(13,13);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("rgba(9,192,194,0.522)").ss(4,1,1).p("AjWAAQAAhYA/g/QA/g/BYAAQBZAAA/A/QA/A/AABYQAABZg/A/Qg/A/hZAAQhYAAg/g/Qg/g/AAhZg");
	this.shape_10.setTransform(13,13);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("rgba(9,192,194,0.408)").ss(4,1,1).p("AjqAAQAAhhBEhFQBFhEBhAAQBiAABEBEQBFBFAABhQAABihFBEQhEBFhiAAQhhAAhFhFQhEhEAAhig");
	this.shape_11.setTransform(13,13);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("rgba(9,192,194,0.282)").ss(4,1,1).p("AkAAAQAAhqBLhLQBLhLBqAAQBrAABLBLQBLBLAABqQAABrhLBLQhLBLhrAAQhqAAhLhLQhLhLAAhrg");
	this.shape_12.setTransform(13,13);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("rgba(9,192,194,0.149)").ss(4,1,1).p("AkYAAQAAh0BShSQBShSB0AAQB1AABSBSQBSBSAAB0QAAB1hSBSQhSBSh1AAQh0AAhShSQhShSAAh1g");
	this.shape_13.setTransform(13,13);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("rgba(9,192,194,0)").ss(4,1,1).p("AE0AAQAACAhaBaQhaBaiAAAQh/AAhahaQhahaAAiAQAAh/BahaQBahaB/AAQCAAABaBaQBaBaAAB/g");
	this.shape_14.setTransform(13,13);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).wait(1));

	// Layer 1
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#09C0C2").s().p("AhbBcQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1QAAA2gmAmQgmAmg2AAQg1AAgmgmg");
	this.shape_15.setTransform(13,13);

	this.timeline.addTween(cjs.Tween.get(this.shape_15).wait(14));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,30,30);


(lib.animacijagumba = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1 copy
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF3D4E").ss(4,1,1).p("ACCAAQAAA2gmAmQgmAmg2AAQg1AAgmgmQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1g");
	this.shape.setTransform(13,13);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(14));

	// Layer 1 copy 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FF3D4E").ss(4,1,1).p("ACCAAQAAA2gmAmQgmAmg2AAQg1AAgmgmQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1g");
	this.shape_1.setTransform(13,13);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(255,61,78,0.992)").ss(4,1,1).p("AiCAAQAAg2AmgmQAmgmA2AAQA2AAAnAmQAmAmAAA2QAAA2gmAnQgnAmg2AAQg2AAgmgmQgmgnAAg2g");
	this.shape_2.setTransform(13,13);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(255,61,78,0.976)").ss(4,1,1).p("AiFAAQAAg3AngnQAngnA3AAQA4AAAnAnQAnAnAAA3QAAA4gnAnQgnAng4AAQg3AAgngnQgngnAAg4g");
	this.shape_3.setTransform(13,13);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("rgba(255,61,78,0.945)").ss(4,1,1).p("AiLAAQAAg5ApgpQApgpA5AAQA6AAApApQApApAAA5QAAA6gpApQgpApg6AAQg5AAgpgpQgpgpAAg6g");
	this.shape_4.setTransform(13,13);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("rgba(255,61,78,0.906)").ss(4,1,1).p("AiSAAQAAg8ArgrQArgrA8AAQA9AAArArQArArAAA8QAAA9grArQgrArg9AAQg8AAgrgrQgrgrAAg9g");
	this.shape_5.setTransform(13,13);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("rgba(255,61,78,0.851)").ss(4,1,1).p("AicAAQAAhAAuguQAuguBAAAQBBAAAuAuQAuAuAABAQAABBguAuQguAuhBAAQhAAAguguQguguAAhBg");
	this.shape_6.setTransform(13,13);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("rgba(255,61,78,0.788)").ss(4,1,1).p("AinAAQAAhFAxgxQAxgxBFAAQBGAAAxAxQAxAxAABFQAABGgxAxQgxAxhGAAQhFAAgxgxQgxgxAAhGg");
	this.shape_7.setTransform(13,13);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("rgba(255,61,78,0.71)").ss(4,1,1).p("Ai1AAQAAhLA1g1QA1g1BLAAQBMAAA1A1QA1A1AABLQAABMg1A1Qg1A1hMAAQhLAAg1g1Qg1g1AAhMg");
	this.shape_8.setTransform(13,13);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(255,61,78,0.62)").ss(4,1,1).p("AjEAAQAAhRA5g6QA6g5BRAAQBSAAA5A5QA6A6AABRQAABSg6A5Qg5A6hSAAQhRAAg6g6Qg5g5AAhSg");
	this.shape_9.setTransform(13,13);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("rgba(255,61,78,0.522)").ss(4,1,1).p("AjWAAQAAhYA/g/QA/g/BYAAQBZAAA/A/QA/A/AABYQAABZg/A/Qg/A/hZAAQhYAAg/g/Qg/g/AAhZg");
	this.shape_10.setTransform(13,13);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("rgba(255,61,78,0.408)").ss(4,1,1).p("AjqAAQAAhhBEhFQBFhEBhAAQBiAABEBEQBFBFAABhQAABihFBEQhEBFhiAAQhhAAhFhFQhEhEAAhig");
	this.shape_11.setTransform(13,13);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("rgba(255,61,78,0.282)").ss(4,1,1).p("AkAAAQAAhqBLhLQBLhLBqAAQBrAABLBLQBLBLAABqQAABrhLBLQhLBLhrAAQhqAAhLhLQhLhLAAhrg");
	this.shape_12.setTransform(13,13);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("rgba(255,61,78,0.149)").ss(4,1,1).p("AkYAAQAAh0BShSQBShSB0AAQB1AABSBSQBSBSAAB0QAAB1hSBSQhSBSh1AAQh0AAhShSQhShSAAh1g");
	this.shape_13.setTransform(13,13);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("rgba(255,61,78,0)").ss(4,1,1).p("AE0AAQAACAhaBaQhaBaiAAAQh/AAhahaQhahaAAiAQAAh/BahaQBahaB/AAQCAAABaBaQBaBaAAB/g");
	this.shape_14.setTransform(13,13);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).wait(1));

	// Layer 1
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF3D4E").s().p("AhbBcQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1QAAA2gmAmQgmAmg2AAQg1AAgmgmg");
	this.shape_15.setTransform(13,13);

	this.timeline.addTween(cjs.Tween.get(this.shape_15).wait(14));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,30,30);


(lib.___Camera___ = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.visible = false;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// viewfinder
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,0,0,0)").ss(2,1,1,3,true).p("EAq+AfQMhV7AAAMAAAg+fMBV7AAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(2));

}).prototype = p = new cjs.MovieClip();


(lib.gumbiczeleni = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CCCCCC").ss(4,1,1).p("ACCAAQAAA2gmAmQgmAmg2AAQg1AAgmgmQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1g");
	this.shape.setTransform(23.1,23.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#09C0C2").s().p("AhbBcQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1QAAA2gmAmQgmAmg2AAQg1AAgmgmg");
	this.shape_1.setTransform(23.1,23.1);

	this.instance = new lib.animacijagumbacopy();
	this.instance.parent = this;
	this.instance.setTransform(23.1,23.1,1,1,0,0,0,13,13);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.instance}]},1).wait(3));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.2)").s().p("AjFDGQhShSAAh0QAAhzBShSQBShSBzAAQB0AABSBSQBSBSAABzQAAB0hSBSQhSBSh0AAQhzAAhShSg");
	this.shape_2.setTransform(23.8,22.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4.2,-5.5,56,56);


(lib.gumbic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CCCCCC").ss(4,1,1).p("ACCAAQAAA2gmAmQgmAmg2AAQg1AAgmgmQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1g");
	this.shape.setTransform(23.1,23.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF3D4E").s().p("AhbBcQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1QAAA2gmAmQgmAmg2AAQg1AAgmgmg");
	this.shape_1.setTransform(23.1,23.1);

	this.instance = new lib.animacijagumba();
	this.instance.parent = this;
	this.instance.setTransform(23.1,23.1,1,1,0,0,0,13,13);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.instance}]},1).wait(3));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.2)").s().p("AjFDGQhShSAAh0QAAhzBShSQBShSBzAAQB0AABSBSQBSBSAABzQAAB0hSBSQhSBSh0AAQhzAAhShSg");
	this.shape_2.setTransform(23.8,22.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4.2,-5.5,56,56);


(lib.asd = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var frequency = 24;
		semafor.stage.enableMouseOver(frequency);


		this.gumb1.addEventListener('click', function () {
		    var event = new CustomEvent('symptom_click', { detail: { gumb: 1 } });
		    semafor.canvas.dispatchEvent(event);
		});

		this.gumb2.addEventListener('click', function () {
            var event = new CustomEvent('symptom_click', { detail: { gumb: 2 } });
            semafor.canvas.dispatchEvent(event);
		});


		this.gumb3.addEventListener('click', function () {
            var event = new CustomEvent('symptom_click', { detail: { gumb: 3 } });
            semafor.canvas.dispatchEvent(event);

		});

		this.gumb4.addEventListener('click', function () {
            var event = new CustomEvent('symptom_click', { detail: { gumb: 4 } });
            semafor.canvas.dispatchEvent(event);
		});

		this.gumb5.addEventListener('click', function () {
            var event = new CustomEvent('symptom_click', { detail: { gumb: 5 } });
            semafor.canvas.dispatchEvent(event);
		});

		this.gumb6.addEventListener('click', function () {
            var event = new CustomEvent('symptom_click', { detail: { gumb: 6 } });
            semafor.canvas.dispatchEvent(event);
		});
		this.gumb7.addEventListener('click', function () {
            var event = new CustomEvent('symptom_click', { detail: { gumb: 7 } });
            semafor.canvas.dispatchEvent(event);
		});

		this.gumb8.addEventListener('click', function () {
            var event = new CustomEvent('symptom_click', { detail: { gumb: 8 } });
            semafor.canvas.dispatchEvent(event);
		});

		this.gumb9.addEventListener('click', function () {
            var event = new CustomEvent('symptom_click', { detail: { gumb: 9 } });
            semafor.canvas.dispatchEvent(event);
		});

		this.gumb10.addEventListener('click', function () {
            var event = new CustomEvent('symptom_click', { detail: { gumb: 10 } });
            semafor.canvas.dispatchEvent(event);
		});

		this.gumb11.addEventListener('click', function () {
            var event = new CustomEvent('symptom_click', { detail: { gumb: 11 } });
            semafor.canvas.dispatchEvent(event);
		});

		this.gumb12.addEventListener('click', function () {
            var event = new CustomEvent('symptom_click', { detail: { gumb: 12 } });
            semafor.canvas.dispatchEvent(event);
		});





		this.gumb2.addEventListener('mouseover', function () {
			semafor.exportRoot.kontent.bol.gotoAndPlay(1);
		});

		this.gumb2.addEventListener('mouseout', function () {
			semafor.exportRoot.kontent.bol.gotoAndStop(0);
		});



		this.gumb3.addEventListener('mouseover', function () {
			semafor.exportRoot.kontent.drhtavica.gotoAndPlay(1);
		});

		this.gumb3.addEventListener('mouseout', function () {
			semafor.exportRoot.kontent.drhtavica.gotoAndStop(0);
		});
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// centar!
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AurNhIAA7BIdXAAIAAbBg");
	this.shape.setTransform(94,86.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// LEGENDA
	this.text = new cjs.Text("1. Vrućica\n\n2. Bol u mišićima\n\n3. Drhtavica\n\n4. Umor, slabost\n\n5. Iscrpljenost\n\n6. Kašalj\n\n7. Kihanje \n\n8. Začepljen nos\n\n9. Bolno grlo\n\n---------------------------\n\n10. pocetak bolesti\n\n11.trajanje bolesti\n\n12. komplikacije", "12px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 15;
	this.text.lineWidth = 135;
	this.text.parent = this;
	this.text.setTransform(-389.4,-176);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// tocke
	this.gumb12 = new lib.gumbiczeleni();
	this.gumb12.parent = this;
	this.gumb12.setTransform(125.3,28);
	new cjs.ButtonHelper(this.gumb12, 0, 1, 2, false, new lib.gumbiczeleni(), 3);

	this.gumb11 = new lib.gumbiczeleni();
	this.gumb11.parent = this;
	this.gumb11.setTransform(-182.7,-87);
	new cjs.ButtonHelper(this.gumb11, 0, 1, 2, false, new lib.gumbiczeleni(), 3);

	this.gumb10 = new lib.gumbiczeleni();
	this.gumb10.parent = this;
	this.gumb10.setTransform(-50.7,107.6);
	new cjs.ButtonHelper(this.gumb10, 0, 1, 2, false, new lib.gumbiczeleni(), 3);

	this.gumb9 = new lib.gumbic();
	this.gumb9.parent = this;
	this.gumb9.setTransform(75.1,-57.8);
	new cjs.ButtonHelper(this.gumb9, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb8 = new lib.gumbic();
	this.gumb8.parent = this;
	this.gumb8.setTransform(66.1,-126.8);
	new cjs.ButtonHelper(this.gumb8, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb7 = new lib.gumbic();
	this.gumb7.parent = this;
	this.gumb7.setTransform(26.1,-125.8);
	new cjs.ButtonHelper(this.gumb7, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb6 = new lib.gumbic();
	this.gumb6.parent = this;
	this.gumb6.setTransform(26.1,13.2);
	new cjs.ButtonHelper(this.gumb6, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb5 = new lib.gumbic();
	this.gumb5.parent = this;
	this.gumb5.setTransform(322.6,-172.3);
	new cjs.ButtonHelper(this.gumb5, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb4 = new lib.gumbic();
	this.gumb4.parent = this;
	this.gumb4.setTransform(-65.7,-179.3);
	new cjs.ButtonHelper(this.gumb4, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb3 = new lib.gumbic();
	this.gumb3.parent = this;
	this.gumb3.setTransform(253.1,-67.8);
	new cjs.ButtonHelper(this.gumb3, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb2 = new lib.gumbic();
	this.gumb2.parent = this;
	this.gumb2.setTransform(-35.4,-4.8);
	new cjs.ButtonHelper(this.gumb2, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb1 = new lib.gumbic();
	this.gumb1.parent = this;
	this.gumb1.setTransform(98.6,-160.3,1,1,0,0,0,23,23);
	new cjs.ButtonHelper(this.gumb1, 0, 1, 2, false, new lib.gumbic(), 3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.gumb1},{t:this.gumb2},{t:this.gumb3},{t:this.gumb4},{t:this.gumb5},{t:this.gumb6},{t:this.gumb7},{t:this.gumb8},{t:this.gumb9},{t:this.gumb10},{t:this.gumb11},{t:this.gumb12}]}).wait(1));

	// text
	this.text_1 = new cjs.Text("BOLNO GRLO", "12px 'Arial'", "#FFFFFF");
	this.text_1.lineHeight = 15;
	this.text_1.lineWidth = 88;
	this.text_1.parent = this;
	this.text_1.setTransform(135.8,-43);

	this.text_2 = new cjs.Text("KIHANJE", "12px 'Arial'", "#FFFFFF");
	this.text_2.lineHeight = 15;
	this.text_2.lineWidth = 79;
	this.text_2.parent = this;
	this.text_2.setTransform(-35.4,-70);

	this.text_3 = new cjs.Text("ZAČEPLJEN NOS", "12px 'Arial'", "#FFFFFF");
	this.text_3.lineHeight = 15;
	this.text_3.lineWidth = 128;
	this.text_3.parent = this;
	this.text_3.setTransform(181.1,-112.5);

	this.text_4 = new cjs.Text("VRUĆICA", "12px 'Arial'", "#FFFFFF");
	this.text_4.lineHeight = 15;
	this.text_4.lineWidth = 128;
	this.text_4.parent = this;
	this.text_4.setTransform(137.1,-147);

	this.text_5 = new cjs.Text("KAŠALJ", "12px 'Arial'", "#FFFFFF");
	this.text_5.lineHeight = 15;
	this.text_5.lineWidth = 128;
	this.text_5.parent = this;
	this.text_5.setTransform(80.1,1);

	this.text_6 = new cjs.Text("KOMPLIKACIJE", "12px 'Arial'", "#FFFFFF");
	this.text_6.lineHeight = 15;
	this.text_6.lineWidth = 128;
	this.text_6.parent = this;
	this.text_6.setTransform(182.1,65.5);

	this.text_7 = new cjs.Text("TRAJANJE BOLESTI", "12px 'Arial'", "#FFFFFF");
	this.text_7.lineHeight = 15;
	this.text_7.lineWidth = 128;
	this.text_7.parent = this;
	this.text_7.setTransform(-134.4,-111.4);

	this.text_8 = new cjs.Text("POČETAK BOLESTI", "12px 'Arial'", "#FFFFFF");
	this.text_8.lineHeight = 15;
	this.text_8.lineWidth = 112;
	this.text_8.parent = this;
	this.text_8.setTransform(-187.4,65.1);

	this.text_9 = new cjs.Text("BOL U MIŠIĆIMA", "12px 'Arial'", "#FFFFFF");
	this.text_9.lineHeight = 15;
	this.text_9.lineWidth = 112;
	this.text_9.parent = this;
	this.text_9.setTransform(-124.6,-18.1);

	this.text_10 = new cjs.Text("DRHTAVICA", "12px 'Arial'", "#FFFFFF");
	this.text_10.lineHeight = 15;
	this.text_10.lineWidth = 112;
	this.text_10.parent = this;
	this.text_10.setTransform(296.8,-83.1);

	this.text_11 = new cjs.Text("ISCRPLJENOST", "12px 'Arial'", "#FFFFFF");
	this.text_11.lineHeight = 15;
	this.text_11.lineWidth = 112;
	this.text_11.parent = this;
	this.text_11.setTransform(230.1,-190.1);

	this.text_12 = new cjs.Text("UMOR, SLABOST", "12px 'Arial'", "#FFFFFF");
	this.text_12.lineHeight = 15;
	this.text_12.lineWidth = 112;
	this.text_12.parent = this;
	this.text_12.setTransform(-163.2,-194.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(1,1,1).p("ABP1TIFQDNABKhsIFHAFAzokaIH1AAIFdn+AdIjrIDIjIIG4AAIEAAAEAnLgUgIi8i8IuHAAAADssINSAAAmVJPID3kpAJILlIEaDmA0108IjIjIIvZAAEgm/gGTID6ksISIAAAwGGJIivivInDAAIndAAAycYFInbnbIxQAA");
	this.shape_1.setTransform(89.7,-24.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.text_12},{t:this.text_11},{t:this.text_10},{t:this.text_9},{t:this.text_8},{t:this.text_7},{t:this.text_6},{t:this.text_5},{t:this.text_4},{t:this.text_3},{t:this.text_2},{t:this.text_1}]}).wait(1));

	// kvartati za tekst
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(1,1,1).p("Am0ynIKdAAIAAEEIqdAAgAm+iXIO1AAIAAEDIu1AAgAvuEgII6AAIAAEEIo6AAgAASOkIPdAAIAAEEIvdAAg");
	this.shape_2.setTransform(174.7,-34.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3D4E").s().p("AnaKKIAAkEIO1AAIAAEEgAnQmFIAAkEIKdAAIAAEEg");
	this.shape_3.setTransform(177.5,-88.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FD3C4D").s().p("AkcCCIAAkDII5AAIAAEDg");
	this.shape_4.setTransform(102.5,7.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#08BEC0").s().p("AnuCCIAAkDIPdAAIAAEDg");
	this.shape_5.setTransform(225.9,72);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// gradient
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#182031","rgba(24,32,49,0)"],[0,1],-1,87.1,-1,-141.1).s().p("Egy2AVkMAAAgrHMBltAAAMAAAArHg");
	this.shape_6.setTransform(92.6,289.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

	// bol u misicima
	this.bol = new lib.bolumisicima();
	this.bol.parent = this;
	this.bol.setTransform(101.4,101.6,0.972,0.972,0,0,0,183.5,284.8);

	this.timeline.addTween(cjs.Tween.get(this.bol).wait(1));

	// drhtavica
	this.drhtavica = new lib.drhtavica();
	this.drhtavica.parent = this;
	this.drhtavica.setTransform(100.6,102.8,0.972,0.972,0,0,0,183.6,284.8);

	this.timeline.addTween(cjs.Tween.get(this.drhtavica).wait(1));

	// avatar
	this.instance = new lib.avatar2();
	this.instance.parent = this;
	this.instance.setTransform(-109,-194,0.927,0.927);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// pozadina
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#182031").s().p("Egx1A0WMAAAhorMBjrAAAMAAABorg");
	this.shape_7.setTransform(93,105.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

}).prototype = getMCSymbolPrototype(lib.asd, new cjs.Rectangle(-391.4,-229.4,809.5,685.4), null);


// stage content:
(lib.avatar_desktop = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camera
	this.___camera___instance = new lib.___Camera___();
	this.___camera___instance.parent = this;
	this.___camera___instance.setTransform(300,310);

	this.timeline.addTween(cjs.Tween.get(this.___camera___instance).wait(1));

	// Layer 1
	this.kontent = new lib.asd();
	this.kontent.parent = this;
	this.kontent.setTransform(300,288,1,1,0,0,0,94,86.5);

	this.timeline.addTween(cjs.Tween.get(this.kontent).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(114.6,282.1,809.5,685.4);
// library properties:
lib.properties = {
	width: 600,
	height: 620,
	fps: 24,
	color: "#172233",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"img/avatar2.png", id:"avatar2"}
	],
	preloads: []
};




})(lib_semafor = lib_semafor||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib_semafor, images, createjs, ss, AdobeAn;