(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {};
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {
	for(var i = 0; i < cacheList.length; i++) {
		if(cacheList[i].cacheCanvas)
			cacheList[i].updateCache();
	}
};

lib.addElementsToCache = function (textInst, cacheList) {
	var cur = textInst;
	while(cur != semafor_mobile.exportRoot) {
		if(cacheList.indexOf(cur) != -1)
			break;
		cur = cur.parent;
	}
	if(cur != semafor_mobile.exportRoot) {
		var cur2 = textInst;
		var index = cacheList.indexOf(cur);
		while(cur2 != cur) {
			cacheList.splice(index, 0, cur2);
			cur2 = cur2.parent;
			index++;
		}
	}
	else {
		cur = textInst;
		while(cur != semafor_mobile.exportRoot) {
			cacheList.push(cur);
			cur = cur.parent;
		}
	}
};

lib.gfontAvailable = function(family, totalGoogleCount) {
	lib.properties.webfonts[family] = true;
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];
	for(var f = 0; f < txtInst.length; ++f)
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);

	loadedGoogleCount++;
	if(loadedGoogleCount == totalGoogleCount) {
		lib.updateListCache(gFontsUpdateCacheList);
	}
};

lib.tfontAvailable = function(family, totalTypekitCount) {
	lib.properties.webfonts[family] = true;
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];
	for(var f = 0; f < txtInst.length; ++f)
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);

	loadedTypekitCount++;
	if(loadedTypekitCount == totalTypekitCount) {
		lib.updateListCache(tFontsUpdateCacheList);
	}
};
// symbols:



(lib.avatar2 = function() {
	this.initialize(img.avatar2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,431,701);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_25 = function() {
		semafor_mobile.exportRoot.kontent.box.gotoAndPlay(90);
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(25).call(this.frame_25).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00FF00").s().p("AifCRIAAkhIE/AAIAAEhg");
	this.shape.setTransform(16,14.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(26));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,32,29);


(lib.k1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#09C0C2").s().p("Ak1BfIAAi9IJrAAIAAC9g");
	this.shape.setTransform(31,9.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.k1copy, new cjs.Rectangle(0,0,62,19), null);


(lib.k1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3D4E").s().p("Ak1BfIAAi9IJrAAIAAC9g");
	this.shape.setTransform(31,9.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.k1, new cjs.Rectangle(0,0,62,19), null);


(lib.glow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(23,178,181,0.529)").s().p("A35YiQi+AAAAi+MAAAgrHQAAi+C+AAMAvzAAAQC+AAAAC+MAAAArHQAAC+i+AAgA6t1jMAAAArHQAAC0C0AAMAvzAAAQC0AAAAi0MAAAgrHQAAi0i0AAMgvzAAAQi0AAAAC0g");
	this.shape.setTransform(179,164);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(23,178,181,0.463)").s().p("A35YsQjIAAAAjIMAAAgrHQAAjIDIAAMAvzAAAQDIAAAADIMAAAArHQAADIjIAAgA631jMAAAArHQAAC+C+AAMAvzAAAQC+AAAAi+MAAAgrHQAAi+i+AAMgvzAAAQi+AAAAC+g");
	this.shape_1.setTransform(179,164);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(23,178,181,0.396)").s().p("A35Y2QjSAAAAjSMAAAgrHQAAjSDSAAMAvzAAAQDSAAAADSMAAAArHQAADSjSAAgA7B1jMAAAArHQAADIDIAAMAvzAAAQDIAAAAjIMAAAgrHQAAjIjIAAMgvzAAAQjIAAAADIg");
	this.shape_2.setTransform(179,164);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(23,178,181,0.329)").s().p("A35ZAQjcAAAAjcMAAAgrHQAAjcDcAAMAvzAAAQDcAAAADcMAAAArHQAADcjcAAgA7L1jMAAAArHQAADSDSAAMAvzAAAQDSAAAAjSMAAAgrHQAAjSjSAAMgvzAAAQjSAAAADSg");
	this.shape_3.setTransform(179,164);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(23,178,181,0.263)").s().p("A35ZKQjmAAAAjmMAAAgrHQAAjmDmAAMAvzAAAQDmAAAADmMAAAArHQAADmjmAAgA7V1jMAAAArHQAADcDcAAMAvzAAAQDcAAAAjcMAAAgrHQAAjcjcAAMgvzAAAQjcAAAADcg");
	this.shape_4.setTransform(179,164);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(23,178,181,0.196)").s().p("A35ZUQjwAAAAjwMAAAgrHQAAjwDwAAMAvzAAAQDwAAAADwMAAAArHQAADwjwAAgA7f1jMAAAArHQAADmDmAAMAvzAAAQDmAAAAjmMAAAgrHQAAjmjmAAMgvzAAAQjmAAAADmg");
	this.shape_5.setTransform(179,164);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(23,178,181,0.129)").s().p("A35ZeQj6AAAAj6MAAAgrHQAAj6D6AAMAvzAAAQD6AAAAD6MAAAArHQAAD6j6AAgA7p1jMAAAArHQAADwDwAAMAvzAAAQDwAAAAjwMAAAgrHQAAjwjwAAMgvzAAAQjwAAAADwg");
	this.shape_6.setTransform(179,164);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(23,178,181,0.063)").s().p("A35ZoQkEAAAAkEMAAAgrHQAAkEEEAAMAvzAAAQEEAAAAEEMAAAArHQAAEEkEAAgA7z1jMAAAArHQAAD6D6AAMAvzAAAQD6AAAAj6MAAAgrHQAAj6j6AAMgvzAAAQj6AAAAD6g");
	this.shape_7.setTransform(179,164);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(23,178,181,0.596)").s().p("A35YYQi0AAAAi0MAAAgrHQAAi0C0AAMAvzAAAQC0AAAAC0MAAAArHQAAC0i0AAgA6j1jMAAAArHQAACqCqAAMAvzAAAQCqAAAAiqMAAAgrHQAAiqiqAAMgvzAAAQiqAAAACqg");
	this.shape_8.setTransform(179,164);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(23,178,181,0.663)").s().p("A35YOQiqAAAAiqMAAAgrHQAAiqCqAAMAvzAAAQCqAAAACqMAAAArHQAACqiqAAgA6Z1jMAAAArHQAACgCgAAMAvzAAAQCgAAAAigMAAAgrHQAAigigAAMgvzAAAQigAAAACgg");
	this.shape_9.setTransform(179,164);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(23,178,181,0.729)").s().p("A35YEQigAAAAigMAAAgrHQAAigCgAAMAvzAAAQCgAAAACgMAAAArHQAACgigAAgA6P1jMAAAArHQAACWCWAAMAvzAAAQCWAAAAiWMAAAgrHQAAiWiWAAMgvzAAAQiWAAAACWg");
	this.shape_10.setTransform(179,164);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("rgba(23,178,181,0.796)").s().p("A35X6QiWAAAAiWMAAAgrHQAAiWCWAAMAvzAAAQCWAAAACWMAAAArHQAACWiWAAgA6F1jMAAAArHQAACMCMAAMAvzAAAQCMAAAAiMMAAAgrHQAAiMiMAAMgvzAAAQiMAAAACMg");
	this.shape_11.setTransform(179,164);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(23,178,181,0.863)").s().p("A35XwQiMAAAAiMMAAAgrHQAAiMCMAAMAvzAAAQCMAAAACMMAAAArHQAACMiMAAgA571jMAAAArHQAACCCCAAMAvzAAAQCCAAAAiCMAAAgrHQAAiCiCAAMgvzAAAQiCAAAACCg");
	this.shape_12.setTransform(179,164);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#17B2B5").s().p("A35XcQh4AAAAh4MAAAgrHQAAh4B4AAMAvzAAAQB4AAAAB4MAAAArHQAAB4h4AAg");
	this.shape_13.setTransform(179,164);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("rgba(23,178,181,0.929)").s().p("A35XmQiCAAAAiCMAAAgrHQAAiCCCAAMAvzAAAQCCAAAACCMAAAArHQAACCiCAAgA5x1jMAAAArHQAAB4B4AAMAvzAAAQB4AAAAh4MAAAgrHQAAh4h4AAMgvzAAAQh4AAAAB4g");
	this.shape_14.setTransform(179,164);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.glow, new cjs.Rectangle(0,0,358,328), null);


(lib.drhtavica = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_35 = function() {
		this.gotoAndPlay(1);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(35).call(this.frame_35).wait(1));

	// Layer 3 copy
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgJKAxVQgHgHgHgUQgbhOgHgjIgKg6IgWhZQgIghgHhBQgIhDgHggQgQg6gFgeIgLhOQgHgvgKgdQgGgSgCgIIgBAAQhWlIAGlVQAChLAGhoQAPkOAOh4QAHg7AHgrQAShTAFgqQAGgrgChLQgBhWACggQhfCXhXC3QhKCahPDEQggBRgNAsQgNAxgVBkQgVBYgfA0IgBAEQgBAJgGAKIgLASQgJASgIAaIgNAuQgPAzgZAVQgRAOgXAAQgXABgNgPQgJAGgKAMIgRAVQgWAZgWABQgNAAgLgKQgKgLAEgMQgmAbgaAFQgSAEgSgEQgTgFgLgNQgPgSgCgqQgUAIgSgCQgWgCgKgPQgIgNADgZQADgaANgfQAHgUARgiIBqjbQgUADgYATQgkAdgGADQgbAQgdgBQgYAAgKgOQgJgKADgSQACgOAPgbQAxhWAcgqQAthFAtgxQAMgOAkgkQAfgfAQgTQAPgRAIgFIAIgEIADgPQAJghAegzQAjg8AJgWQALgZAJgpIAPhDQAJgjAhhaQAxiIA1jNQAShGgBglIAAgYIABgGIgBgIQAAgMAMgZQAMgaAQguQASg2AJgUQAOggAohNQAZgvAbg5QARgmABgXIgBgVQgBgOABgIIABgBIgBgPQAAhdAGhbIAFgyQAIg5Ath2QArhwAGg/IACgMQgFgGgDgIQgCgIAAgLIAAgTQAAgVgIghIgNg3QgDgUABgcIABgwIgBgvQgBgbAEgTQAGghAcgzQAnhGAkgpQBYhhC5g7IAOgEIAGgDQA7geAhgLIAfgLQASgGALgGQAigQALgbQAEgJAEgZQAOhigBgyQgBgNABgJIgDgDQglgfgSgTQgcgfgNgeQgOghgEg5QgFhLgEgSQgDgPgIgdQgJgegDgPQgGggAAhCQABivAKilIABgLQgGgIgCgPQgHg2AXhHQAhhiBKhPQBIhNBggtQBdgrBqgJQBogJBnAYQA0AMAjASQAiARA2AtQBQBGAvA9QA8BPARBSIACAKIADAJQAYBZgTBzQgMBHgqCCQglBygYA3IgbA/QgQAkgIAbQgLAogDAzQgCAdAABAQABA/AGAjQAJA2AbAiQAbAkA1AWQAgAOBCAQQCqApBkAoQCQA7BcBWQA9A7ArBLQAqBLASBTQAEAPgCAMIgCAFQAHCdgQCbQgFAqAAATQgBAjAGAaQAJAiAfAvQAqA/AHAPQAPAeANAsIATBNQAVBOAtBvIBNC4QBKDCAfDSQADAXgGAMIABABQAGALgBAYQgBBOgOERQgLDaACCGIACBoQABA8gDArQgDAsgKBIQgNBXgDAcQgIBIgDCgIAAATIAAA4IACABQgBARACAYIAFAyIAFAuIAEAYIABAUQgBAOgFAdIAAAxIAAAMQgEAYgGAVIgPAzQgJAfgEAUQgDAWgBAeIAAA0QgBBogcCXQgNBMgTAnQgWAwgiARQgVAKgWgCQgYgDgPgPQgcAWgWAEQgPADgOgEQgPgEgHgMQgKgNACgcQADgigDgKQgHAdgfANQgfANgagPQgVgNgKgaQgJgXAAgdQAAgOAFgmQADghgBgTQgWAEgOgCQgUgCgKgMQgLgLgDgbQgEgoAJgzIAUhZQAch2APh5IADgPIgBAAIAAgBQAJhcACgvIAEi6QAChvAOhKQAEgRAIgLIABgBQgBgJABgNQAMjMgrjGIgVhcQgNg4gGglQgJg9gFhtIgdpdQAAgPACgLQgGgIgIgPIjXnPIgDANIgIApQgFAVgKAdIgSAyQgPAvgWBnIgqDPQgMBCgGAoQgIA7AAAxQAACKBHDSIA8CqQAhBkAQBKQATBcAJCTQAMDQgKC0IgCAbIgDAAIgFArIgFAtIgGArQgCAQAAAcIAAArIgFAvIgFAuIABBsQABBBgPAoQgGAPgIAGIgCAJIgSA7QgNAngFAVQgMAugKBVQgLBcgJAnIgUBQQgFAagLBLQgIBAgLAkQgFATgLAEQgIAEgJgFQgJgFgEgJQgEgMAEgaIBPnFQAMhFAJgiIAYhOIAMgwIAKAAIABgFQAMgngBg/QgChaABgOIAFgqIAEgpQABgNAAgYIAAgmQABgWAIg0QAHgwAAgbQAAgVAEgIIABgBIABghQAJi2gJiKQgKisgmiPQgQg9gfhbIg0iXQg8i2gCh/QgBgzAJhPQAUinBCkBQAdhuAShCIAZhaQAJghATgDQAMgBAJALQAGAHACAJQAHAHAHATIA8CPQAvBvAbA3IA0BiQAfA7ARAoQANAbgIAOQADAKAAAMIAeJrQAFBcAFAkQAFAiALAvIATBPQA0DpgPDwQgBAWgJAJIgCAOQgMBFgDBfIgEClQgCBdgHBNIgEAAQACAKgCARQgLBogmCdQgNAygDAVQgGApAFAgQANgEAHgQIAKgeQANgxAkgkQAOgNALABQAIAAAHAHQAGAGACAJQADANgHAWIgkCCQgTBLAEA4QABAMAGAJQAGALAKgDQAIgCAGgQIBHjRQAOgmAWAEQALADAFANQAEAMgBAPQgCAZgTBtQgOBSAHA0QAKACAJgIQAJgHADgLIADgVQABgPACgGQACgLAHgIQAIgJAKgBQAJAAAHAHQAHAGADAJQADAMgFAZQgEATAHAIQAHAIANgFQALgEAIgLQAPgUALgeQAHgVAIgiQAgiVABiYIAAgyQABgcAEgVQAEgRAJgcIAPgtQAHgaAFgeQAFgiABgoIAAgoIgKgiQgIglgDguQgCgdABg1IAAgCQAAiRAIhIIASh7QAPhtABidQgBixAChZQABhcAKi5IANjxIgBgBQgCgEAAgGQgBgLAGgGIAAgBIABgEIgFgGQgHgMgCgcQgNidhbjoIhMi9QgrhvgXhSIgShCQgMgmgNgaQgHgOgshFQgggygIgkQgGgaABgiQAAgQAFgrQAPibgEigQgBgQACgLIgDgLQgShHgkhAQglhAg0gzQhUhRiEg3QhegoiaglQhZgWgqgSQhGgegigxQgdgogLg+QgGgogBhIQAAhBACgiQADg3ALgrQAKgnAWg0IAnhZQAWg2AjhxQAehgAIg0QAOhTgPhCIgEgXIgBgNIgFgNIgGgUQgQhAgxg/QglgvhCg5QgqglgdgRQgqgZhAgQQhogZhtARQhsARhcA4QhZA2g4BUQg7BZgCBgQAAAVgDALIADAEQAGAMgCAZQgJClgCClQgBAvAFAaQADAOAIAYIALAlQAHAcADAoIADBFQAEAmAJAUQALAXAeAcQAoAkAIALQAJAMADAMQABAFgBAEQACAIAAAMQABAagCAgIgJBeQgDAcgEAOQgOArgwAiQgSANgQAGIgqANQgSAGgeAOIggAPQgRAIgHABQgLAGgSAGQizA5hOBqQgTAagXAsQgVAjgHAYQgHAVgBAcQgBAQAAAhQABA0ADAZQADASAKA0QAJArABAaQABAOgCAJIABACQAHALgBAVQgBAcgQA3QgbBZggBRQgOAlgDALQgFATgFAnQgMBnACBqQAAARgCAJIABAGQAGAogPAwQgKAhgbAzIgvBaQgbA1gQAoIgXBEQgOArgLAZIgDAGIAAABQAAA8gPBJQgKAtgYBUIgvCeQgQA1gKAaIgYBDQgPAogHAbIgKA1QgGAggHAVQgLAfgjA7QgiA6gLAhQgGARgEAHQgGAJgIACIAAABQgFAMgQARIhIBHQgpAqgaAhQgdAngrBKIgqBIQAkgYAegXQAVgRAOgGQAVgJARAGQANAFAJAPQAHANABAQQAAAVgQAlQgLAZg4B0QgrBVgTA6IBBg/QAYgWAOAFQAKADAEALQAEAKgCALQgCAIgGALIgLARQgYApABAuQAcgEAXgRQAXgRANgZIAOgdQALgRAQACQAKAAAHAKQAHAKAAALQABAOgOAbQASgCANghQAOghATABQAQAAAIARQAFALABAUQAGADAGgGQAFgFACgIQAXhKAihHQAFgJAFgHQABgHADgIIALgTQASgiAMg1IARhbQAMg1AghbQCbmrDxmDQAJgPAKgIQAOgKAMAGQAPAGABAiIABDOQAABVgGAqQgCARgMBBQgYCCgNCjQgIBlgIDBQgGCNABBHQAEDtBKE2QAEAHADAKQAJAaAHAjIALA9IAXCIIAKA8QAPBYAKAsQATBbADAXIAHAqQAEAOAIAVIAYBBQAGATgBAKQgCATgPAEIgHACQgLAAgJgLg");
	this.shape.setTransform(181,306.6);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(17).to({_off:false},0).to({_off:true},8).wait(11));

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EgJKAxVQgHgHgHgUQgbhOgHgjIgKg6IgWhZQgIghgHhBQgIhDgHggQgQg6gFgeIgLhOQgHgvgKgdQgGgSgCgIIgBAAQhWlIAGlVQAChLAGhoQAPkOAOh4QAHg7AHgrQAShTAFgqQAGgrgChLQgBhWACggQhfCXhXC3QhKCahPDEQggBRgNAsQgNAxgVBkQgVBYgfA0IgBAEQgBAJgGAKIgLASQgJASgIAaIgNAuQgPAzgZAVQgRAOgXAAQgXABgNgPQgJAGgKAMIgRAVQgWAZgWABQgNAAgLgKQgKgLAEgMQgmAbgaAFQgSAEgSgEQgTgFgLgNQgPgSgCgqQgUAIgSgCQgWgCgKgPQgIgNADgZQADgaANgfQAHgUARgiIBqjbQgUADgYATQgkAdgGADQgbAQgdgBQgYAAgKgOQgJgKADgSQACgOAPgbQAxhWAcgqQAthFAtgxQAMgOAkgkQAfgfAQgTQAPgRAIgFIAIgEIADgPQAJghAegzQAjg8AJgWQALgZAJgpIAPhDQAJgjAhhaQAxiIA1jNQAShGgBglIAAgYIABgGIgBgIQAAgMAMgZQAMgaAQguQASg2AJgUQAOggAohNQAZgvAbg5QARgmABgXIgBgVQgBgOABgIIABgBIgBgPQAAhdAGhbIAFgyQAIg5Ath2QArhwAGg/IACgMQgFgGgDgIQgCgIAAgLIAAgTQAAgVgIghIgNg3QgDgUABgcIABgwIgBgvQgBgbAEgTQAGghAcgzQAnhGAkgpQBYhhC5g7IAOgEIAGgDQA7geAhgLIAfgLQASgGALgGQAigQALgbQAEgJAEgZQAOhigBgyQgBgNABgJIgDgDQglgfgSgTQgcgfgNgeQgOghgEg5QgFhLgEgSQgDgPgIgdQgJgegDgPQgGggAAhCQABivAKilIABgLQgGgIgCgPQgHg2AXhHQAhhiBKhPQBIhNBggtQBdgrBqgJQBogJBnAYQA0AMAjASQAiARA2AtQBQBGAvA9QA8BPARBSIACAKIADAJQAYBZgTBzQgMBHgqCCQglBygYA3IgbA/QgQAkgIAbQgLAogDAzQgCAdAABAQABA/AGAjQAJA2AbAiQAbAkA1AWQAgAOBCAQQCqApBkAoQCQA7BcBWQA9A7ArBLQAqBLASBTQAEAPgCAMIgCAFQAHCdgQCbQgFAqAAATQgBAjAGAaQAJAiAfAvQAqA/AHAPQAPAeANAsIATBNQAVBOAtBvIBNC4QBKDCAfDSQADAXgGAMIABABQAGALgBAYQgBBOgOERQgLDaACCGIACBoQABA8gDArQgDAsgKBIQgNBXgDAcQgIBIgDCgIAAATIAAA4IACABQgBARACAYIAFAyIAFAuIAEAYIABAUQgBAOgFAdIAAAxIAAAMQgEAYgGAVIgPAzQgJAfgEAUQgDAWgBAeIAAA0QgBBogcCXQgNBMgTAnQgWAwgiARQgVAKgWgCQgYgDgPgPQgcAWgWAEQgPADgOgEQgPgEgHgMQgKgNACgcQADgigDgKQgHAdgfANQgfANgagPQgVgNgKgaQgJgXAAgdQAAgOAFgmQADghgBgTQgWAEgOgCQgUgCgKgMQgLgLgDgbQgEgoAJgzIAUhZQAch2APh5IADgPIgBAAIAAgBQAJhcACgvIAEi6QAChvAOhKQAEgRAIgLIABgBQgBgJABgNQAMjMgrjGIgVhcQgNg4gGglQgJg9gFhtIgdpdQAAgPACgLQgGgIgIgPIjXnPIgDANIgIApQgFAVgKAdIgSAyQgPAvgWBnIgqDPQgMBCgGAoQgIA7AAAxQAACKBHDSIA8CqQAhBkAQBKQATBcAJCTQAMDQgKC0IgCAbIgDAAIgFArIgFAtIgGArQgCAQAAAcIAAArIgFAvIgFAuIABBsQABBBgPAoQgGAPgIAGIgCAJIgSA7QgNAngFAVQgMAugKBVQgLBcgJAnIgUBQQgFAagLBLQgIBAgLAkQgFATgLAEQgIAEgJgFQgJgFgEgJQgEgMAEgaIBPnFQAMhFAJgiIAYhOIAMgwIAKAAIABgFQAMgngBg/QgChaABgOIAFgqIAEgpQABgNAAgYIAAgmQABgWAIg0QAHgwAAgbQAAgVAEgIIABgBIABghQAJi2gJiKQgKisgmiPQgQg9gfhbIg0iXQg8i2gCh/QgBgzAJhPQAUinBCkBQAdhuAShCIAZhaQAJghATgDQAMgBAJALQAGAHACAJQAHAHAHATIA8CPQAvBvAbA3IA0BiQAfA7ARAoQANAbgIAOQADAKAAAMIAeJrQAFBcAFAkQAFAiALAvIATBPQA0DpgPDwQgBAWgJAJIgCAOQgMBFgDBfIgEClQgCBdgHBNIgEAAQACAKgCARQgLBogmCdQgNAygDAVQgGApAFAgQANgEAHgQIAKgeQANgxAkgkQAOgNALABQAIAAAHAHQAGAGACAJQADANgHAWIgkCCQgTBLAEA4QABAMAGAJQAGALAKgDQAIgCAGgQIBHjRQAOgmAWAEQALADAFANQAEAMgBAPQgCAZgTBtQgOBSAHA0QAKACAJgIQAJgHADgLIADgVQABgPACgGQACgLAHgIQAIgJAKgBQAJAAAHAHQAHAGADAJQADAMgFAZQgEATAHAIQAHAIANgFQALgEAIgLQAPgUALgeQAHgVAIgiQAgiVABiYIAAgyQABgcAEgVQAEgRAJgcIAPgtQAHgaAFgeQAFgiABgoIAAgoIgKgiQgIglgDguQgCgdABg1IAAgCQAAiRAIhIIASh7QAPhtABidQgBixAChZQABhcAKi5IANjxIgBgBQgCgEAAgGQgBgLAGgGIAAgBIABgEIgFgGQgHgMgCgcQgNidhbjoIhMi9QgrhvgXhSIgShCQgMgmgNgaQgHgOgshFQgggygIgkQgGgaABgiQAAgQAFgrQAPibgEigQgBgQACgLIgDgLQgShHgkhAQglhAg0gzQhUhRiEg3QhegoiaglQhZgWgqgSQhGgegigxQgdgogLg+QgGgogBhIQAAhBACgiQADg3ALgrQAKgnAWg0IAnhZQAWg2AjhxQAehgAIg0QAOhTgPhCIgEgXIgBgNIgFgNIgGgUQgQhAgxg/QglgvhCg5QgqglgdgRQgqgZhAgQQhogZhtARQhsARhcA4QhZA2g4BUQg7BZgCBgQAAAVgDALIADAEQAGAMgCAZQgJClgCClQgBAvAFAaQADAOAIAYIALAlQAHAcADAoIADBFQAEAmAJAUQALAXAeAcQAoAkAIALQAJAMADAMQABAFgBAEQACAIAAAMQABAagCAgIgJBeQgDAcgEAOQgOArgwAiQgSANgQAGIgqANQgSAGgeAOIggAPQgRAIgHABQgLAGgSAGQizA5hOBqQgTAagXAsQgVAjgHAYQgHAVgBAcQgBAQAAAhQABA0ADAZQADASAKA0QAJArABAaQABAOgCAJIABACQAHALgBAVQgBAcgQA3QgbBZggBRQgOAlgDALQgFATgFAnQgMBnACBqQAAARgCAJIABAGQAGAogPAwQgKAhgbAzIgvBaQgbA1gQAoIgXBEQgOArgLAZIgDAGIAAABQAAA8gPBJQgKAtgYBUIgvCeQgQA1gKAaIgYBDQgPAogHAbIgKA1QgGAggHAVQgLAfgjA7QgiA6gLAhQgGARgEAHQgGAJgIACIAAABQgFAMgQARIhIBHQgpAqgaAhQgdAngrBKIgqBIQAkgYAegXQAVgRAOgGQAVgJARAGQANAFAJAPQAHANABAQQAAAVgQAlQgLAZg4B0QgrBVgTA6IBBg/QAYgWAOAFQAKADAEALQAEAKgCALQgCAIgGALIgLARQgYApABAuQAcgEAXgRQAXgRANgZIAOgdQALgRAQACQAKAAAHAKQAHAKAAALQABAOgOAbQASgCANghQAOghATABQAQAAAIARQAFALABAUQAGADAGgGQAFgFACgIQAXhKAihHQAFgJAFgHQABgHADgIIALgTQASgiAMg1IARhbQAMg1AghbQCbmrDxmDQAJgPAKgIQAOgKAMAGQAPAGABAiIABDOQAABVgGAqQgCARgMBBQgYCCgNCjQgIBlgIDBQgGCNABBHQAEDtBKE2QAEAHADAKQAJAaAHAjIALA9IAXCIIAKA8QAPBYAKAsQATBbADAXIAHAqQAEAOAIAVIAYBBQAGATgBAKQgCATgPAEIgHACQgLAAgJgLg");
	this.shape_1.setTransform(181,306.6);
	this.shape_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1).to({_off:false},0).to({_off:true},7).wait(28));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.close = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(4,1,1).p("AACACIBHBHAhEBJIBGhHAhIhIIBKBKIBHhG");
	this.shape.setTransform(15.5,15.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(4,1,1).p("AACACIBHBHAhEBJIBGhHAhIhIIBKBKIBHhG");
	this.shape_1.setTransform(15.5,15.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).wait(3));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF3D4E").s().p("AhsBuQguguAAhAQAAg/AugtQAtguA/AAQBAAAAuAuQAtAtAAA/QAABAgtAuQguAthAAAQg/AAgtgtg");
	this.shape_2.setTransform(15.5,15.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(4));

	// Layer 3
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.008)").s().p("AkMEMQhvhvAAidQAAidBvhwQBvhuCdAAQCdAABvBuQBwBwAACdQAACdhwBvQhvBwidAAQidAAhvhwg");
	this.shape_3.setTransform(16,15);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22,-23,76,76);


(lib.bolumisicima = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_34 = function() {
		this.gotoAndPlay(1);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(34).call(this.frame_34).wait(1));

	// Layer 1 copy
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3D4E").s().p("EgJKAxVQgHgHgHgUQgbhOgHgjIgKg6IgWhZQgIghgHhBQgIhDgHggQgQg6gFgeIgLhOQgHgvgKgdQgGgSgCgIIgBAAQhWlIAGlVQAChLAGhoQAPkOAOh4QAHg7AHgrQAShTAFgqQAGgrgChLQgBhWACggQhfCXhXC3QhKCahPDEQggBRgNAsQgNAxgVBkQgVBYgfA0IgBAEQgBAJgGAKIgLASQgJASgIAaIgNAuQgPAzgZAVQgRAOgXAAQgXABgNgPQgJAGgKAMIgRAVQgWAZgWABQgNAAgLgKQgKgLAEgMQgmAbgaAFQgSAEgSgEQgTgFgLgNQgPgSgCgqQgUAIgSgCQgWgCgKgPQgIgNADgZQADgaANgfQAHgUARgiIBqjbQgUADgYATQgkAdgGADQgbAQgdgBQgYAAgKgOQgJgKADgSQACgOAPgbQAxhWAcgqQAthFAtgxQAMgOAkgkQAfgfAQgTQAPgRAIgFIAIgEIADgPQAJghAegzQAjg8AJgWQALgZAJgpIAPhDQAJgjAhhaQAxiIA1jNQAShGgBglIAAgYIABgGIgBgIQAAgMAMgZQAMgaAQguQASg2AJgUQAOggAohNQAZgvAbg5QARgmABgXIgBgVQgBgOABgIIABgBIgBgPQAAhdAGhbIAFgyQAIg5Ath2QArhwAGg/IACgMQgFgGgDgIQgCgIAAgLIAAgTQAAgVgIghIgNg3QgDgUABgcIABgwIgBgvQgBgbAEgTQAGghAcgzQAnhGAkgpQBYhhC5g7IAOgEIAGgDQA7geAhgLIAfgLQASgGALgGQAigQALgbQAEgJAEgZQAOhigBgyQgBgNABgJIgDgDQglgfgSgTQgcgfgNgeQgOghgEg5QgFhLgEgSQgDgPgIgdQgJgegDgPQgGggAAhCQABivAKilIABgLQgGgIgCgPQgHg2AXhHQAhhiBKhPQBIhNBggtQBdgrBqgJQBogJBnAYQA0AMAjASQAiARA2AtQBQBGAvA9QA8BPARBSIACAKIADAJQAYBZgTBzQgMBHgqCCQglBygYA3IgbA/QgQAkgIAbQgLAogDAzQgCAdAABAQABA/AGAjQAJA2AbAiQAbAkA1AWQAgAOBCAQQCqApBkAoQCQA7BcBWQA9A7ArBLQAqBLASBTQAEAPgCAMIgCAFQAHCdgQCbQgFAqAAATQgBAjAGAaQAJAiAfAvQAqA/AHAPQAPAeANAsIATBNQAVBOAtBvIBNC4QBKDCAfDSQADAXgGAMIABABQAGALgBAYQgBBOgOERQgLDaACCGIACBoQABA8gDArQgDAsgKBIQgNBXgDAcQgIBIgDCgIAAATIAAA4IACABQgBARACAYIAFAyIAFAuIAEAYIABAUQgBAOgFAdIAAAxIAAAMQgEAYgGAVIgPAzQgJAfgEAUQgDAWgBAeIAAA0QgBBogcCXQgNBMgTAnQgWAwgiARQgVAKgWgCQgYgDgPgPQgcAWgWAEQgPADgOgEQgPgEgHgMQgKgNACgcQADgigDgKQgHAdgfANQgfANgagPQgVgNgKgaQgJgXAAgdQAAgOAFgmQADghgBgTQgWAEgOgCQgUgCgKgMQgLgLgDgbQgEgoAJgzIAUhZQAch2APh5IADgPIgBAAIAAgBQAJhcACgvIAEi6QAChvAOhKQAEgRAIgLIABgBQgBgJABgNQAMjMgrjGIgVhcQgNg4gGglQgJg9gFhtIgdpdQAAgPACgLQgGgIgIgPIjXnPIgDANIgIApQgFAVgKAdIgSAyQgPAvgWBnIgqDPQgMBCgGAoQgIA7AAAxQAACKBHDSIA8CqQAhBkAQBKQATBcAJCTQAMDQgKC0IgCAbIgDAAIgFArIgFAtIgGArQgCAQAAAcIAAArIgFAvIgFAuIABBsQABBBgPAoQgGAPgIAGIgCAJIgSA7QgNAngFAVQgMAugKBVQgLBcgJAnIgUBQQgFAagLBLQgIBAgLAkQgFATgLAEQgIAEgJgFQgJgFgEgJQgEgMAEgaIBPnFQAMhFAJgiIAYhOIAMgwIAKAAIABgFQAMgngBg/QgChaABgOIAFgqIAEgpQABgNAAgYIAAgmQABgWAIg0QAHgwAAgbQAAgVAEgIIABgBIABghQAJi2gJiKQgKisgmiPQgQg9gfhbIg0iXQg8i2gCh/QgBgzAJhPQAUinBCkBQAdhuAShCIAZhaQAJghATgDQAMgBAJALQAGAHACAJQAHAHAHATIA8CPQAvBvAbA3IA0BiQAfA7ARAoQANAbgIAOQADAKAAAMIAeJrQAFBcAFAkQAFAiALAvIATBPQA0DpgPDwQgBAWgJAJIgCAOQgMBFgDBfIgEClQgCBdgHBNIgEAAQACAKgCARQgLBogmCdQgNAygDAVQgGApAFAgQANgEAHgQIAKgeQANgxAkgkQAOgNALABQAIAAAHAHQAGAGACAJQADANgHAWIgkCCQgTBLAEA4QABAMAGAJQAGALAKgDQAIgCAGgQIBHjRQAOgmAWAEQALADAFANQAEAMgBAPQgCAZgTBtQgOBSAHA0QAKACAJgIQAJgHADgLIADgVQABgPACgGQACgLAHgIQAIgJAKgBQAJAAAHAHQAHAGADAJQADAMgFAZQgEATAHAIQAHAIANgFQALgEAIgLQAPgUALgeQAHgVAIgiQAgiVABiYIAAgyQABgcAEgVQAEgRAJgcIAPgtQAHgaAFgeQAFgiABgoIAAgoIgKgiQgIglgDguQgCgdABg1IAAgCQAAiRAIhIIASh7QAPhtABidQgBixAChZQABhcAKi5IANjxIgBgBQgCgEAAgGQgBgLAGgGIAAgBIABgEIgFgGQgHgMgCgcQgNidhbjoIhMi9QgrhvgXhSIgShCQgMgmgNgaQgHgOgshFQgggygIgkQgGgaABgiQAAgQAFgrQAPibgEigQgBgQACgLIgDgLQgShHgkhAQglhAg0gzQhUhRiEg3QhegoiaglQhZgWgqgSQhGgegigxQgdgogLg+QgGgogBhIQAAhBACgiQADg3ALgrQAKgnAWg0IAnhZQAWg2AjhxQAehgAIg0QAOhTgPhCIgEgXIgBgNIgFgNIgGgUQgQhAgxg/QglgvhCg5QgqglgdgRQgqgZhAgQQhogZhtARQhsARhcA4QhZA2g4BUQg7BZgCBgQAAAVgDALIADAEQAGAMgCAZQgJClgCClQgBAvAFAaQADAOAIAYIALAlQAHAcADAoIADBFQAEAmAJAUQALAXAeAcQAoAkAIALQAJAMADAMQABAFgBAEQACAIAAAMQABAagCAgIgJBeQgDAcgEAOQgOArgwAiQgSANgQAGIgqANQgSAGgeAOIggAPQgRAIgHABQgLAGgSAGQizA5hOBqQgTAagXAsQgVAjgHAYQgHAVgBAcQgBAQAAAhQABA0ADAZQADASAKA0QAJArABAaQABAOgCAJIABACQAHALgBAVQgBAcgQA3QgbBZggBRQgOAlgDALQgFATgFAnQgMBnACBqQAAARgCAJIABAGQAGAogPAwQgKAhgbAzIgvBaQgbA1gQAoIgXBEQgOArgLAZIgDAGIAAABQAAA8gPBJQgKAtgYBUIgvCeQgQA1gKAaIgYBDQgPAogHAbIgKA1QgGAggHAVQgLAfgjA7QgiA6gLAhQgGARgEAHQgGAJgIACIAAABQgFAMgQARIhIBHQgpAqgaAhQgdAngrBKIgqBIQAkgYAegXQAVgRAOgGQAVgJARAGQANAFAJAPQAHANABAQQAAAVgQAlQgLAZg4B0QgrBVgTA6IBBg/QAYgWAOAFQAKADAEALQAEAKgCALQgCAIgGALIgLARQgYApABAuQAcgEAXgRQAXgRANgZIAOgdQALgRAQACQAKAAAHAKQAHAKAAALQABAOgOAbQASgCANghQAOghATABQAQAAAIARQAFALABAUQAGADAGgGQAFgFACgIQAXhKAihHQAFgJAFgHQABgHADgIIALgTQASgiAMg1IARhbQAMg1AghbQCbmrDxmDQAJgPAKgIQAOgKAMAGQAPAGABAiIABDOQAABVgGAqQgCARgMBBQgYCCgNCjQgIBlgIDBQgGCNABBHQAEDtBKE2QAEAHADAKQAJAaAHAjIALA9IAXCIIAKA8QAPBYAKAsQATBbADAXIAHAqQAEAOAIAVIAYBBQAGATgBAKQgCATgPAEIgHACQgLAAgJgLg");
	this.shape.setTransform(181,306.6);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(17).to({_off:false},0).to({_off:true},9).wait(9));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF3D4E").s().p("EgJKAxVQgHgHgHgUQgbhOgHgjIgKg6IgWhZQgIghgHhBQgIhDgHggQgQg6gFgeIgLhOQgHgvgKgdQgGgSgCgIIgBAAQhWlIAGlVQAChLAGhoQAPkOAOh4QAHg7AHgrQAShTAFgqQAGgrgChLQgBhWACggQhfCXhXC3QhKCahPDEQggBRgNAsQgNAxgVBkQgVBYgfA0IgBAEQgBAJgGAKIgLASQgJASgIAaIgNAuQgPAzgZAVQgRAOgXAAQgXABgNgPQgJAGgKAMIgRAVQgWAZgWABQgNAAgLgKQgKgLAEgMQgmAbgaAFQgSAEgSgEQgTgFgLgNQgPgSgCgqQgUAIgSgCQgWgCgKgPQgIgNADgZQADgaANgfQAHgUARgiIBqjbQgUADgYATQgkAdgGADQgbAQgdgBQgYAAgKgOQgJgKADgSQACgOAPgbQAxhWAcgqQAthFAtgxQAMgOAkgkQAfgfAQgTQAPgRAIgFIAIgEIADgPQAJghAegzQAjg8AJgWQALgZAJgpIAPhDQAJgjAhhaQAxiIA1jNQAShGgBglIAAgYIABgGIgBgIQAAgMAMgZQAMgaAQguQASg2AJgUQAOggAohNQAZgvAbg5QARgmABgXIgBgVQgBgOABgIIABgBIgBgPQAAhdAGhbIAFgyQAIg5Ath2QArhwAGg/IACgMQgFgGgDgIQgCgIAAgLIAAgTQAAgVgIghIgNg3QgDgUABgcIABgwIgBgvQgBgbAEgTQAGghAcgzQAnhGAkgpQBYhhC5g7IAOgEIAGgDQA7geAhgLIAfgLQASgGALgGQAigQALgbQAEgJAEgZQAOhigBgyQgBgNABgJIgDgDQglgfgSgTQgcgfgNgeQgOghgEg5QgFhLgEgSQgDgPgIgdQgJgegDgPQgGggAAhCQABivAKilIABgLQgGgIgCgPQgHg2AXhHQAhhiBKhPQBIhNBggtQBdgrBqgJQBogJBnAYQA0AMAjASQAiARA2AtQBQBGAvA9QA8BPARBSIACAKIADAJQAYBZgTBzQgMBHgqCCQglBygYA3IgbA/QgQAkgIAbQgLAogDAzQgCAdAABAQABA/AGAjQAJA2AbAiQAbAkA1AWQAgAOBCAQQCqApBkAoQCQA7BcBWQA9A7ArBLQAqBLASBTQAEAPgCAMIgCAFQAHCdgQCbQgFAqAAATQgBAjAGAaQAJAiAfAvQAqA/AHAPQAPAeANAsIATBNQAVBOAtBvIBNC4QBKDCAfDSQADAXgGAMIABABQAGALgBAYQgBBOgOERQgLDaACCGIACBoQABA8gDArQgDAsgKBIQgNBXgDAcQgIBIgDCgIAAATIAAA4IACABQgBARACAYIAFAyIAFAuIAEAYIABAUQgBAOgFAdIAAAxIAAAMQgEAYgGAVIgPAzQgJAfgEAUQgDAWgBAeIAAA0QgBBogcCXQgNBMgTAnQgWAwgiARQgVAKgWgCQgYgDgPgPQgcAWgWAEQgPADgOgEQgPgEgHgMQgKgNACgcQADgigDgKQgHAdgfANQgfANgagPQgVgNgKgaQgJgXAAgdQAAgOAFgmQADghgBgTQgWAEgOgCQgUgCgKgMQgLgLgDgbQgEgoAJgzIAUhZQAch2APh5IADgPIgBAAIAAgBQAJhcACgvIAEi6QAChvAOhKQAEgRAIgLIABgBQgBgJABgNQAMjMgrjGIgVhcQgNg4gGglQgJg9gFhtIgdpdQAAgPACgLQgGgIgIgPIjXnPIgDANIgIApQgFAVgKAdIgSAyQgPAvgWBnIgqDPQgMBCgGAoQgIA7AAAxQAACKBHDSIA8CqQAhBkAQBKQATBcAJCTQAMDQgKC0IgCAbIgDAAIgFArIgFAtIgGArQgCAQAAAcIAAArIgFAvIgFAuIABBsQABBBgPAoQgGAPgIAGIgCAJIgSA7QgNAngFAVQgMAugKBVQgLBcgJAnIgUBQQgFAagLBLQgIBAgLAkQgFATgLAEQgIAEgJgFQgJgFgEgJQgEgMAEgaIBPnFQAMhFAJgiIAYhOIAMgwIAKAAIABgFQAMgngBg/QgChaABgOIAFgqIAEgpQABgNAAgYIAAgmQABgWAIg0QAHgwAAgbQAAgVAEgIIABgBIABghQAJi2gJiKQgKisgmiPQgQg9gfhbIg0iXQg8i2gCh/QgBgzAJhPQAUinBCkBQAdhuAShCIAZhaQAJghATgDQAMgBAJALQAGAHACAJQAHAHAHATIA8CPQAvBvAbA3IA0BiQAfA7ARAoQANAbgIAOQADAKAAAMIAeJrQAFBcAFAkQAFAiALAvIATBPQA0DpgPDwQgBAWgJAJIgCAOQgMBFgDBfIgEClQgCBdgHBNIgEAAQACAKgCARQgLBogmCdQgNAygDAVQgGApAFAgQANgEAHgQIAKgeQANgxAkgkQAOgNALABQAIAAAHAHQAGAGACAJQADANgHAWIgkCCQgTBLAEA4QABAMAGAJQAGALAKgDQAIgCAGgQIBHjRQAOgmAWAEQALADAFANQAEAMgBAPQgCAZgTBtQgOBSAHA0QAKACAJgIQAJgHADgLIADgVQABgPACgGQACgLAHgIQAIgJAKgBQAJAAAHAHQAHAGADAJQADAMgFAZQgEATAHAIQAHAIANgFQALgEAIgLQAPgUALgeQAHgVAIgiQAgiVABiYIAAgyQABgcAEgVQAEgRAJgcIAPgtQAHgaAFgeQAFgiABgoIAAgoIgKgiQgIglgDguQgCgdABg1IAAgCQAAiRAIhIIASh7QAPhtABidQgBixAChZQABhcAKi5IANjxIgBgBQgCgEAAgGQgBgLAGgGIAAgBIABgEIgFgGQgHgMgCgcQgNidhbjoIhMi9QgrhvgXhSIgShCQgMgmgNgaQgHgOgshFQgggygIgkQgGgaABgiQAAgQAFgrQAPibgEigQgBgQACgLIgDgLQgShHgkhAQglhAg0gzQhUhRiEg3QhegoiaglQhZgWgqgSQhGgegigxQgdgogLg+QgGgogBhIQAAhBACgiQADg3ALgrQAKgnAWg0IAnhZQAWg2AjhxQAehgAIg0QAOhTgPhCIgEgXIgBgNIgFgNIgGgUQgQhAgxg/QglgvhCg5QgqglgdgRQgqgZhAgQQhogZhtARQhsARhcA4QhZA2g4BUQg7BZgCBgQAAAVgDALIADAEQAGAMgCAZQgJClgCClQgBAvAFAaQADAOAIAYIALAlQAHAcADAoIADBFQAEAmAJAUQALAXAeAcQAoAkAIALQAJAMADAMQABAFgBAEQACAIAAAMQABAagCAgIgJBeQgDAcgEAOQgOArgwAiQgSANgQAGIgqANQgSAGgeAOIggAPQgRAIgHABQgLAGgSAGQizA5hOBqQgTAagXAsQgVAjgHAYQgHAVgBAcQgBAQAAAhQABA0ADAZQADASAKA0QAJArABAaQABAOgCAJIABACQAHALgBAVQgBAcgQA3QgbBZggBRQgOAlgDALQgFATgFAnQgMBnACBqQAAARgCAJIABAGQAGAogPAwQgKAhgbAzIgvBaQgbA1gQAoIgXBEQgOArgLAZIgDAGIAAABQAAA8gPBJQgKAtgYBUIgvCeQgQA1gKAaIgYBDQgPAogHAbIgKA1QgGAggHAVQgLAfgjA7QgiA6gLAhQgGARgEAHQgGAJgIACIAAABQgFAMgQARIhIBHQgpAqgaAhQgdAngrBKIgqBIQAkgYAegXQAVgRAOgGQAVgJARAGQANAFAJAPQAHANABAQQAAAVgQAlQgLAZg4B0QgrBVgTA6IBBg/QAYgWAOAFQAKADAEALQAEAKgCALQgCAIgGALIgLARQgYApABAuQAcgEAXgRQAXgRANgZIAOgdQALgRAQACQAKAAAHAKQAHAKAAALQABAOgOAbQASgCANghQAOghATABQAQAAAIARQAFALABAUQAGADAGgGQAFgFACgIQAXhKAihHQAFgJAFgHQABgHADgIIALgTQASgiAMg1IARhbQAMg1AghbQCbmrDxmDQAJgPAKgIQAOgKAMAGQAPAGABAiIABDOQAABVgGAqQgCARgMBBQgYCCgNCjQgIBlgIDBQgGCNABBHQAEDtBKE2QAEAHADAKQAJAaAHAjIALA9IAXCIIAKA8QAPBYAKAsQATBbADAXIAHAqQAEAOAIAVIAYBBQAGATgBAKQgCATgPAEIgHACQgLAAgJgLg");
	this.shape_1.setTransform(181,306.6);
	this.shape_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1).to({_off:false},0).to({_off:true},8).wait(26));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.animacijagumbacopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1 copy
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(4,1,1).p("ACCAAQAAA2gmAmQgmAmg2AAQg1AAgmgmQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1g");
	this.shape.setTransform(13,13);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(14));

	// Layer 1 copy 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#09C0C2").ss(4,1,1).p("ACCAAQAAA2gmAmQgmAmg2AAQg1AAgmgmQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1g");
	this.shape_1.setTransform(13,13);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#09C0C2").ss(4,1,1).p("AiCAAQAAg2AmgmQAmgmA2AAQA2AAAnAmQAmAmAAA2QAAA2gmAnQgnAmg2AAQg2AAgmgmQgmgnAAg2g");
	this.shape_2.setTransform(13,13);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#09C0C2").ss(4,1,1).p("AiFAAQAAg3AngnQAngnA3AAQA4AAAnAnQAnAnAAA3QAAA4gnAnQgnAng4AAQg3AAgngnQgngnAAg4g");
	this.shape_3.setTransform(13,13);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#09C0C2").ss(4,1,1).p("AiLAAQAAg5ApgpQApgpA5AAQA6AAApApQApApAAA5QAAA6gpApQgpApg6AAQg5AAgpgpQgpgpAAg6g");
	this.shape_4.setTransform(13,13);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#09C0C2").ss(4,1,1).p("AiSAAQAAg8ArgrQArgrA8AAQA9AAArArQArArAAA8QAAA9grArQgrArg9AAQg8AAgrgrQgrgrAAg9g");
	this.shape_5.setTransform(13,13);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#09C0C2").ss(4,1,1).p("AicAAQAAhAAuguQAuguBAAAQBBAAAuAuQAuAuAABAQAABBguAuQguAuhBAAQhAAAguguQguguAAhBg");
	this.shape_6.setTransform(13,13);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#09C0C2").ss(4,1,1).p("AinAAQAAhFAxgxQAxgxBFAAQBGAAAxAxQAxAxAABFQAABGgxAxQgxAxhGAAQhFAAgxgxQgxgxAAhGg");
	this.shape_7.setTransform(13,13);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#09C0C2").ss(4,1,1).p("Ai1AAQAAhLA1g1QA1g1BLAAQBMAAA1A1QA1A1AABLQAABMg1A1Qg1A1hMAAQhLAAg1g1Qg1g1AAhMg");
	this.shape_8.setTransform(13,13);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#09C0C2").ss(4,1,1).p("AjEAAQAAhRA5g6QA6g5BRAAQBSAAA5A5QA6A6AABRQAABSg6A5Qg5A6hSAAQhRAAg6g6Qg5g5AAhSg");
	this.shape_9.setTransform(13,13);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#09C0C2").ss(4,1,1).p("AjWAAQAAhYA/g/QA/g/BYAAQBZAAA/A/QA/A/AABYQAABZg/A/Qg/A/hZAAQhYAAg/g/Qg/g/AAhZg");
	this.shape_10.setTransform(13,13);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#09C0C2").ss(4,1,1).p("AjqAAQAAhhBEhFQBFhEBhAAQBiAABEBEQBFBFAABhQAABihFBEQhEBFhiAAQhhAAhFhFQhEhEAAhig");
	this.shape_11.setTransform(13,13);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#09C0C2").ss(4,1,1).p("AkAAAQAAhqBLhLQBLhLBqAAQBrAABLBLQBLBLAABqQAABrhLBLQhLBLhrAAQhqAAhLhLQhLhLAAhrg");
	this.shape_12.setTransform(13,13);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#09C0C2").ss(4,1,1).p("AkYAAQAAh0BShSQBShSB0AAQB1AABSBSQBSBSAAB0QAAB1hSBSQhSBSh1AAQh0AAhShSQhShSAAh1g");
	this.shape_13.setTransform(13,13);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#09C0C2").ss(4,1,1).p("AE0AAQAACAhaBaQhaBaiAAAQh/AAhahaQhahaAAiAQAAh/BahaQBahaB/AAQCAAABaBaQBaBaAAB/g");
	this.shape_14.setTransform(13,13);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).wait(1));

	// Layer 1
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#09C0C2").s().p("AhbBcQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1QAAA2gmAmQgmAmg2AAQg1AAgmgmg");
	this.shape_15.setTransform(13,13);

	this.timeline.addTween(cjs.Tween.get(this.shape_15).wait(14));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,30,30);


(lib.animacijagumba = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1 copy
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF3D4E").ss(4,1,1).p("ACCAAQAAA2gmAmQgmAmg2AAQg1AAgmgmQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1g");
	this.shape.setTransform(13,13);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(14));

	// Layer 1 copy 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FF3D4E").ss(4,1,1).p("ACCAAQAAA2gmAmQgmAmg2AAQg1AAgmgmQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1g");
	this.shape_1.setTransform(13,13);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(255,61,78,0.992)").ss(4,1,1).p("AiCAAQAAg2AmgmQAmgmA2AAQA2AAAnAmQAmAmAAA2QAAA2gmAnQgnAmg2AAQg2AAgmgmQgmgnAAg2g");
	this.shape_2.setTransform(13,13);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(255,61,78,0.976)").ss(4,1,1).p("AiFAAQAAg3AngnQAngnA3AAQA4AAAnAnQAnAnAAA3QAAA4gnAnQgnAng4AAQg3AAgngnQgngnAAg4g");
	this.shape_3.setTransform(13,13);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("rgba(255,61,78,0.945)").ss(4,1,1).p("AiLAAQAAg5ApgpQApgpA5AAQA6AAApApQApApAAA5QAAA6gpApQgpApg6AAQg5AAgpgpQgpgpAAg6g");
	this.shape_4.setTransform(13,13);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("rgba(255,61,78,0.906)").ss(4,1,1).p("AiSAAQAAg8ArgrQArgrA8AAQA9AAArArQArArAAA8QAAA9grArQgrArg9AAQg8AAgrgrQgrgrAAg9g");
	this.shape_5.setTransform(13,13);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("rgba(255,61,78,0.851)").ss(4,1,1).p("AicAAQAAhAAuguQAuguBAAAQBBAAAuAuQAuAuAABAQAABBguAuQguAuhBAAQhAAAguguQguguAAhBg");
	this.shape_6.setTransform(13,13);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("rgba(255,61,78,0.788)").ss(4,1,1).p("AinAAQAAhFAxgxQAxgxBFAAQBGAAAxAxQAxAxAABFQAABGgxAxQgxAxhGAAQhFAAgxgxQgxgxAAhGg");
	this.shape_7.setTransform(13,13);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("rgba(255,61,78,0.71)").ss(4,1,1).p("Ai1AAQAAhLA1g1QA1g1BLAAQBMAAA1A1QA1A1AABLQAABMg1A1Qg1A1hMAAQhLAAg1g1Qg1g1AAhMg");
	this.shape_8.setTransform(13,13);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(255,61,78,0.62)").ss(4,1,1).p("AjEAAQAAhRA5g6QA6g5BRAAQBSAAA5A5QA6A6AABRQAABSg6A5Qg5A6hSAAQhRAAg6g6Qg5g5AAhSg");
	this.shape_9.setTransform(13,13);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("rgba(255,61,78,0.522)").ss(4,1,1).p("AjWAAQAAhYA/g/QA/g/BYAAQBZAAA/A/QA/A/AABYQAABZg/A/Qg/A/hZAAQhYAAg/g/Qg/g/AAhZg");
	this.shape_10.setTransform(13,13);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("rgba(255,61,78,0.408)").ss(4,1,1).p("AjqAAQAAhhBEhFQBFhEBhAAQBiAABEBEQBFBFAABhQAABihFBEQhEBFhiAAQhhAAhFhFQhEhEAAhig");
	this.shape_11.setTransform(13,13);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("rgba(255,61,78,0.282)").ss(4,1,1).p("AkAAAQAAhqBLhLQBLhLBqAAQBrAABLBLQBLBLAABqQAABrhLBLQhLBLhrAAQhqAAhLhLQhLhLAAhrg");
	this.shape_12.setTransform(13,13);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("rgba(255,61,78,0.149)").ss(4,1,1).p("AkYAAQAAh0BShSQBShSB0AAQB1AABSBSQBSBSAAB0QAAB1hSBSQhSBSh1AAQh0AAhShSQhShSAAh1g");
	this.shape_13.setTransform(13,13);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("rgba(255,61,78,0)").ss(4,1,1).p("AE0AAQAACAhaBaQhaBaiAAAQh/AAhahaQhahaAAiAQAAh/BahaQBahaB/AAQCAAABaBaQBaBaAAB/g");
	this.shape_14.setTransform(13,13);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).wait(1));

	// Layer 1
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF3D4E").s().p("AhbBcQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1QAAA2gmAmQgmAmg2AAQg1AAgmgmg");
	this.shape_15.setTransform(13,13);

	this.timeline.addTween(cjs.Tween.get(this.shape_15).wait(14));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,30,30);


(lib.gumbiczeleni = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CCCCCC").ss(4,1,1).p("ACCAAQAAA2gmAmQgmAmg2AAQg1AAgmgmQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1g");
	this.shape.setTransform(23.1,23.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#09C0C2").s().p("AhbBcQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1QAAA2gmAmQgmAmg2AAQg1AAgmgmg");
	this.shape_1.setTransform(23.1,23.1);

	this.instance = new lib.animacijagumbacopy();
	this.instance.parent = this;
	this.instance.setTransform(23.1,23.1,1,1,0,0,0,13,13);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.instance}]},1).wait(3));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.2)").s().p("AjFDGQhShSAAh0QAAhzBShSQBShSBzAAQB0AABSBSQBSBSAABzQAAB0hSBSQhSBSh0AAQhzAAhShSg");
	this.shape_2.setTransform(23.8,22.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4.2,-5.5,56,56);


(lib.gumbic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CCCCCC").ss(4,1,1).p("ACCAAQAAA2gmAmQgmAmg2AAQg1AAgmgmQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1g");
	this.shape.setTransform(23.1,23.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF3D4E").s().p("AhbBcQgmgmAAg2QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1QAAA2gmAmQgmAmg2AAQg1AAgmgmg");
	this.shape_1.setTransform(23.1,23.1);

	this.instance = new lib.animacijagumba();
	this.instance.parent = this;
	this.instance.setTransform(23.1,23.1,1,1,0,0,0,13,13);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.instance}]},1).wait(3));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.2)").s().p("AjFDGQhShSAAh0QAAhzBShSQBShSBzAAQB0AABSBSQBSBSAABzQAAB0hSBSQhSBSh0AAQhzAAhShSg");
	this.shape_2.setTransform(23.8,22.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4.2,-5.5,56,56);


(lib.boxpozadina = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1 copy
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#212B41").s().p("A35XSQhuAAAAhuMAAAgrHQAAhuBuAAMAvzAAAQBuAAAABuMAAAArHQAABuhuAAg");
	this.shape.setTransform(164,149);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.instance = new lib.glow();
	this.instance.parent = this;
	this.instance.setTransform(164,149,1,1,0,0,0,179,164);
	this.instance.alpha = 0.262;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.boxpozadina, new cjs.Rectangle(-15,-15,358,328), null);


(lib.box = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{VRUCICA:0,BOL_U_MISICIMA:9,DRHTAVICA:19,UMOR_SLABOST:29,ISCRPLJENOST:39,KASALJ:49,KIHANJE:59,ZACEPLJEN_NOS:69});

	// timeline functions:
	this.frame_0 = function() {
		this.close.addEventListener('click', function () {
			//alert("adasda");

			semafor_mobile.exportRoot.kontent.box.alpha=0;

		});
		this.stop();
	}
	this.frame_1 = function() {
		this.alpha = 0;

		var tween = createjs.Tween.get(this, {
			loop: false
		})
			.wait(500)
			.to({
				alpha: 1,
				visible: true
			}, 500);
	}
	this.frame_9 = function() {
		this.stop();
	}
	this.frame_10 = function() {
		this.alpha = 0;

		var tween = createjs.Tween.get(this, {
			loop: false
		})
			.wait(500)
			.to({
				alpha: 1,
				visible: true
			}, 500);
	}
	this.frame_19 = function() {
		this.stop();
	}
	this.frame_20 = function() {
		this.alpha = 0;

		var tween = createjs.Tween.get(this, {
			loop: false
		})
			.wait(500)
			.to({
				alpha: 1,
				visible: true
			}, 500);
	}
	this.frame_29 = function() {
		this.stop();
	}
	this.frame_30 = function() {
		this.alpha = 0;

		var tween = createjs.Tween.get(this, {
			loop: false
		})
			.wait(500)
			.to({
				alpha: 1,
				visible: true
			}, 500);
	}
	this.frame_39 = function() {
		this.stop();
	}
	this.frame_40 = function() {
		this.alpha = 0;

		var tween = createjs.Tween.get(this, {
			loop: false
		})
			.wait(500)
			.to({
				alpha: 1,
				visible: true
			}, 500);
	}
	this.frame_49 = function() {
		this.stop();
	}
	this.frame_50 = function() {
		this.alpha = 0;

		var tween = createjs.Tween.get(this, {
			loop: false
		})
			.wait(500)
			.to({
				alpha: 1,
				visible: true
			}, 500);
	}
	this.frame_59 = function() {
		this.stop();
	}
	this.frame_60 = function() {
		this.alpha = 0;

		var tween = createjs.Tween.get(this, {
			loop: false
		})
			.wait(500)
			.to({
				alpha: 1,
				visible: true
			}, 500);
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_70 = function() {
		this.alpha = 0;

		var tween = createjs.Tween.get(this, {
			loop: false
		})
			.wait(500)
			.to({
				alpha: 1,
				visible: true
			}, 500);
	}
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_80 = function() {
		this.alpha = 0;

		var tween = createjs.Tween.get(this, {
			loop: false
		})
			.wait(500)
			.to({
				alpha: 1,
				visible: true
			}, 500);
	}
	this.frame_89 = function() {
		this.stop();
	}
	this.frame_90 = function() {
		this.alpha = 0;

		var tween = createjs.Tween.get(this, {
			loop: false
		})
			.wait(500)
			.to({
				alpha: 1,
				visible: true
			}, 500);
	}
	this.frame_99 = function() {
		this.stop();
	}
	this.frame_100 = function() {
		this.alpha = 0;

		var tween = createjs.Tween.get(this, {
			loop: false
		})
			.wait(500)
			.to({
				alpha: 1,
				visible: true
			}, 500);
	}
	this.frame_109 = function() {
		this.stop();
	}
	this.frame_110 = function() {
		this.alpha = 0;

		var tween = createjs.Tween.get(this, {
			loop: false
		})
			.wait(500)
			.to({
				alpha: 1,
				visible: true
			}, 500);
	}
	this.frame_119 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(8).call(this.frame_9).wait(1).call(this.frame_10).wait(9).call(this.frame_19).wait(1).call(this.frame_20).wait(9).call(this.frame_29).wait(1).call(this.frame_30).wait(9).call(this.frame_39).wait(1).call(this.frame_40).wait(9).call(this.frame_49).wait(1).call(this.frame_50).wait(9).call(this.frame_59).wait(1).call(this.frame_60).wait(9).call(this.frame_69).wait(1).call(this.frame_70).wait(9).call(this.frame_79).wait(1).call(this.frame_80).wait(9).call(this.frame_89).wait(1).call(this.frame_90).wait(9).call(this.frame_99).wait(1).call(this.frame_100).wait(9).call(this.frame_109).wait(1).call(this.frame_110).wait(9).call(this.frame_119).wait(18));

	// close button
	this.close = new lib.close();
	this.close.parent = this;
	this.close.setTransform(303.5,-10.5,1,1,0,0,0,15.5,15.5);
	this.close._off = true;
	new cjs.ButtonHelper(this.close, 0, 1, 2, false, new lib.close(), 3);

	this.timeline.addTween(cjs.Tween.get(this.close).wait(1).to({_off:false},0).to({_off:true},119).wait(17));

	// 1. VRUCICA
	this.text = new cjs.Text("VRUĆICA", "bold 14px 'Arial'", "#FF3D4E");
	this.text.lineHeight = 18;
	this.text.lineWidth = 144;
	this.text.parent = this;
	this.text.setTransform(18,-18.4);

	this.text_1 = new cjs.Text("Gripa: visoka temperatura (38 do 40 stupnjeva)\n3 do 4 dana\n\nPrehlada: rijetko\n\nSavjet dr. Kuzmana:\n\nZa gripu je uz nagli početak karakteristična vrlo visoka temperature, nerijetko i viša od 40 stupnjeva. Tako visoka temperatura ostaje 2-4 dana, a potom se postupno normalizira. Većina virusnih infekcija gornjeg dijela dišnog sustava protječe bez povišene temperature, samo uz otežano disanje na nos i možda blagu grlobolju. Takvo stanje zovemo obična prehlada. No, prehlada može biti praćena i povišenom temperaturom, koja se pojavljuje i raste postupno.", "12px 'Arial'", "#FFFFFF");
	this.text_1.lineHeight = 15;
	this.text_1.lineWidth = 305;
	this.text_1.parent = this;
	this.text_1.setTransform(18.5,10);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.text_1},{t:this.text}]},1).to({state:[]},9).wait(127));

	// 2. BOL U MISICIMA
	this.text_2 = new cjs.Text("BOL U MIŠIĆIMA", "bold 14px 'Arial'", "#FF3D4E");
	this.text_2.lineHeight = 18;
	this.text_2.lineWidth = 144;
	this.text_2.parent = this;
	this.text_2.setTransform(19,-17.5);

	this.text_3 = new cjs.Text("Gripa: jaka bol\n\nPrehlada: blaga\n\nSavjet dr. Kuzmana:\n\nUz visoku temperaturu, bolesnici s gripom obično imaju bolove u mišićima (mialgije) i zglobovima (artralgije). Ponekad ti bolovi mogu biti i vrlo izraženi - a bolesnici ih opisuju kao da su pregaženi te moraju leći u krevet (“gripa obara u krevet”). U prehladama takvih bolova uglavnom nema.", "12px 'Arial'", "#FFFFFF");
	this.text_3.lineHeight = 15;
	this.text_3.lineWidth = 294;
	this.text_3.parent = this;
	this.text_3.setTransform(19.5,10.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.text_3},{t:this.text_2}]},10).to({state:[]},10).wait(117));

	// 3. DRHTAVICA
	this.text_4 = new cjs.Text("DRHTAVICA", "bold 14px 'Arial'", "#FF3D4E");
	this.text_4.lineHeight = 18;
	this.text_4.lineWidth = 144;
	this.text_4.parent = this;
	this.text_4.setTransform(20.5,-18);

	this.text_5 = new cjs.Text("Gripa: uobičajena\n\nPrehlada: nije uobičajena\n\nSavjet dr. Kuzmana:\n\nBolji je izraz svakako tresavica, a označava vidljivo podrhtavanje mišića zbog vrlo visoke temperature. Tako organizam nastoji samostalno sniziti vlastitu temperaturu (usporedi: lepeza, propeler). Osim jače tresavice, bolesnici opisuju i cvokotanje zubi, a ponekad daju slikovit opis “tresao se krevet na kojem sam ležao”. Bolesnici s prehladom u pravilu nemaju tresavicu. ", "12px 'Arial'", "#FFFFFF");
	this.text_5.lineHeight = 15;
	this.text_5.lineWidth = 294;
	this.text_5.parent = this;
	this.text_5.setTransform(20.5,10.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.text_5},{t:this.text_4}]},20).to({state:[]},10).wait(107));

	// 4. UMOR SLABOST
	this.text_6 = new cjs.Text("UMOR, SLABOST", "bold 14px 'Arial'", "#FF3D4E");
	this.text_6.lineHeight = 18;
	this.text_6.lineWidth = 144;
	this.text_6.parent = this;
	this.text_6.setTransform(20,-18);

	this.text_7 = new cjs.Text("Gripa: izražen umor koji može potrajati 2 do 3 tjedna\n\nPrehlada: blagi \n\nSavjet dr. Kuzmana:\n\nGripu u početku bolesti (dok je povišena temperature) prati i jaki umor, no umor i opća slabost ostaju nekoliko dana i nakon normalizacije temperature. Pri težim oblicima bolesti i umor može biti nazočan i nekoliko tjedana.  Za prehladu bez povišene temperature nije karakterističan umor, a uz temperaturu se može pojaviti u blažem obliku i kraćeg trajanja.\n", "12px 'Arial'", "#FFFFFF");
	this.text_7.lineHeight = 15;
	this.text_7.lineWidth = 294;
	this.text_7.parent = this;
	this.text_7.setTransform(20.5,10.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.text_7},{t:this.text_6}]},30).to({state:[]},10).to({state:[]},50).wait(47));

	// 5. ISCRPLJENOST
	this.text_8 = new cjs.Text("ISCRPLJENOST", "bold 14px 'Arial'", "#FF3D4E");
	this.text_8.lineHeight = 18;
	this.text_8.lineWidth = 144;
	this.text_8.parent = this;
	this.text_8.setTransform(18.5,-18);

	this.text_9 = new cjs.Text("Gripa: često prisutan simptom\n\nPrehlada: nikad\n\nSavjet dr. Kuzmana:\n\nSve navedeno kao umor i opća slabost uključuje i iscrpljenost. Iscrpljenost posebno ističemo pri duljem trajanju gripe, osobito ako se razviju komplikacije, kada se bolesnik vraća svojim uobičajenim aktivnostima tek za nekoliko tjedana ili mjeseci.\n\n", "12px 'Arial'", "#FFFFFF");
	this.text_9.lineHeight = 15;
	this.text_9.lineWidth = 294;
	this.text_9.parent = this;
	this.text_9.setTransform(19.5,10.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.text_9},{t:this.text_8}]},40).to({state:[]},10).wait(87));

	// 6. KAŠALJ
	this.text_10 = new cjs.Text("KAŠALJ", "bold 14px 'Arial'", "#FF3D4E");
	this.text_10.lineHeight = 18;
	this.text_10.lineWidth = 144;
	this.text_10.parent = this;
	this.text_10.setTransform(17,-18);

	this.text_11 = new cjs.Text("Gripa: suhi kašalj\n\nPrehlada: produktivni kašalj\n\nSavjet dr. Kuzmana:\n\nKašalj je učestaliji i uporniji u bolesnika s gripom nego u onih s prehladom. To je poglavito suhi, nadražajni kašalj koji jako iscrpljuje. Kašalj se može i treba ublažavati hidracijom (pijenjem velike količine vode i čaja) te ovlaživanjem zraka u sobi gdje bolesnik boravi. Manje značenje imaju lijekovi za suzbijanje kašlja.\n", "12px 'Arial'", "#FFFFFF");
	this.text_11.lineHeight = 15;
	this.text_11.lineWidth = 294;
	this.text_11.parent = this;
	this.text_11.setTransform(18.5,9.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.text_11},{t:this.text_10}]},50).to({state:[]},10).wait(77));

	// 7. KIHANJE
	this.text_12 = new cjs.Text("KIHANJE", "bold 14px 'Arial'", "#FF3D4E");
	this.text_12.lineHeight = 18;
	this.text_12.lineWidth = 144;
	this.text_12.parent = this;
	this.text_12.setTransform(22,-17.5);

	this.text_13 = new cjs.Text("Gripa: ponekad\n\nPrehlada: uobičajen simptom\n\nSavjet dr. Kuzmana:\n\nNema veće medicinsko značenje, a nije osobito neugodno ni za bolesnika. Nema bitne razlike u pojavnosti u različitim respiratornim infekcijama. Ne može se voljno kontrolirati.\n\n\n", "12px 'Arial'", "#FFFFFF");
	this.text_13.lineHeight = 15;
	this.text_13.lineWidth = 294;
	this.text_13.parent = this;
	this.text_13.setTransform(21.5,10.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.text_13},{t:this.text_12}]},60).to({state:[]},10).to({state:[]},20).wait(47));

	// 8. ZAČEPLJEN NOS
	this.text_14 = new cjs.Text("ZAČEPLJEN NOS", "bold 14px 'Arial'", "#FF3D4E");
	this.text_14.lineHeight = 18;
	this.text_14.lineWidth = 144;
	this.text_14.parent = this;
	this.text_14.setTransform(21.5,-17.5);

	this.text_15 = new cjs.Text("Gripa: ponekad\n\nPrehlada: česti simptom, spontano prolazi unutar tjedan dana\n\nSavjet dr. Kuzmana:\n\nPrirodno je imati prohodan nos. Zbog otoka sluznice u većine bolesnika s bilo kojom respiratornom infekcijom disanje na nos biva otežano.  Mogu pomoći inhalacije vodene pare podjednako kao i razne kapi za nos koje smanjuju otok sluznice. Kapi za nos treba rabiti što kraće jer imaju i neželjenih nuspojava među kojima je i navikavanje. U male djece treba savjesno mehanički, štapićima omotanim vatom odstranjivati sadržaj iz nosnih šupljina. \n", "12px 'Arial'", "#FFFFFF");
	this.text_15.lineHeight = 15;
	this.text_15.lineWidth = 294;
	this.text_15.parent = this;
	this.text_15.setTransform(21.5,10.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.text_15},{t:this.text_14}]},70).to({state:[]},10).wait(57));

	// 9. BOLNO GRLO
	this.text_16 = new cjs.Text("BOLNO GRLO", "bold 14px 'Arial'", "#FF3D4E");
	this.text_16.lineHeight = 18;
	this.text_16.lineWidth = 144;
	this.text_16.parent = this;
	this.text_16.setTransform(20,-18);

	this.text_17 = new cjs.Text("Gripa: ponekad\n\nPrehlada: uobičajeno \n\nSavjet dr. Kuzmana:\n\nNema neke bitne razlike. Grlobolja nije osobito istaknut simptom ni kod prehlade niti kod gripe. Jaka grlobolja s otežanim gutanjem karakteristična je za streptokoknu (gnojnu) anginu koju treba liječiti antibiotikom.\n\nBolesnici s gripom najčešće imaju osjećaj suhoće i pečenja ždrijela, a slično je i s drugim virusnim infekcijama.\n", "12px 'Arial'", "#FFFFFF");
	this.text_17.lineHeight = 15;
	this.text_17.lineWidth = 294;
	this.text_17.parent = this;
	this.text_17.setTransform(20.5,9.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.text_17},{t:this.text_16}]},80).to({state:[]},10).wait(47));

	// 10. pocetak bolesti
	this.text_18 = new cjs.Text("POČETAK BOLESTI", "bold 14px 'Arial'", "#09C0C2");
	this.text_18.lineHeight = 18;
	this.text_18.lineWidth = 160;
	this.text_18.parent = this;
	this.text_18.setTransform(20,-18);

	this.text_19 = new cjs.Text("Gripa: nagli početak, razvoj simptoma unutar 3-6 sati\n\nPrehlada: postepen razvoj bolesti\n\nSavjet dr. Kuzmana: \n\nGripa ili influenca (oba su naziva istoznačna)  nastupa vrlo naglo. U prvom su planu tzv. opći simptomi, odnosno vrlo visoka temperatura praćena glavoboljom i bolovima mišića, a nakon dan-dva pojavljuju se grlobolja i suhi kašalj. Druge virusne infekcije dišnog sustava (prehlade) počinju postupno, odnosno manje burno, a simptomi se razvijaju kroz nekoliko dana. ", "12px 'Arial'", "#FFFFFF");
	this.text_19.lineHeight = 15;
	this.text_19.lineWidth = 294;
	this.text_19.parent = this;
	this.text_19.setTransform(20.5,10.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.text_19},{t:this.text_18}]},90).to({state:[]},10).wait(37));

	// 11.trajanje bolesti
	this.text_20 = new cjs.Text("TRAJANJE BOLESTI", "bold 14px 'Arial'", "#09C0C2");
	this.text_20.lineHeight = 18;
	this.text_20.lineWidth = 163;
	this.text_20.parent = this;
	this.text_20.setTransform(20,-18);

	this.text_21 = new cjs.Text("Gripa: 7 do 10 dana ili dulje\n\nPrehlada: 3 do 5 dana\n\nSavjet dr. Kuzmana:  \n\nGripa u prethodno zdravih i vitalnih ljudi traje 4-6 dana, ali u bolesnika s kroničnim bolestima, odnosno u svih onih sa slabijom općom kondicijom simptomi mogu potrajati i do dva tjedna. Kada je riječ o komplikacijama koje su kod gripe relativno česte, trajanje bolesti može se protegnuti i do mjesec dana.  Prehlada uobičajeno traje kraće, a komplikacije su mnogo rjeđe. Savjet: mirovanje dok je povišena temperature.", "12px 'Arial'", "#FFFFFF");
	this.text_21.lineHeight = 15;
	this.text_21.lineWidth = 294;
	this.text_21.parent = this;
	this.text_21.setTransform(20.5,10.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.text_21},{t:this.text_20}]},100).to({state:[]},10).wait(27));

	// 12. komplikacije
	this.text_22 = new cjs.Text("KOMPLIKACIJE", "bold 14px 'Arial'", "#09C0C2");
	this.text_22.lineHeight = 18;
	this.text_22.lineWidth = 144;
	this.text_22.parent = this;
	this.text_22.setTransform(20,-18);

	this.text_23 = new cjs.Text("Gripa: bronhitis, upala pluća, pogoršanje postojeće kronične bolesti, može biti životno ugrožavajuća\n\nPrehlada: upala sinusa, upala srednjeg uha\n\nSavjet dr. Kuzmana:\n\nBudući da svi stariji od 65 godina, sve osobe s kroničnim bolestima, mala djeca i trudnice pripadaju rizičnim skupinama za češće i teže obolijevanje, odnosno pojavu komplikacija neizostavno se moraju cijepiti protiv gripe svaka godine.", "12px 'Arial'", "#FFFFFF");
	this.text_23.lineHeight = 15;
	this.text_23.lineWidth = 294;
	this.text_23.parent = this;
	this.text_23.setTransform(20.5,10.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.text_23},{t:this.text_22}]},110).to({state:[]},10).wait(17));

	// BOX
	this.instance = new lib.boxpozadina();
	this.instance.parent = this;
	this.instance.setTransform(0.5,-36);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).wait(136));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.asd = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();

		var frequency = 24;
		semafor_mobile.stage.enableMouseOver(frequency);




		this.gumb1.addEventListener('click', function () {
			semafor_mobile.exportRoot.kontent.box.gotoAndPlay(1);
		});

		this.gumb2.addEventListener('click', function () {
			semafor_mobile.exportRoot.kontent.box.gotoAndPlay(10);
		});


		this.gumb3.addEventListener('click', function () {
			semafor_mobile.exportRoot.kontent.box.gotoAndPlay(20);

		});

		this.gumb4.addEventListener('click', function () {
			semafor_mobile.exportRoot.kontent.box.gotoAndPlay(30);
		});

		this.gumb5.addEventListener('click', function () {
			semafor_mobile.exportRoot.kontent.box.gotoAndPlay(40);
		});

		this.gumb6.addEventListener('click', function () {
			semafor_mobile.exportRoot.kontent.box.gotoAndPlay(50);
		});
		this.gumb7.addEventListener('click', function () {
			semafor_mobile.exportRoot.kontent.box.gotoAndPlay(60);
		});

		this.gumb8.addEventListener('click', function () {
			semafor_mobile.exportRoot.kontent.box.gotoAndPlay(70);
		});

		this.gumb9.addEventListener('click', function () {
			semafor_mobile.exportRoot.kontent.box.gotoAndPlay(80);
		});

		this.gumb10.addEventListener('click', function () {
			semafor_mobile.exportRoot.kontent.box.gotoAndPlay(90);
		});

		this.gumb11.addEventListener('click', function () {
			semafor_mobile.exportRoot.kontent.box.gotoAndPlay(100);
		});

		this.gumb12.addEventListener('click', function () {
			semafor_mobile.exportRoot.kontent.box.gotoAndPlay(110);
		});








		this.gumb2.addEventListener('mouseover', function () {
			semafor_mobile.exportRoot.kontent.bol.gotoAndPlay(1);
		});

		this.gumb2.addEventListener('mouseout', function () {
			semafor_mobile.exportRoot.kontent.bol.gotoAndStop(0);
		});



		this.gumb3.addEventListener('mouseover', function () {
			semafor_mobile.exportRoot.kontent.drhtavica.gotoAndPlay(1);
		});

		this.gumb3.addEventListener('mouseout', function () {
			semafor_mobile.exportRoot.kontent.drhtavica.gotoAndStop(0);
		});
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// timer
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(-262.9,-152.9,1,1,0,0,0,16,14.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},1).wait(1));

	// centar!
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AurNhIAA7BIdXAAIAAbBg");
	this.shape.setTransform(94,86.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},1).wait(1));

	// text
	this.text = new cjs.Text("KOMPLIKACIJE", "10px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 13;
	this.text.lineWidth = 78;
	this.text.parent = this;
	this.text.setTransform(145.5,11.9);

	this.text_1 = new cjs.Text("TRAJANJE BOLESTI", "10px 'Arial'", "#FFFFFF");
	this.text_1.lineHeight = 13;
	this.text_1.lineWidth = 100;
	this.text_1.parent = this;
	this.text_1.setTransform(-88.5,-106.1);

	this.text_2 = new cjs.Text("POČETAK BOLESTI", "10px 'Arial'", "#FFFFFF");
	this.text_2.lineHeight = 13;
	this.text_2.lineWidth = 100;
	this.text_2.parent = this;
	this.text_2.setTransform(-86.5,-8.1);

	this.text_3 = new cjs.Text("BOLNO GRLO", "10px 'Arial'", "#FFFFFF");
	this.text_3.lineHeight = 13;
	this.text_3.lineWidth = 69;
	this.text_3.parent = this;
	this.text_3.setTransform(95.6,-67.1);

	this.text_4 = new cjs.Text("ZAČEPLJEN NOS", "10px 'Arial'", "#FFFFFF");
	this.text_4.lineHeight = 13;
	this.text_4.lineWidth = 100;
	this.text_4.parent = this;
	this.text_4.setTransform(99.5,-136.1);

	this.text_5 = new cjs.Text("KIHANJE", "10px 'Arial'", "#FFFFFF");
	this.text_5.lineHeight = 13;
	this.text_5.lineWidth = 49;
	this.text_5.parent = this;
	this.text_5.setTransform(30.5,-139.1);

	this.text_6 = new cjs.Text("KAŠALJ", "10px 'Arial'", "#FFFFFF");
	this.text_6.lineHeight = 13;
	this.text_6.lineWidth = 47;
	this.text_6.parent = this;
	this.text_6.setTransform(57.5,-3.1);

	this.text_7 = new cjs.Text("ISCRPLJENOST", "10px 'Arial'", "#FFFFFF");
	this.text_7.lineHeight = 13;
	this.text_7.lineWidth = 82;
	this.text_7.parent = this;
	this.text_7.setTransform(200.5,-162.1);

	this.text_8 = new cjs.Text("UMOR, SLABOST", "10px 'Arial'", "#FFFFFF");
	this.text_8.lineHeight = 13;
	this.text_8.lineWidth = 89;
	this.text_8.parent = this;
	this.text_8.setTransform(-52.5,-186.1);

	this.text_9 = new cjs.Text("DRHTAVICA", "10px 'Arial'", "#FFFFFF");
	this.text_9.lineHeight = 13;
	this.text_9.lineWidth = 61;
	this.text_9.parent = this;
	this.text_9.setTransform(185.5,-78.1);

	this.text_10 = new cjs.Text("BOL U MIŠIĆIMA", "10px 'Arial'", "#FFFFFF");
	this.text_10.lineHeight = 13;
	this.text_10.lineWidth = 85;
	this.text_10.parent = this;
	this.text_10.setTransform(-7.4,-63.1);

	this.text_11 = new cjs.Text("VRUĆICA", "10px 'Arial'", "#FFFFFF");
	this.text_11.lineHeight = 13;
	this.text_11.lineWidth = 48;
	this.text_11.parent = this;
	this.text_11.setTransform(87,-186.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.text_11},{t:this.text_10},{t:this.text_9},{t:this.text_8},{t:this.text_7},{t:this.text_6},{t:this.text_5},{t:this.text_4},{t:this.text_3},{t:this.text_2},{t:this.text_1},{t:this.text}]},1).wait(1));

	// kvadrati
	this.instance_1 = new lib.k1copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(137.1,7.2,1.363,1,0,0,0,-0.4,-0.1);

	this.instance_2 = new lib.k1copy();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-94.4,-111.3,1.668,1,0,0,0,-0.4,-0.1);

	this.instance_3 = new lib.k1copy();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-95.2,-13.3,1.644,1,0,0,0,-0.4,-0.1);

	this.instance_4 = new lib.k1();
	this.instance_4.parent = this;
	this.instance_4.setTransform(86.5,-72.8,1.243,1,0,0,0,-0.4,-0.1);

	this.instance_5 = new lib.k1();
	this.instance_5.parent = this;
	this.instance_5.setTransform(92,-141.3,1.476,1,0,0,0,-0.3,-0.1);

	this.instance_6 = new lib.k1();
	this.instance_6.parent = this;
	this.instance_6.setTransform(24.4,-144.3,0.867,1,0,0,0,-0.4,-0.1);

	this.instance_7 = new lib.k1();
	this.instance_7.parent = this;
	this.instance_7.setTransform(51.5,-9.3,0.778,1,0,0,0,-0.3,-0.1);

	this.instance_8 = new lib.k1();
	this.instance_8.parent = this;
	this.instance_8.setTransform(192.9,-167.8,1.364,1,0,0,0,-0.4,-0.1);

	this.instance_9 = new lib.k1();
	this.instance_9.parent = this;
	this.instance_9.setTransform(-61.4,-191.3,1.476,1,0,0,0,-0.3,-0.1);

	this.instance_10 = new lib.k1();
	this.instance_10.parent = this;
	this.instance_10.setTransform(178.7,-83.8,1.09,1,0,0,0,-0.2,-0.1);

	this.instance_11 = new lib.k1();
	this.instance_11.parent = this;
	this.instance_11.setTransform(-14.4,-69.3,1.427,1,0,0,0,-0.1,-0.1);

	this.instance_12 = new lib.k1();
	this.instance_12.parent = this;
	this.instance_12.setTransform(79,-192.4,0.919,1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_12},{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1}]},1).wait(1));

	// box
	this.box = new lib.box();
	this.box.parent = this;
	this.box.setTransform(93.1,226.7,1,1,0,0,0,164,115.5);

	this.timeline.addTween(cjs.Tween.get(this.box).to({_off:true},1).wait(1));

	// legenda
	this.text_12 = new cjs.Text("1. Vrućica\n\n2. Bol u mišićima\n\n3. Drhtavica\n\n4. Umor, slabost\n\n5. Iscrpljenost\n\n6. Kašalj\n\n7. Kihanje \n\n8. Začepljen nos\n\n9. Bolno grlo\n\n---------------------------\n\n10. pocetak bolesti\n\n11.trajanje bolesti\n\n12. komplikacije", "12px 'Arial'", "#FFFFFF");
	this.text_12.lineHeight = 15;
	this.text_12.lineWidth = 135;
	this.text_12.parent = this;
	this.text_12.setTransform(-507.4,-227);

	this.timeline.addTween(cjs.Tween.get(this.text_12).to({_off:true},1).wait(1));

	// tocke
	this.gumb11 = new lib.gumbiczeleni();
	this.gumb11.parent = this;
	this.gumb11.setTransform(-79.2,-94);
	new cjs.ButtonHelper(this.gumb11, 0, 1, 2, false, new lib.gumbiczeleni(), 3);

	this.gumb12 = new lib.gumbiczeleni();
	this.gumb12.parent = this;
	this.gumb12.setTransform(130.8,24);
	new cjs.ButtonHelper(this.gumb12, 0, 1, 2, false, new lib.gumbiczeleni(), 3);

	this.gumb10 = new lib.gumbiczeleni();
	this.gumb10.parent = this;
	this.gumb10.setTransform(-55.7,3.6);
	new cjs.ButtonHelper(this.gumb10, 0, 1, 2, false, new lib.gumbiczeleni(), 3);

	this.gumb9 = new lib.gumbic();
	this.gumb9.parent = this;
	this.gumb9.setTransform(82.1,-56.8);
	new cjs.ButtonHelper(this.gumb9, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb8 = new lib.gumbic();
	this.gumb8.parent = this;
	this.gumb8.setTransform(88.1,-125.8);
	new cjs.ButtonHelper(this.gumb8, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb7 = new lib.gumbic();
	this.gumb7.parent = this;
	this.gumb7.setTransform(29.1,-127.8);
	new cjs.ButtonHelper(this.gumb7, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb6 = new lib.gumbic();
	this.gumb6.parent = this;
	this.gumb6.setTransform(39.1,6.2);
	new cjs.ButtonHelper(this.gumb6, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb5 = new lib.gumbic();
	this.gumb5.parent = this;
	this.gumb5.setTransform(208.6,-149.3);
	new cjs.ButtonHelper(this.gumb5, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb4 = new lib.gumbic();
	this.gumb4.parent = this;
	this.gumb4.setTransform(-37.7,-173.3);
	new cjs.ButtonHelper(this.gumb4, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb3 = new lib.gumbic();
	this.gumb3.parent = this;
	this.gumb3.setTransform(163.1,-68.8);
	new cjs.ButtonHelper(this.gumb3, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb2 = new lib.gumbic();
	this.gumb2.parent = this;
	this.gumb2.setTransform(10.6,-51.8);
	new cjs.ButtonHelper(this.gumb2, 0, 1, 2, false, new lib.gumbic(), 3);

	this.gumb1 = new lib.gumbic();
	this.gumb1.parent = this;
	this.gumb1.setTransform(107.6,-156.3,1,1,0,0,0,23,23);
	new cjs.ButtonHelper(this.gumb1, 0, 1, 2, false, new lib.gumbic(), 3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.gumb1},{t:this.gumb2},{t:this.gumb3},{t:this.gumb4},{t:this.gumb5},{t:this.gumb6},{t:this.gumb7},{t:this.gumb8},{t:this.gumb9},{t:this.gumb10},{t:this.gumb12},{t:this.gumb11}]}).to({state:[]},1).wait(1));

	// gradient
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#182033","rgba(24,32,51,0)"],[0,1],0,98,0,-98).s().p("EgjiATYMAAAgmvMBHFAAAMAAAAmvg");
	this.shape_1.setTransform(98.6,289.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).to({_off:true},1).wait(1));

	// bol u misicima
	this.bol = new lib.bolumisicima();
	this.bol.parent = this;
	this.bol.setTransform(107.4,101.6,0.972,0.972,0,0,0,183.5,284.8);

	this.timeline.addTween(cjs.Tween.get(this.bol).to({_off:true},1).wait(1));

	// drhtavica
	this.drhtavica = new lib.drhtavica();
	this.drhtavica.parent = this;
	this.drhtavica.setTransform(106.6,102.8,0.972,0.972,0,0,0,183.6,284.8);

	this.timeline.addTween(cjs.Tween.get(this.drhtavica).to({_off:true},1).wait(1));

	// avatar
	this.instance_13 = new lib.avatar2();
	this.instance_13.parent = this;
	this.instance_13.setTransform(-103,-194,0.927,0.927);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).to({_off:true},1).wait(1));

	// pozadina
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#182031").s().p("EglLA0WMAAAhorMBKXAAAMAAABorg");
	this.shape_2.setTransform(99,105.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-509.4,-229.4,846.5,685.4);


// stage content:
(lib.avatar_mobile = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.kontent = new lib.asd();
	this.kontent.parent = this;
	this.kontent.setTransform(212,288,1,1,0,0,0,94,86.5);

	this.timeline.addTween(cjs.Tween.get(this.kontent).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-191.4,282.1,846.5,685.4);
// library properties:
lib.properties = {
	width: 400,
	height: 620,
	fps: 24,
	color: "#172233",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"img/avatar2.png", id:"avatar2"}
	],
	preloads: []
};




})(lib_semaform = lib_semaform||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib_semaform, images, createjs, ss, AdobeAn;