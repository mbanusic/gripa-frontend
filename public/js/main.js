(function() {
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
        
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Kako ljudi utječu na emisiju CO2 (%)'],
          ['Upotreba folsilnih goriva',     87],
          ['Land use changes',      9],
          ['Indusrijski procesi',  7]

        ]);

        var options = {
          title: 'Kako ljudi utječu na emisiju CO2 (%)',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
