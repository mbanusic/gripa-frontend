/* global $ */
import React, { Component } from 'react';
import './App.css';
import './components/slider.css';

import Menu from './components/Menu';
import Semafor from './components/Semafor';
import Vodic from './components/Vodic';
import Anketa from './components/Anketa';
import Graf1 from './components/Graf1';
import Graf2 from './components/Graf2';
import Graf3 from './components/Graf3';
import ShortNews from './components/ShortNews';
import Graf4 from './components/Graf4';
import Doktori from './components/Doktori';
import Karta from './components/Karta';
import Footer from './components/Footer';
import Numbers from './components/Numbers';
import Footer_end from './components/Footer_end';
import Adstudio from './components/Adstudio';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            url: 'http://gripa.inspirium.hr/api',
            doktor_url: 'http://gripa.inspirium.hr/api/question',
            county: 20,
            counties: [],
            symptoms: [],
            bubbles: [],
            bars: [],
            lines: [],
            circles: [],
            marks: [
                '12.12-18.12',
                '19.12-25.12',
                '20.12-01.01',
                '02.01-08.01',
                '09.01-15.01',
                '16.01-22.01',
                '23.01-29.01',
                '30.01-05.02',
                '06.02-12.02',
                '13.02-20.02',
                '20.02-26.02',
            ]
        }
    }
    componentDidMount() {
        const url = 'http://gripa.inspirium.hr/api';
        $.get(url, (r) => {
            if (r) {
                this.setState({counties: r.counties, symptoms: r.symptoms, bubbles: r.bubbles, bars: r.bars, lines: r.lines, circles: r.circles2});
            }
        });
    }
    render() {
        return (
            <div className="App">
                <div className="central">
                    <Menu/>
                    <Vodic/>
                    <Semafor/>
                    <Footer/>
                    <Doktori url={this.state.doktor_url}/>
                    <Anketa counties={this.state.counties} symptoms={this.state.symptoms} url={this.state.url}/>
                    <Graf1 counties={this.state.counties} symptoms={this.state.symptoms} data={this.state.bubbles} marks={this.state.marks}/>
                    <ShortNews/>
                    <Graf2 symptoms={this.state.symptoms} counties={this.state.counties} data={this.state.bars} marks={this.state.marks}/>
                    <Graf3 data={this.state.lines} counties={this.state.counties} symptoms={this.state.symptoms} />
                    <Numbers/>
                    <Graf4 data={this.state.circles}/>
                    <Karta/>
                    <Footer_end/>
                    <Adstudio/>


                </div>
            </div>
    );
    }
}

export default App;
