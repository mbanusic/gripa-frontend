/* global semafor, $, HumanAPI, semafor_mobile */
import React, { Component } from 'react';
import _ from 'underscore';

class Semafor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: window.innerWidth,
            modal: 0,
            ucitano:0,
            buttons: {
                0 :{
                    title: '',
                    content: '',
                    src: ''
                },
                1 :{
                    title: 'VRUĆICA',
                    content: '<p><strong>Gripa:</strong> visoka temperatura (38 do 40 stupnjeva) 3 do 4 dana<strong></p><p>Prehlada:</strong> rijetko</p><p><strong>Savjet dr. Kuzmana:</strong>Za gripu je uz nagli početak karakteristična vrlo visoka temperature, nerijetko i viša od 40 stupnjeva. Tako visoka temperatura ostaje 2-4 dana, a potom se postupno normalizira. Većina virusnih infekcija gornjeg dijela dišnog sustava protječe bez povišene temperature, samo uz otežano disanje na nos i možda blagu grlobolju.<br><br>Takvo stanje zovemo obična prehlada. No, prehlada može biti praćena i povišenom temperaturom, koja se pojavljuje i raste postupno.</p>',
                    src: 'https://human.biodigital.com/widget?be=1WT3&bgstd=24,32,49&ui-share=false&ui-fullscreen=false&ui-nav=false&ui-load-progress=false&pre=KLIKNI ZA INTERAKCIJU!&dk=d937b3a232e76f869f15f56fa1fa239e183f4893'
                },
                2 :{
                    title: 'BOL U MIŠIĆIMA',
                    content: '<p><strong>Gripa:</strong> jaka bol</p> <p><strong>Prehlada:</strong> blaga</p> <p><strong>Savjet dr. Kuzmana:</strong></p> <p>Uz visoku temperaturu, bolesnici s gripom obično imaju bolove u mišićima (mialgije) i zglobovima (artralgije). Ponekad ti bolovi mogu biti i vrlo izraženi - a bolesnici ih opisuju kao da su pregaženi te moraju leći u krevet (“gripa obara u krevet”). U prehladama takvih bolova uglavnom nema.</p>',
                    src: 'https://human.biodigital.com/widget?be=1WT7&bgstd=24,32,49&ui-share=false&ui-fullscreen=false&ui-nav=false&ui-load-progress=false&pre=KLIKNI ZA INTERAKCIJU!&dk=d937b3a232e76f869f15f56fa1fa239e183f4893'
                },
                3 :{
                    title: 'DRHTAVICA',
                    content: '<p><strong>Gripa:</strong> uobičajena<br><br></p> <p><strong>Prehlada:</strong> nije uobičajena</p> <p><strong>Savjet dr. Kuzmana:</strong></p> <p>Bolji je izraz svakako tresavica, a označava vidljivo podrhtavanje mišića zbog vrlo visoke temperature. Tako organizam nastoji samostalno sniziti vlastitu temperaturu (usporedi: lepeza, propeler). Osim jače tresavice, bolesnici opisuju i cvokotanje zubi, a ponekad daju slikovit opis “tresao se krevet na kojem sam ležao”. Bolesnici s prehladom u pravilu nemaju tresavicu. </p>',
                    src: 'https://human.biodigital.com/widget?be=1WfB&bgstd=24,32,49&ui-share=false&ui-fullscreen=false&ui-nav=false&ui-load-progress=false&pre=KLIKNI ZA INTERAKCIJU!&dk=d937b3a232e76f869f15f56fa1fa239e183f4893'
                },
                4 :{
                    title: 'UMOR, SLABOST',
                    content: '<p><strong>Gripa:</strong> izražen umor koji može potrajati 2 do 3 tjedna</p> <p><strong>Prehlada:</strong> blagi</p> <p><strong>Savjet dr. Kuzmana:</strong></p> <p> Gripu u početku bolesti (dok je povišena temperature) prati i jaki umor, no umor i opća slabost ostaju nekoliko dana i nakon normalizacije temperature. Pri težim oblicima bolesti i umor može biti nazočan i nekoliko tjedana.<br><br>Za prehladu bez povišene temperature nije karakterističan umor, a uz temperaturu se može pojaviti u blažem obliku i kraćeg trajanja.</p>',
                    src: 'https://human.biodigital.com/widget?be=1WfB&bgstd=24,32,49&ui-share=false&ui-fullscreen=false&ui-nav=false&ui-load-progress=false&pre=KLIKNI ZA INTERAKCIJU!&dk=d937b3a232e76f869f15f56fa1fa239e183f4893'
                },
                5 :{
                    title: 'ISCRPLJENOST',
                    content: '<p><strong>Gripa:</strong> često prisutan simptom</p><p><strong>Prehlada:</strong> nikad</p><p><strong>Savjet dr. Kuzmana:</strong></p><p>Sve navedeno kao umor i opća slabost uključuje i iscrpljenost. Iscrpljenost posebno ističemo pri duljem trajanju gripe, osobito ako se razviju komplikacije, kada se bolesnik vraća svojim uobičajenim aktivnostima tek za nekoliko tjedana ili mjeseci.</p>',
                    src: 'https://human.biodigital.com/widget?be=1WfB&bgstd=24,32,49&ui-share=false&ui-fullscreen=false&ui-nav=false&ui-load-progress=false&pre=KLIKNI ZA INTERAKCIJU!&dk=d937b3a232e76f869f15f56fa1fa239e183f4893'
                },
                6 :{
                    title: 'KAŠALJ',
                    content: '<p><strong>Gripa:</strong> suhi kašalj</p><p><strong>Prehlada:</strong>  produktivni kašalj</p><p><strong>Savjet dr. Kuzmana:</strong> </p><p>Kašalj je učestaliji i uporniji u bolesnika s gripom nego u onih s prehladom. To je poglavito suhi, nadražajni kašalj koji jako iscrpljuje.<br><br>Kašalj se može i treba ublažavati hidracijom (pijenjem velike količine vode i čaja) te ovlaživanjem zraka u sobi gdje bolesnik boravi. Manje značenje imaju lijekovi za suzbijanje kašlja.</p>',
                    src: 'https://human.biodigital.com/widget?be=1WTJ&bgstd=24,32,49&ui-share=false&ui-fullscreen=false&ui-nav=false&ui-load-progress=false&pre=KLIKNI ZA INTERAKCIJU!&dk=d937b3a232e76f869f15f56fa1fa239e183f4893'
                },
                7 :{
                    title: 'KIHANJE',
                    content: '<p><strong>Gripa:</strong> ponekad</p><p><strong>Prehlada:</strong> uobičajen simptom</p><p><strong>Savjet dr. Kuzmana:</strong></p><p>Nema veće medicinsko značenje, a nije osobito neugodno ni za bolesnika. Nema bitne razlike u pojavnosti u različitim respiratornim infekcijama. Ne može se voljno kontrolirati.</p>',
                    src: 'https://human.biodigital.com/widget?be=1WTQ&bgstd=24,32,49&ui-share=false&ui-fullscreen=false&ui-nav=false&ui-load-progress=false&pre=KLIKNI ZA INTERAKCIJU!&dk=d937b3a232e76f869f15f56fa1fa239e183f4893'
                },
                8 :{
                    title: 'ZAČEPLJEN NOS',
                    content: '<p><strong>Gripa:</strong> ponekad</p><p><strong>Prehlada:</strong> česti simptom, spontano prolazi unutar tjedan dana</p><p><strong>Savjet dr. Kuzmana:</strong></p><p>Prirodno je imati prohodan nos. Zbog otoka sluznice u većine bolesnika s bilo kojom respiratornom infekcijom disanje na nos biva otežano. Mogu pomoći inhalacije vodene pare podjednako kao i razne kapi za nos koje smanjuju otok sluznice.<br><br>Kapi za nos treba rabiti što kraće jer imaju i neželjenih nuspojava među kojima je i navikavanje. U male djece treba savjesno mehanički, štapićima omotanim vatom odstranjivati sadržaj iz nosnih šupljina. </p>',
                    src: 'https://human.biodigital.com/widget?be=1WTQ&bgstd=24,32,49&ui-share=false&ui-fullscreen=false&ui-nav=false&ui-load-progress=false&pre=KLIKNI ZA INTERAKCIJU!&dk=d937b3a232e76f869f15f56fa1fa239e183f4893'
                },
                9 :{
                    title: 'BOLNO GRLO',
                    content: '<p><strong>Gripa:</strong> ponekad</p><p><strong>Prehlada:</strong> uobičajeno</p><p><strong>Savjet dr. Kuzmana:</strong></p><p>Nema neke bitne razlike. Grlobolja nije osobito istaknut simptom ni kod prehlade niti kod gripe. Jaka grlobolja s otežanim gutanjem karakteristična je za streptokoknu (gnojnu) anginu koju treba liječiti antibiotikom. Bolesnici s gripom najčešće imaju osjećaj suhoće i pečenja ždrijela, a slično je i s drugim virusnim infekcijama.</p>',
                    src: 'https://human.biodigital.com/widget?be=1WTV&bgstd=24,32,49&ui-share=false&ui-fullscreen=false&ui-nav=false&ui-load-progress=false&pre=KLIKNI ZA INTERAKCIJU!&dk=d937b3a232e76f869f15f56fa1fa239e183f4893'
                },
                10 :{
                    title: 'POČETAK BOLESTI',
                    content: '<p><strong>Gripa:</strong> nagli početak, razvoj simptoma unutar 3-6 sati</p><p><strong>Prehlada:</strong> postepen razvoj bolesti</p><p><strong>Savjet dr. Kuzmana:</strong></p><p>Gripa ili influenca (oba su naziva istoznačna)  nastupa vrlo naglo. U prvom su planu tzv. opći simptomi, odnosno vrlo visoka temperatura praćena glavoboljom i bolovima mišića, a nakon dan-dva pojavljuju se grlobolja i suhi kašalj.<br><br> Druge virusne infekcije dišnog sustava (prehlade) počinju postupno, odnosno manje burno, a simptomi se razvijaju kroz nekoliko dana. </p>',
                    src: 'https://human.biodigital.com/widget?be=1WfB&bgstd=24,32,49&ui-share=false&ui-fullscreen=false&ui-nav=false&ui-load-progress=false&pre=KLIKNI ZA INTERAKCIJU!&dk=d937b3a232e76f869f15f56fa1fa239e183f4893'
                },
                11 :{
                    title: 'TRAJANJE BOLESTI',
                    content: '<p><strong>Gripa:</strong> 7 do 10 dana ili dulje</p><p><strong>Prehlada:</strong> 3 do 5 dana</p><p><strong>Savjet dr. Kuzmana:</strong></p><p>Gripa u prethodno zdravih i vitalnih ljudi traje 4-6 dana, ali u bolesnika s kroničnim bolestima, odnosno u svih onih sa slabijom općom kondicijom simptomi mogu potrajati i do dva tjedna. Kada je riječ o komplikacijama koje su kod gripe relativno česte, trajanje bolesti može se protegnuti i do mjesec dana.<br><br>Prehlada uobičajeno traje kraće, a komplikacije su mnogo rjeđe. Savjet: mirovanje dok je povišena temperature.</p>',
                    src: 'https://human.biodigital.com/widget?be=1WfB&bgstd=24,32,49&ui-share=false&ui-fullscreen=false&ui-nav=false&ui-load-progress=false&pre=KLIKNI ZA INTERAKCIJU!&dk=d937b3a232e76f869f15f56fa1fa239e183f4893'
                },
                12: {
                    title: 'KOMPLIKACIJE',
                    content: '<p><strong>Gripa:</strong> bronhitis, upala pluća, pogoršanje postojeće kronične bolesti, može biti životno ugrožavajuća</p><p><strong>Prehlada:</strong> upala sinusa, upala srednjeg uha</p><p><strong>Savjet dr. Kuzmana:</strong></p><p>Budući da svi stariji od 65 godina, sve osobe s kroničnim bolestima, mala djeca i trudnice pripadaju rizičnim skupinama za češće i teže obolijevanje, odnosno pojavu komplikacija neizostavno se moraju cijepiti protiv gripe svaka godine.</p>',
                    src: 'https://human.biodigital.com/widget?be=1WLK&bgstd=24,32,49&ui-share=false&ui-fullscreen=false&ui-nav=false&ui-load-progress=false&pre=KLIKNI ZA INTERAKCIJU!&dk=d937b3a232e76f869f15f56fa1fa239e183f4893'
                }
            }
        };
    }
    componentDidMount() {
        if (this.state.width > 800) {
            semafor.init();
            this.refs.semafor.addEventListener("symptom_click", (e) => this.handleSymptomClick(e));
            this.refs.prozor.addEventListener("hide.bs.modal", (e) => this.modalClose(e));
        }
        else {
            this.setState({modal:10});
            //semafor_mobile.init();
        }
    }
    componentWillUnmount() {
        this.refs.semafor.removeEventListener("symptom_click", this.handleSymptomClick);
        this.refs.prozor.removeEventListener("hide.bs.modal", this.modalClose);
    }
    handleSymptomClick(e) {
        const gumb = e.detail.gumb;
        //load modal
        this.setState({modal:gumb});
        $('#symptomd').modal('toggle');
        let human = new HumanAPI("embedded-human");
        human.send("input.disable");
        human.on("human.ready", function() {
            this.setState({ucitano:1});
            (function update() {
                // Orbit camera horizontally around target
                human.send("camera.orbit", {
                    yaw: 0.2
                });
                requestAnimationFrame(update);
            })();
            human.send("input.disable");
        }.bind(this));
    }
    handleSymptomClickm(e, index) {
        this.setState({modal:index});
    }
    modalClose() {
        this.setState({modal:0, ucitano:0});
    }
    render() {
        let canvas, modals = null;
        if (this.state.width>800){
            canvas = (<canvas ref="semafor" id="canvas_semafor" width="600" height="632" style={{backgroundColor:'rgba(24, 32, 51, 1.00)'}}/>);
            modals = (<div id="symptomd" className="modal fade" ref="prozor">
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <button type="button" className="close" data-dismiss="modal" onClick={() => this.modalClose()}><span>&times;</span></button>

                        <div className="modal-body row">
                            <div id="test_3d" className="col-md-6">
                                <iframe
                                    id="embedded-human"
                                    frameBorder="0"
                                    width="400"
                                    height="410"
                                    allowFullScreen="true"
                                    src={this.state.buttons[this.state.modal].src}>
                                </iframe>
                                <div className="ucitavanje" style={{display:this.state.ucitano?'none':'block'}}/>
                            </div>
                            <div className="col-md-6 symptom_desc">
                                <h4 className="modal_title">{this.state.buttons[this.state.modal].title}</h4>
                                <div className="modal_desc" dangerouslySetInnerHTML={{__html:this.state.buttons[this.state.modal].content}}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>);
        }
        else {
            canvas = (
                <div id="avatar_2d" className="symptom_desc">
                    <div id="tocka_crvena_4" className="crvena" onClick={(e) => {this.handleSymptomClickm(e, 4)}}/>
                    <div id="tocka_crvena_1" className="crvena" onClick={(e) => {this.handleSymptomClickm(e, 1)}} />
                    <div id="tocka_crvena_5" className="crvena" onClick={(e) => {this.handleSymptomClickm(e, 5)}} />
                    <div id="tocka_zelena_11" className="zelena" onClick={(e) => {this.handleSymptomClickm(e, 11)}} />
                    <div id="tocka_crvena_7" className="crvena" onClick={(e) => {this.handleSymptomClickm(e, 7)}} />
                    <div id="tocka_crvena_8" className="crvena" onClick={(e) => {this.handleSymptomClickm(e, 8)}} />
                    <div id="tocka_crvena_2" className="crvena" onClick={(e) => {this.handleSymptomClickm(e, 2)}}/>
                    <div id="tocka_crvena_9" className="crvena" onClick={(e) => {this.handleSymptomClickm(e, 9)}} />
                    <div id="tocka_crvena_3" className="crvena" onClick={(e) => {this.handleSymptomClickm(e, 3)}} />
                    <div id="tocka_zelena_10" className="zelena" onClick={(e) => {this.handleSymptomClickm(e, 10)}} />
                    <div id="tocka_crvena_6" className="crvena" onClick={(e) => {this.handleSymptomClickm(e, 6)}} />
                    <div id="tocka_zelena_12" className="zelena" onClick={(e) => {this.handleSymptomClickm(e, 12)}} />
                    <div id="avatar_box">
                        <div id="avatar_naslov" className={_.contains([10,11,12], this.state.modal)?'modal-title blue':'modal-title'} >{this.state.buttons[this.state.modal].title}</div>
                        <div id="avatar_tekst" className="modal_desc" dangerouslySetInnerHTML={{__html:this.state.buttons[this.state.modal].content}}/>
                    </div>
                </div>);
        }
        return (
            <div id="semafor" className="row">
                <div className="col-md-4 col-xs-12">
                    <h2>NAJTOČNIJI VODIČ</h2>
                    <h3>KAKO PREPOZNATI SIMPTOME GRIPE I PREHLADE</h3>
                    <p>Kako raste temperatura ako ste prehlađeni, a što se događa ako imate gripu? Kako izgleda gripa na početku, koliko traje i koje su najčešće komplikacije? Zašto ne treba ignorirati simptome? Koji su organi najčešće zahvaćeni gripom, a koji prehladom? Istražite interaktivni 3D model ljudskog tijela i saznajte korisne informacije o gripi i prehladi kroz autentične prikaze dijelova ljudskog tijela kako biste na vrijeme prepoznali simptome i poduzeli odgovarajući postupak liječenja. </p>
                </div>
                <div className="col-md-8 col-xs-12" id="box">
                    {canvas}
                </div>
                {modals}
            </div>
        );
    }
}

export default Semafor;