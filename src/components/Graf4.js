/* global $ */
import React, { Component } from 'react';
import karta from '../img/karta_hrv_regije.png';

class Graf4 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            week: 0,
            sum: 0,
            data: {
                1: {
                        week: 0,
                        mark: '',
                        data: [1,1,1,1,1]
                    },
                2:
                    {
                        week: 0,
                        mark: '',
                        data: [1,1,1,1,1]
                    }
            },
            slider: {
                min: 1,
                max: 2,
                marks: []
            },
            svg: {
                width: 0,
                height: 0
            },
            counties: [
                'Središnja Hrvatska',
                'Sjeverozapadna Hrvatska',
                'Istočna Hrvatska',
                'Sjeverni Jadran i Lika',
                'Srednji i južni Jadran'
            ],
            county: 0,
            answer: 0,
            anketa: 1
        }
    }
    componentDidUpdate() {
        if (this.refs.karta) {
            setTimeout(() => {
                this.setState({svg: {width: this.refs.karta.width, height:this.refs.karta.height}});
            }, 200);
        }

    }
    changeCounty(e, index) {
        e.preventDefault();
        if ('open' === index) {
            this.setState({modal:1});
        }
        else if ('close' === index) {
            this.setState({modal:0});
        }
        else {
            this.setState({county: index, modal:0});
        }
    };
    handleClick(e, answer) {
        e.preventDefault();
        this.setState({answer:answer});
    }
    handleSubmit(e) {
        e.preventDefault();
        if (this.state.answer) {
            $.post('http://gripa.inspirium.hr/api/anketa', {
                answer: this.state.answer,
                county: this.state.county
            });
            this.setState({anketa: 0});
        }
    }
    render() {
        if (this.props.data.length === 0) {
            return null;
        }
        const regions = [
            {title:"sredisnja", x:290 * this.state.svg.width/610, y: 160 * this.state.svg.height/597},
            {title:"sjeverozapadna", x:315* this.state.svg.width/610, y: 45* this.state.svg.height/597},
            {title:"istocna", x:490* this.state.svg.width/610, y: 160* this.state.svg.height/597},
            {title:"sjeverni", x:115* this.state.svg.width/610, y: 220* this.state.svg.height/597},
            {title:"juzni", x:338* this.state.svg.width/610, y: 450* this.state.svg.height/597}
        ];
        const r = this.props.data[this.state.week].data,
            sum = this.props.data[this.state.week].sum;
        const circles = r.map((r, index) => {
            const width = window.innerWidth<800?60:100;
            const font = window.innerWidth<800?10:16;
            const regija = regions[index];
            const radius = r/sum * width;
            return (
                <g key={index}>
                    <text x={regija.x-5} y={(regija.y-radius-5)} stroke="white" fill="white" fontSize={font}>{r}</text>
                <circle  cx={regija.x} cy={regija.y} r={radius} stroke="darkred" fill="darkred" />
                </g>
            )
        });
        const counties = this.state.counties.map((county, index) => {
            return (<li key={index} className="col-xs-12"><a href="#" onClick={(e) => {this.changeCounty(e, index)}}>{county}</a></li>);
        });
        const modal = (
            <div className="over_modal">
                <div className="overlay" onClick={(e) => this.changeCounty(e,'close')}/>
                <ul className="county_modal row">
                    {counties}
                </ul>
            </div>
        );
        const anketa = this.state.anketa?
            (<div>
                <p className="red big m_t_60">Anketa</p>
                <h2>Imate li gripu?</h2>
                <a className={this.state.answer==='da'?"a_question selected":"a_question"} href="#" onClick={(e) => this.handleClick(e, 'da')}>Da</a><a className={this.state.answer==='ne'?"a_question selected":"a_question"} href="#" onClick={(e) => this.handleClick(e,'ne')}>Ne</a>
                <h2 className="m_b_15">Gdje živite?</h2>
                <p className="county" onClick={(e) => this.changeCounty(e,'open')}>{this.state.counties[this.state.county]}</p>
                {this.state.modal?modal:null}
                <a className="a_send" href="#" onClick={(e) => this.handleSubmit(e)}>Pošalji</a>
            </div>)
            :(<h1 className="hvala">Hvala</h1>);
        return (
            <div id="graf4" className="row karta relative">
                <div className="col-md-4">
                    <h2>GRIPA ALERT</h2>
                    <h1>Koliko ima slučajeva gripe* u Hrvatskoj?</h1>
                    {anketa}
                </div>
                <p className="notification">*broj slučajeva gripe ne odnosi se na službene podatke HZJZ-a nego se temelji na vašem izjašnjavanju u ovoj anketi</p>
                <div className="col-md-8 relative">
                    <img ref="karta" src={karta} alt="karta Hrvatske sa županijama" role="presentation"/>
                    <svg width={this.state.svg.width} height={this.state.svg.height} style={{position:'absolute', top:0, left:0}}>
                        {circles}
                    </svg>
                    <div className="bosna">
                        <h2 className="">UKUPNO:</h2>
                        <p className="bigger">{sum}</p>
                        <p className="small">zbirni broj prijavljenih slučajeva u anketi</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Graf4;