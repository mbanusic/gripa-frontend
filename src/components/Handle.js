import React, { Component } from 'react';
import handle from '../img/handle_img.svg';

class Handle extends Component {
    render() {
        const handle_style = {
            position: 'absolute',
            display: 'block',
            width: 20,
            height: 30,
            top: 9,
            transform: 'translate(-50%, -50%)',
            cursor: 'pointer',
            backgroundColor: '#08c3c6',
            left: this.props.offset+'%',
            backgroundImage: "url(" + handle + ")",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center"
        };
        let span_style = {
            top: 30,
            fontSize: 14,
            left: -30,
            width: 200,
            position: 'absolute'
        };
        if (this.props.marks.length > 1 && this.props.max === this.props.value && window.innerWidth < 800) {
            span_style.left = -200;
        }
        return (
            <div style={handle_style}><span style={span_style}>{this.props.marks[this.props.value]}</span></div>
        );
    }
}

export default Handle