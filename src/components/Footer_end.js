import React, { Component } from 'react';
import foto from '../img/dijete.jpg';
import foto1 from '../img/bolno-grlo.jpg';
import foto2 from '../img/vrucica.jpg';

class Footer extends Component {
    render() {
        return (
            <div>
                <div id="vijesti" className="row">
                    <h1 className="news_heading_1">Aktualno</h1>
                    <div className="vijesti_box">
            
                        <a href="http://www.jutarnji.hr/life/zdravlje/vecina-oboljelih-od-gripe-u-hrvatskoj-su-djeca-osam-savjeta-pedijatara-kako-zastititi-djecu-od-virusa-i-osigurati-da-ostanu-zdravi/5425479/">
                        <div><img src={foto} role="presentation" width="350" className="#"/>
                            <div className="vijest_title"><span>VEĆINA OBOLJELIH OD GRIPE U HRVATSKOJ SU DJECA:</span> Osam korisnih savjeta za mame</div>
                        </div>
                        </a>
            
                        <a href="http://www.jutarnji.hr/zivim/mitovi-i-istina-o-gripi-osam-cinjenica-o-najrasirenijoj-bolesti-koje-ce-vas-iznenaditi-i-pomoci-da-ostanete-zdravi/5381279/">
                        <div><img src={foto1} role="presentation" width="350" className="#"/>
                            <div className="vijest_title"><span>MITOVI I ISTINA O GRIPI:</span> Osam činjenica o najraširenijoj bolesti koje će vas iznenaditi</div>
                        </div>
                        </a>
            
                        <a href="http://www.jutarnji.hr/life/zdravlje/epidemija-gripe-na-vrhuncu-kako-se-zastititi-od-zaraze-u-prvom-tjednu-skole-nakon-praznika/5500755/">
                        <div><img src={foto2} role="presentation" width="350" className="#"/>
                            <div className="vijest_title"><span>EPIDEMIJA GRIPE NA VRHUNCU</span> Kako se zaštititi od zaraze u školi</div>
                        </div>
                        </a>
            
                    </div>
                </div>
                
            </div>
        );
    }
}    

export default Footer;