import React, { Component } from 'react';
import { VictoryChart, VictoryLine, VictoryAxis } from 'victory';
import theme from './theme';
import _ from 'underscore';

class Graf3 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            county: 0,
            symptoms: [0,1],

        }
    }

    changeCounty(e, index) {
        e.preventDefault();
        if ('open' === index) {
            this.setState({modal:1});
        }
        else if ('close' === index) {
            this.setState({modal:0});
        }
        else {
            this.setState({county: index, modal:0});
        }
    };
    selectSymptoms(index) {
        let state = this.state.symptoms;
        if (_.contains(state, index)) {
            state.splice(state.indexOf(index), 1);
        }
        else {
            state.push(index);
        }

        this.setState({symptoms: state});
    }
    render() {
        if (this.props.data.length === 0) {
            return null;
        }
        const lines = this.state.symptoms.map((symptom, index) => {
            return (<VictoryLine key={index}
                data={this.props.data[this.state.county][symptom]}
                                 style={{
                                     data: {stroke:this.props.symptoms[symptom].color}
                                     }}
            />);
        });
        const symptoms = this.props.symptoms.map((symptom, index) => {
            const style = {
                color: _.contains(this.state.symptoms, index)?symptom.color:'#ffffff'
            };
            return (
                <li key={index} style={style} onClick={() => this.selectSymptoms(index)}>{symptom.title}</li>
            )
        });
        const counties = this.props.counties.map((county, index) => {
            return (<li key={index} className="col-xs-6"><a href="#" onClick={(e) => {this.changeCounty(e, index)}}>{county.title}</a></li>);
        });
        const modal = (
            <div>
                <div className="overlay" onClick={(e) => this.changeCounty(e,'close')}/>
                <ul className="county_modal row">
                    {counties}
                </ul>
            </div>
        );
        return (
            <div id="graf3" className="row">
                <h2>Kako se osjećaš danas</h2>
                <h1>Kako se simptomi u vašoj blizini pojavljuju i nestaju</h1>
                <div className="col-md-4 ticker">
                    <p className="county" onClick={(e) => this.changeCounty(e, 'open')}>{this.props.counties[this.state.county].title}</p>
                    {this.state.modal?modal:null}
                    <ul className="symps">
                    {symptoms}
                    </ul>
                </div>
                <div className="col-md-8">
                    <VictoryChart theme={theme.theme} height={320}>
                        <VictoryAxis crossAxis={true}
                                     tickValues={[
                                         'prije 5 dana',
                                         'prije 4 dana',
                                         'prije 3 dana',
                                         'prekjučer',
                                         'jučer',
                                         'danas',
                                     ]}
                        />
                        <VictoryAxis dependentAxis={true} tickCount={5} crossAxis={true}/>
                        {lines}
                    </VictoryChart>
                </div>
            </div>
        );
    }
}

export default Graf3;