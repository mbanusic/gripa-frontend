/* global karta */
import React, { Component } from 'react';
import eu1 from '../img/mobile_intezitet.gif';
import eu2 from '../img/mobile_sirenje.gif';
let timeout = false;
let delta = 200;
let rtime;

class Karta extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: window.innerWidth,
            init: 0
        }
    }
    updateSize() {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(this.resizeEnd.bind(this), delta);
        }
    }
    resizeEnd() {
        if (new Date() - rtime < delta) {
            setTimeout(this.resizeEnd.bind(this), delta);
        } else {
            timeout = false;
            this.setState({width:window.innerWidth, init:window.innerWidth<800?0:this.state.init});
        }
    }
    componentDidUpdate() {
        if (this.state.width > 800 && !this.state.init) {
            karta.init();
            this.setState({init:1});
        }
    }
    componentWillMount() {
        window.addEventListener('resize', () => this.updateSize());
    }
    componentWillUnmount() {
        window.removeEventListener('resize', () => this.updateSize());
    }
    componentDidMount() {
        if (this.state.width > 800 && !this.state.init) {
            karta.init();
            this.setState({init:1});
        }
    }
    render() {
        if (this.state.width > 800) {
            return (
                <div id="graf6" className="row">
                    <div className="col-md-12">
                        <h2>GRIPA ALERT</h2>
                        <h1>Kako se gripa širi Europom. Istraži interaktivnu
                            mapu</h1>
                        <canvas id="canvas" width="800" height="700" style={{
                            position: 'relative',
                            display: 'block',
                            backgroundColor: 'rgba(255, 255, 255, 1.00)',
                            margin: '30px auto'
                        }}/>
                        <p className="credit_map">World Health Organization
                            2016./European Centre for Disease Prevention and
                            Control 2016</p>
                    </div>
                </div>
            );
        }
        else {
            return (
                <div id="graf6" className="row">
                    <div className="col-xs-12">
                    <h2>GRIPA ALERT</h2>
                    <h1>Kako se gripa širi Europom. Istraži interaktivnu
                        mapu</h1>
                    <img className="eu_map_mobile" role="presentation"
                         src={eu1}/>
                    <img className="eu_map_mobile" role="presentation"
                         src={eu2}/>
                    </div>
                </div>
            );
        }
    }
}

export default Karta;