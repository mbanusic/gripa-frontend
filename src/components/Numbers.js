import React, { Component } from 'react';

class Numbers extends Component {
    render() {
        return (
            <div>
                <div id="numbers" className="row">
                    <h1><span>460.000</span> oboljelih 1958.</h1>
                    <h1><span>200.000</span> oboljelih 1999.</h1>
                </div>
            </div>
        );
    }
}

export default Numbers;