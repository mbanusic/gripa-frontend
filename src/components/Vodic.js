/* global $ */
import React, { Component } from 'react';
import video from '../img/Avatar_5.mp4';

class Vodic extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: '100%',
            height: 600
        }
    }
    componentDidMount() {
        const ratio = 352/335;
        let width= $(this.refs.video).width();
        this.setState({height: width * ratio});
    }
    render() {
        return (
            <div id="vodic" className="row">
                    <h2 className="pliva">OMOGUĆAVA MAXIRINO PLUS</h2>
                    <div className="col-md-6 col-md-push-6 col-xs-12">
                    <h3>VELIKI SPECIJAL<span><br/>Gripa i prehlada u <br/>realnom vremenu</span></h3>
                    <div className="redbox"> Otkrijte simptome na vrijeme i zaštitite sebe i obitelj</div>
                    <p>Gripa je stigla u Hrvatsku 27. studenog, mjesec dana ranije nego što je uobičajeno, a očekuje se da će trajati najmanje do ožujka. Svake godine u Hrvatskoj od gripe oboli u prosjeku 70 tisuća ljudi. Kako na vrijeme prepoznati simptome te najraširenije bolesti? Kako razlikovati imate li gripu ili prehladu? Kako se virus gripe širi Europom i Hrvatskom? <br/><br/>
Prvi puta u Hrvatskoj na jednostavan i zanimljiv način možete pretraživati relevantne i korisne činjenice o gripi u realnom vremenu i povijesne podatke da biste mogli zaštititi obitelj i sebe. Sadržaj je napravljen u suradnji sa Hrvatskim zavodom za javno zdravstvo, Zavodom za akutno respiratorne infekcije Klinike za infektivne bolesti dr. Fran Mihaljević, newyorškom kompanijom BioDigital Systems     i New York School of Medicine koji su napravili najdetaljniji 3D prikaz ljudskog tijela. Koristi se na medicinskim fakultetima za proučavanje anatomije, a svaki, i najmanji dio 3D modela, na kojemu možete pretraživati simptome po organima, predstavlja autentični dio ljudskog tijela. <br/><br/>
Na sva vaša pitanja odgovaraju vodeći stručnjaci u Hrvatskoj za gripu doktori Vladimir Draženović i Ilija Kuzman. </p>
                </div>
                <div className="col-md-6 col-md-pull-6 relative">
                    <div ref="maska" className="png-maska" style={{width: this.state.width, height: this.state.height}}/>
                    <video ref="video" autoPlay muted loop playsInline>
                        <source className="mask" src={video} type="video/mp4"/>
                    </video>
                </div>
            </div>
        );
    }
}

export default Vodic;