import _ from 'underscore';

// Colors
const colors = [
    "#1b404d",
    "#525252",
    "#737373",
    "#969696",
    "#bdbdbd",
    "#d9d9d9",
    "#f0f0f0"
];

const charcoal = "#1b404d";

// Typography
const sansSerif = "'Arial', 'Gill Sans MT', 'Ser­avek', 'Trebuchet MS', sans-serif";
const letterSpacing = "normal";
const fontSize = 14;

// Layout
const baseProps = {
    width: 650,
    height: 420,
    padding: {left: 50, right:50, top:10, bottom:50},
    colorScale: colors
};

// Labels
const baseLabelStyles = {
    fontFamily: sansSerif,
    fontSize,
    letterSpacing,
    padding: 10,
    fill: charcoal,
    stroke: "transparent",
    marginRight: 10
};

const centeredLabelStyles = _.assign({ textAnchor: "middle" }, baseLabelStyles);

// Strokes
const strokeLinecap = "round";
const strokeLinejoin = "round";

// Create the theme
const theme = {
    axis: _.assign({
        style: {
            axis: {
                fill: "transparent",
                stroke: "transparent",
                strokeWidth: 1,
                strokeLinecap,
                strokeLinejoin
            },
            axisLabel: _.assign({}, centeredLabelStyles, {
                padding: 0
            }),
            grid: {
                fill: "transparent",
                stroke: "#172d3b",
            },
            ticks: {
                fill: "transparent",
                size: 1,
                stroke: "#172d3b"
            },
            tickLabels: baseLabelStyles
        }
    }, baseProps),
    bar: _.assign({
        style: {
            data: {
                fill: charcoal,
                padding: 0,
                stroke: "transparent",
                strokeWidth: 0,
                width: 20
            },
            labels: baseLabelStyles
        }
    }, baseProps),
    chart: baseProps,

    line: _.assign({
        style: {
            data: {
                fill: "transparent",
                stroke: charcoal,
                strokeWidth: 2
            },
            labels: _.assign({}, baseLabelStyles, {
                textAnchor: "start"
            })
        }
    }, baseProps)
};

export default {theme}

