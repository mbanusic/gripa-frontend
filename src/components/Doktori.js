/* global $ */
import React, { Component } from 'react';
import ilija from '../img/ilija_kuzman.jpg';
import vladimir from '../img/vladimir_drazenovix.jpg';

class Doktori extends Component {
    constructor(props) {
        super(props);
        this.state = {
            form: 0,
            sent: 0,
            question: '',
            name: '',
            mail: ''
        }
    }
    openForm(e) {
        e.preventDefault();
        this.setState({form:1});
    }
    handleChange(event, type) {
        switch (type) {
            case 'name':
                this.setState({name: event.target.value});
                break;
            case 'mail':
                this.setState({mail: event.target.value});
                break;
            case 'question':
                this.setState({question: event.target.value});
                break;
            default:
                break;
        }

    }
    handleSubmit() {
        if (this.state.question && this.state.name && this.state.mail) {
            $.post(this.props.url, {
                question: this.state.question,
                name: this.state.name,
                mail: this.state.mail
            });//f&f
            this.setState({sent: 1});
            setTimeout(function () {
                this.setState({sent: 0, form: 0});
            }.bind(this), 3000);
        }
    }
    render() {
        const forma = (
            <div className="doctor_send row flex">
                <div className="red_line"></div>
                <textarea placeholder="Napiši pitanje..." className="doctor_question form-control" value={this.state.question} onChange={(e) => this.handleChange(e, 'question')}/>
                <div>
                    <input className="doctor_field form-control" type="text" name="FirstName" placeholder="Ime i prezime" value={this.state.name} onChange={(e) => this.handleChange(e, 'name')}/>
                    <input className="doctor_field form-control" type="email" name="eMail" placeholder="E-mail" value={this.state.mail} onChange={(e) => this.handleChange(e, 'mail')}/>
                </div>
                <button className="doctor_ask" onClick={() => this.handleSubmit()}>Pošalji</button>
            </div>
        );
        const thanks = (
            <div className="doctor_send_hvala row flex">
                <h1>Hvala</h1>
//                <div className="doctor_send_rezultati">Pogledaj rezultate</div>
            </div>
        );
        return (
            <div>
                <div id="pitaj_doktora" className="row flex">
                    <div className="red_line"></div>
                    <div className="doctor_one flex">
                        <div className="doctor_box flex_column">
                            <h4>Prof. dr. sc. Ilija Kuzman</h4>
                            <p className="doctor_txt">Pročelnik Zavoda za akutne respiratorne infekcije Klinike za infektivne bolesti dr. Fran Mihaljević i redoviti profesor na Medicinskom fakultetu Sveučilišta u Zagrebu. Jedan od najvećih hrvatskih stručnjaka koji liječi pacijente oboljele od gripe više od 35 godina</p>
                        </div>
                    </div>
                    <img width="144px" height="106px" role="presentation" src={ilija}/>

                    <div className="doctor_two flex">
                        <div className="doctor_box flex_column">
                            <h4>Dr. med. Vladimir Draženović</h4>
                            <p className="doctor_txt">Specijalist virusolog, voditelj Nacionalnog centra Svjetske zdravstvene organizacije za influencu pri Hrvatskom zavodu za javno zdravstvo. Vodeći ekspert za virus gripe u Hrvatskoj</p>
                        </div>
                    </div>
                    <img width="144px" role="presentation" src={vladimir}/>

                    <button className="doctor_ask" onClick={(e) => this.openForm(e)}>Pitaj doktora</button>
                </div>

                {this.state.form?forma:''}
                {this.state.sent?thanks:''}

            </div>
        );
    }
}

export default Doktori;