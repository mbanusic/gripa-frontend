/* global $ */
import React, { Component } from 'react';
import _ from 'underscore';

class Anketa extends Component {
    constructor(props) {
        super(props);
        this.state = {
            submitted: 0,
            county: 0,
            selected: []
        };
    }
    chooseCounty(e, i) {
        e.preventDefault();
        if (this.state.submitted) {
            return;
        }
        this.setState({ county: i });
    }

    chooseSymptoms(e, index) {
        e.preventDefault();
        if (this.state.submitted) {
            return;
        }
        let selected = this.state.selected;
        if (_.contains(this.state.selected, index)) {
            const i = selected.indexOf(index);
            selected.splice(i, 1);
        }
        else {
            selected.push(index);
        }
        this.setState({selected:selected});
    }

    submit(e) {
        e.preventDefault();
        if (this.state.selected.length && !this.state.submitted) {
            let that = this;
            $.post(this.props.url, {county:this.state.county, symptoms: this.state.selected}, function() {
                that.setState({submitted: 1});
            });
        }
    }

    getCounties() {
        return this.props.counties.map((county, index)=>{
                return (
                    <li key={index} className="col-xs-6 county" ><a href="#"
                                              className={(index === this.state.county) ? 'selected' : ''}
                                              onClick={(e) => this.chooseCounty(e, index)}>{ county.title }</a>
                    </li>
                );
        });
    }

    getSymptoms() {
        return this.props.symptoms.map((symptom, index)=>{
            const style = {
                color: _.contains(this.state.selected, index)?symptom.color:'#ffffff'
            };
            const circle_class = _.contains(this.state.selected, index)?'circle selected':'circle';
            return (
                <li key={index.toString()} className="row"><a href="#" onClick={(e) => this.chooseSymptoms(e, index)} style={style}><span className={circle_class} style={{backgroundColor: symptom.color}}/>{ symptom.title }</a></li>
            );
        });
    }
    render() {
        const overlay1 = this.state.submitted?(<div style={{width: '100%', height: 550, position: 'absolute', top:-20, left:-50, backgroundColor:"rgba(1,1,1,0.1)"}}/>):'';
        const overlay2 = this.state.submitted?(<div style={{width: '120%', height: 550, position: 'absolute', top:-20, left:-50, backgroundColor:"rgba(1,1,1,0.1)"}}/>):'';
        let hvala = null;
        if (this.state.submitted) {
            hvala = (<div className="anketa_thanks">Hvala<br/><span>Pogledajte rezultate</span></div>);
        }
        let button = (<button className="cyanbox" onClick={(e) => this.submit(e)}>POŠALJI</button>);
        if (this.state.submitted && window.innerWidth < 800) {
            button = hvala;

        }
        return (
            <div id="anketa" className="row">
                <div className="col-md-4">
                    <h2>OSTANITE ZDRAVI</h2>
                    <h3>KAKO SE OSJEĆAŠ DANAS?</h3>
                    <div className="redbox">Uključite se u anketu i saznajte ima li simptoma gripe u vašem susjedstvu</div>
                    <button className="red-button">{this.state.submitted?'HVALA':'ISPUNI ANKETU'}</button>
                </div>

                {hvala}
                <div className="col-md-4">
                    <span className="cyanbox">Odaberi simptome koji te muče</span>
                    <ul className="symptoms">
                        { this.getSymptoms() }
                    </ul>
                    {overlay1}
                </div>
                <div className="col-md-4">

                    <span className="cyanbox">Odaberi županiju u kojoj se nalaziš</span>
                    <div style={{position:'relative'}}>
                            <ul className="row counties">
                            { this.getCounties() }
                            </ul>

                        {button}

                    </div>
                    {overlay2}
                </div>
            </div>
        );
    }
}

export default Anketa;