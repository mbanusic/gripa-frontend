/* global $ */
import React, { Component } from 'react';
import { VictoryChart, VictoryBar, VictoryAxis } from 'victory';
import Slider from 'rc-slider';
import Handle from './Handle'
import theme from './theme';
import _ from 'underscore';

class Graf2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            week: 0,
            symptom: 0,
            county: 0,
            modal:0,
            updated:0
        }
    }
    sliderChange(value) {
        if (typeof this.props.data[value] !== 'undefined') {
            this.setState({week: value});
        }
    }
    componentDidUpdate() {
        if (!this.state.updated) {
            this.setState({week: (this.props.data.length - 1), updated:1});
        }
    }
    changeCounty(e, index) {
        e.preventDefault();
        if ('open' === index) {
            this.setState({modal:1});
        }
        else if ('close' === index) {
            this.setState({modal:0});
        }
        else {
             this.setState({county: index, modal:0});
        }
    };
    render() {
        if (this.props.data.length === 0) {
            return null;
        }
        const total = this.props.data[this.state.week][this.state.county].total;
        let max = 0;
        const percentages = _.map(this.props.data[this.state.week][this.state.county], (symptom, index) => {
            if (index !== 'total') {
                const y = Math.ceil(symptom/total*100);
                if (y>max) {
                    max = y;
                }
                return { x:parseInt(index, 10), y: y};
            }
        });
        const percentages_f = _.compact(percentages);
        const counties = this.props.counties.map((county, index) => {
            return (<li key={index} className="col-xs-6"><a href="#" onClick={(e) => {this.changeCounty(e, index)}}>{county.title}</a></li>);
        });
        const modal = (
            <div>
                <div className="overlay" onClick={(e) => this.changeCounty(e,'close')}/>
            <ul className="county_modal row">
                {counties}
            </ul>
            </div>
        );
        return (
            <div id="graf2">
                <h2>Kako se osjećaš danas?</h2>
                <h1>Kako se osjećaju ljudi u vašoj županiji?</h1>
                <h1>Koji su simptomi najčešći?</h1>
                <div className="row">
                    <div className="col-md-4 ticker">
                        <p className="county" onClick={(e) => this.changeCounty(e,'open')}>{this.props.counties[this.state.county].title}</p>
                        {this.state.modal?modal:null}
                        <p className="symptom" style={{color:this.props.symptoms[this.state.symptom].color}}>{this.props.symptoms[this.state.symptom].title}</p>
                        <p className="symptom_percent">{percentages[this.state.symptom].y} %</p>
                    </div>
                    <div className="col-md-8" id="bars">
                        <VictoryChart theme={theme.theme} height={220}>
                            <VictoryAxis dependentAxis={true} tickCount={10} offsetX={50}/>
                            <VictoryBar
                                data={percentages_f}
                                domain={{y:[0,max], x:[-1,10]}}
                                style={{
                                    data: {fill:(d) => d.x === this.state.symptom? this.props.symptoms[this.state.symptom].color : "rgba(0,149,164,0.3)"}
                                }}
                                events={[
                                    {
                                        target: "data",
                                        eventHandlers: {
                                            onClick: (event, target) => {
                                                this.setState({symptom:target.index});
                                            },
                                            onMouseEnter: (event, target) => {
                                                this.setState({symptom:target.index});
                                            }
                                        }
                                    }
                                ]}
                            />
                        </VictoryChart>
                    </div>
                </div>
                <div className="row">
                    <Slider
                        min={0} max={this.props.marks.length-1}
                        handle={<Handle marks={this.props.marks} max={this.props.marks.length-1} />}
                        onChange={(value) => this.sliderChange(value)}
                        dots
                        value={this.state.week}
                    />
                </div>

            </div>
        );
    }
}

export default Graf2;