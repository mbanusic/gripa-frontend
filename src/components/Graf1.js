/* global $ */
import React, { Component } from 'react';
import * as d3 from "d3";
import d3Tip from "d3-tip";
d3.tip = d3Tip;
import _ from 'underscore';
import Slider from 'rc-slider';
import Handle from './Handle';

class Graf1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            symptom:0,
            week:0,
            updated:0

        }
    }
    componentDidUpdate() {
        if (!this.state.updated) {
            this.setState({week: (this.props.data.length - 1), updated:1});
        }
        else {
            this.loadBubbles();
        }
    }
    sliderChange(value) {
        if (this.props.data.length > 1) {
            if (typeof(this.props.data[value]) !== 'undefined') {
                this.setState({week: value});
            }
        }
    }
    loadBubbles() {
        if (this.props.data.length === 0) {
            return;
        }
        d3.select('.bubbles').remove();
        const dataset = this.props.data[this.state.week][this.state.symptom];
        let diameter = window.innerWidth;
        if (diameter>450) {
            diameter = 450;
        }
        else {
            diameter -=50;
        }

        const bubble = d3.pack(dataset)
            .size([diameter, diameter])
            .padding(1.5);
        const tip = d3.tip()
            .attr('class', 'circle_tooltip')
            .offset([40, 0])
            .html(function(d) {
                return d.data.county;
            });
        const svg = d3.select(".chart")
            .append("svg")
            .attr("width", diameter)
            .attr("height", diameter)
            .attr("class", "bubbles");
        svg.call(tip);
        const nodes = d3.hierarchy(dataset)
            .sum(function(d) { return d.number; });

        const node = svg.selectAll(".node")
            .data(bubble(nodes).descendants())
            .enter()
            .filter(function(d){
                return  !d.children
            })
            .append("g")
            .attr("class", "node")
            .attr("transform", function(d) {
                return "translate(" + d.x + "," + d.y + ")";
            })
            .on('mouseover', tip.show)
            .on('mouseout', tip.hide)
            .on('touchstart', tip.show);


        node.append("circle")
            .attr("r", function(d) {
                return d.r;
            })
            .style("fill", this.props.symptoms[this.state.symptom].color);

        node.append("text")
            .style('fill', '#ffffff')
            .attr("transform", function(d) {
                return "translate(-30,0)";
            })
            .style('font-size', '20px')
            .text(function (d) {
                if (d.data.county === 'Grad Zagreb')
                    return 'Zagreb';
            });

        d3.select(self.frameElement)
            .style("height", diameter + "px");

    }
    handleClick(symptom, e) {
        e.preventDefault();
        this.setState({symptom:symptom});
    }
    render() {
        if (this.props.data.length === 0) {
            return null;
        }
        const symptoms = this.props.symptoms.map( (symptom, index)=>{
            let klasa = symptom.class_name;
            let style = {}, style_a = {};
            if (index === this.state.symptom) {
                klasa += ' selected';
                style = {
                    borderColor: symptom.color,
                };
                style_a = {
                    color: symptom.color
                };
            }
            return (
                <li className={klasa} key={index} style={style}><a href="#" onClick={(event) => this.handleClick(index, event)} style={style_a}>{symptom.title}</a></li>
            );
        } );
        return (
            <div id="graf1">
                <div className="row bubble">
                    <div className="col-md-5">
                        <h2>Kako se osjećaš danas?</h2>
                        <h1>Gdje su najučestaliji simptomi gripe i prehlade</h1>
                        <ul>
                            {symptoms}
                        </ul>
                    </div>
                    <div className="col-md-7 chart">

                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12">
                        <Slider
                            min={0} max={this.props.marks.length-1}
                            handle={<Handle marks={this.props.marks} max={this.props.marks.length-1} />}
                            onChange={(value) => this.sliderChange(value)}
                            value={this.state.week}
                            dots
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default Graf1;