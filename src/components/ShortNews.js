import React, { Component } from 'react';

class ShortNews extends Component {
    render() {
        return (
            <div id="short" className="row">
                <a href="http://www.jutarnji.hr/zivim/dossier-gripa-evo-zasto-je-cijepljenje-jedina-razumna-mjera-za-sprecavanje-gripe/5391503/"><h1><span>DOSSIER GRIPA:</span> Evo zašto je cijepljenje jedina razumna mjera za sprečavanje gripe</h1></a>
                <a href="http://www.jutarnji.hr/zivim/mitovi-i-istina-o-gripi-osam-cinjenica-o-najrasirenijoj-bolesti-koje-ce-vas-iznenaditi-i-pomoci-da-ostanete-zdravi/5381279/"><h1><span>Mitovi i istina o gripi:</span> Osam činjenica o najraširenijoj bolesti koje će vas iznenaditi</h1></a>
            <div>
            </div>
            </div>

        );
    }
}

export default ShortNews;