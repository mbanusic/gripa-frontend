/* global $ */
import React, { Component } from 'react';

class Menu extends Component {
    handleClick(e) {
        const offset = -120, scrollTime = 700;
        $("html, body").animate({
            scrollTop: $( $(e.target).attr("href") ).offset().top + offset
        }, scrollTime);
    }
    render() {
        return (
            <header className="row">
                <ul>
                    <li><a href="#vodic" onClick={(e) => this.handleClick(e)}>VODIČ KROZ SIMPTOME</a></li>
                    <li><a href="#vijesti" onClick={(e) => this.handleClick(e)}>VIJESTI</a></li>
                    <li><a href="#pitaj_doktora" onClick={(e) => this.handleClick(e)}>PITAJ DOKTORA</a></li>
                    <li><a href="#anketa" onClick={(e) => this.handleClick(e)}>ANKETA: KAKO SE OSJEĆAŠ</a></li>
                    <li><a href="#graf4" onClick={(e) => this.handleClick(e)}>Gripa u Hrvatskoj</a></li>
                    <li><a href="#graf6" onClick={(e) => this.handleClick(e)}>Gripa u EU</a></li>
                </ul>
            </header>
        );
    }
}

export default Menu;