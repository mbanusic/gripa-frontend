import React, { Component } from 'react';
import foto from '../img/dijete.jpg';
import foto1 from '../img/bolno-grlo.jpg';
import foto2 from '../img/gripa_rez.jpg';
import foto3 from '../img/gripa_virus.jpg';

class Footer extends Component {
    render() {
        return (
            <div>
                <div id="vijesti" className="row">
                    <h1 className="news_heading_1">Aktualno</h1>
                    <div className="vijesti_box">
            
                        <a href="http://www.jutarnji.hr/life/zdravlje/kako-se-zastititi-od-epidemije-evo-sto-trebate-o-znati-o-virusima-i-podtipovima-gripe-koji-su-stigli-u-hrvatsku/5482563/">
                        <div><img src={foto3} role="presentation" width="350" className="#"/>
                            <div className="vijest_title"><span>KAKO SE ZAŠTITITI OD EPIDEMIJE?</span> Evo što trebate o znati o virusima i podtipovima gripe koji su stigli u Hrvatsku</div>
                        </div>
                        </a>
            
                        <a href="http://www.jutarnji.hr/zivim/mitovi-i-istina-o-gripi-osam-cinjenica-o-najrasirenijoj-bolesti-koje-ce-vas-iznenaditi-i-pomoci-da-ostanete-zdravi/5381279/">
                        <div><img src={foto1} role="presentation" width="350" className="#"/>
                            <div className="vijest_title"><span>MITOVI I ISTINA O GRIPI:</span> Osam činjenica o najraširenijoj bolesti koje će vas iznenaditi</div>
                        </div>
                        </a>
            
                        <a href="http://apps.jutarnji.hr/gripa_rezultati/">
                        <div><img src={foto2} role="presentation" width="350" className="#"/>
                            <div className="vijest_title"><span>VELIKA ANKETA OTKRIVA:</span>Evo kako su se Hrvati osjećali ove zime</div>
                        </div>
                        </a>
            
                    </div>
                </div>
                
            </div>
        );
    }
}    

export default Footer;